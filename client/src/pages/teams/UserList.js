import React, { useEffect, useState } from "react";
import { getAllUsers } from "../../api/user";
import Searchbar from "../../components/Searchbar";
import { CatchAsync } from "../../util";
import UserTable from "./UserTable";

const initUser = {
  fullname: "",
  email: "",
  phone: "",
  dob: "",
  gender: "",
  addresses: [],
  is_admin: false,
  is_externalInstructor: false,
  is_internalInstructor: false,
  is_staff: false,
  password: "",
  confirm_password: "",
  image: null,
  gender2: "",
};

const searchOptions = [
  {
    name: "Email",
    selector: "email",
  },
];

const probableRoles = {
  is_admin: "active",
  is_externalInstructor: "external instructor",
  is_internalInstructor: "internal instructor",
  is_hod: "hod",
};

const UserList = ({ onRowClicked, selectedUsers, selectedDepts }) => {
  // Hooks
  const [rows, setRows] = useState([]);
  const [isUsersListManpulated, setUsersListManipulated] = useState(true);
  const [filterBy, setFilterBy] = useState(searchOptions[0]);
  const [searchTerm, setSearchTerm] = useState("");
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(5);
  const [totalRows, setTotalRows] = useState(0);
  const [isUserDataLoading, setIsUserDataLoading] = useState(false);

  useEffect(
    CatchAsync(
      async () => {
        if (isUsersListManpulated) {
          const params = {
            search: searchTerm,
            filter_by: filterBy.selector,
            limit: perPage,
            offset: (page - 1) * perPage,
          };
          setIsUserDataLoading(true);
          const res = await getAllUsers(params);
          setRows(res.data.results);
          setTotalRows(res.data.count);
          setIsUserDataLoading(false);
        }
        setUsersListManipulated(false);
      },
      () => setIsUserDataLoading(false)
    ),
    [isUsersListManpulated]
  );

  const handleFilter = (term) => {
    setSearchTerm(term);
    setUsersListManipulated(true);
  };

  const handleFilterBy = (option) => {
    setFilterBy(option);
    setUsersListManipulated(true);
  };

  const handlePageChange = (page) => {
    setPage(page);
    setUsersListManipulated(true);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setIsUserDataLoading(true);

    const params = {
      search: searchTerm,
      filter_by: filterBy.selector,
      limit: newPerPage,
      offset: (page - 1) * newPerPage,
    };

    getAllUsers(params).then((res) => {
      setRows(res.data.results);
      setTotalRows(res.data.count);
    });

    setPerPage(newPerPage);

    setIsUserDataLoading(false);
  };

  return (
    <div className="user-list">
      <div className="card shadow mb-4">
        <div className="card-header py-3">
          <Searchbar
            options={searchOptions}
            selected={filterBy.name}
            onChange={handleFilter}
            onFilterChange={handleFilterBy}
          />
        </div>
        <div className="card-body">
          <UserTable
            rows={rows}
            onRowClicked={onRowClicked}
            perPage={perPage}
            handlePerRowsChange={handlePerRowsChange}
            handlePageChange={handlePageChange}
            isUserDataLoading={isUserDataLoading}
            totalRows={totalRows}
            selectedUsers = {selectedUsers}
          />
        </div>
      </div>
    </div>
  );
};

export default UserList;
