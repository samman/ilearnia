import meetingsData from "../assets/mock-data/meetings.json";
import usersData from "../assets/mock-data/users.json";
import examsData from "../assets/mock-data/exams.json";
import levelsData from "../assets/mock-data/levels.json";
import MockAdapter from "axios-mock-adapter";

let meetingsList = meetingsData.meetings;
let usersList = usersData.users;
let examsList = examsData.exams;
let levelsList = levelsData.levels

export const isMockEnabled = () => {
  return process.env.REACT_APP_MOCK_ENABLED === "true";
};

export const initializeAxiosMockAdapter = (instance) => {
  const mock = new MockAdapter(instance);
  mock.onGet("/meetings").reply((config) => getMeetings(config));
  mock.onGet(/\/meetings\/\d+/).reply((config) => getMeeting(config));
  mock.onPost("/meetings").reply((config) => addMeeting(config));
  mock.onPut(/\/meetings\/\d+/).reply((config) => editMeeting(config));
  mock.onDelete(/\/meetings\/\d+/).reply((config) => removeMeeting(config));

  mock.onGet("/users").reply((config) => getUsers(config));
  mock.onGet(/\/users\/\d+/).reply((config) => getUser(config));
  mock.onPost("/users").reply((config) => addUser(config));
  mock.onPut(/\/users\/\d+/).reply((config) => editUser(config));
  mock.onDelete(/\/users\/\d+/).reply((config) => removeUser(config));

  mock.onGet("/exams").reply((config) => getAllExams(config));
  mock.onGet(/\/exams\/\d+/).reply((config) => getExam(config));
  mock.onPost("/exams").reply((config) => addExam(config));
  mock.onPut(/\/exams\/\d+/).reply((config) => editExam(config));
  mock.onDelete(/\/exams\/\d+/).reply((config) => removeExam(config));

  mock.onGet("/levels").reply((config) => getAllLevels(config));
  mock.onGet(/\/levels\/\d+/).reply((config) => getLevel(config));
  mock.onPost("/levels").reply((config) => addLevel(config));
  mock.onPut(/\/levels\/\d+/).reply((config) => editLevel(config));
  mock.onDelete(/\/levels\/\d+/).reply((config) => removeLevel(config));
};

export const getMeetings = (config) => {
  let filteredMeetings = meetingsList;
  if (config.params && config.params.filterBy && config.params.searchTerm) {
    filteredMeetings = meetingsList.filter((meeting) =>
      meeting[config.params.filterBy]
        .toString()
        .match(config.params.searchTerm.toString())
    );
  }
  return [200, filteredMeetings];
};

export const getMeeting = (config) => {
  const id = extractIdPathParamFromUrl(config);
  const meeting = meetingsList.find((c) => c.id === id);
  return [200, meeting];
};

const extractIdPathParamFromUrl = (config) => {
  return config.url.split("/").pop();
};

export const addMeeting = (config) => {
  const meeting = JSON.parse(config.data);
  meetingsList.push(meeting);
  return [200, meeting];
};

export const editMeeting = (config) => {
  const id = extractIdPathParamFromUrl(config);
  const meetingIndex = meetingsList.findIndex((c) => c.id === id);
  const meeting = JSON.parse(config.data);
  meetingsList[meetingIndex] = meeting;
  return [200, meeting];
};

export const removeMeeting = (config) => {
  const id = extractIdPathParamFromUrl(config);
  meetingsList = meetingsList.filter((c) => String(c.id) !== String(id));
  console.log(meetingsList);
  return [204, null];
};

// For users
export const getUsers = (config) => {
  let filteredUsers = usersList;
  if (config.params && config.params.filterBy && config.params.searchTerm) {
    filteredUsers = usersList.filter((user) =>
      user[config.params.filterBy]
        .toString()
        .match(config.params.searchTerm.toString())
    );
  }
  return [200, filteredUsers];
};

export const getUser = (config) => {
  const id = extractIdPathParamFromUrl(config);
  const user = usersList.find((c) => c.id === id);
  return [200, user];
};

export const addUser = (config) => {
  const user = JSON.parse(config.data);
  usersList.push(user);
  return [200, user];
};

export const editUser = (config) => {
  const id = extractIdPathParamFromUrl(config);
  const userIndex = usersList.findIndex((c) => c.id === id);
  const user = JSON.parse(config.data);
  meetingsList[userIndex] = { ...meetingsData[userIndex], ...user };
  console.log(meetingsList[userIndex]);
  return [200, user];
};

export const removeUser = (config) => {
  const id = extractIdPathParamFromUrl(config);
  usersList = usersList.filter((c) => `${c.id}` !== id);
  return [204, null];
};

// For exams
export const getAllExams = (config) => {
  let filteredExams = examsList;
  if (config.params && config.params.filterBy && config.params.searchTerm) {
    filteredExams = examsList.filter((exam) =>
      exam[config.params.filterBy]
        .toString()
        .match(config.params.searchTerm.toString())
    );
  }
  return [200, {results: filteredExams}];
};

export const getExam = (config) => {
  const id = extractIdPathParamFromUrl(config);
  const exam = examsList.find((c) => c.id === id);
  return [200, exam];
};

export const addExam = (config) => {
  const exam = JSON.parse(config.data);
  examsList.push(exam);
  return [200, exam];
};

export const editExam = (config) => {
  const id = extractIdPathParamFromUrl(config);
  const examIndex = examsList.findIndex((c) => c.id === id);
  const exam = JSON.parse(config.data);
  meetingsList[examIndex] = { ...meetingsData[examIndex], ...exam };
  console.log(meetingsList[examIndex]);
  return [200, exam];
};

export const removeExam = (config) => {
  const id = extractIdPathParamFromUrl(config);
  examsList = examsList.filter((c) => `${c.id}` !== id);
  return [204, null];
};

// For levels
export const getAllLevels = (config) => {
  let filteredLevels = levelsList;
  if (config.params && config.params.filterBy && config.params.searchTerm) {
    filteredLevels = levelsList.filter((level) =>
      level[config.params.filterBy]
        .toString()
        .match(config.params.searchTerm.toString())
    );
  }
  return [200, {results: filteredLevels}];
};

export const getLevel = (config) => {
  const id = extractIdPathParamFromUrl(config);
  const level = levelsList.find((c) => c.id === id);
  return [200, level];
};

export const addLevel = (config) => {
  const level = JSON.parse(config.data);
  levelsList.push(level);
  return [200, level];
};

export const editLevel = (config) => {
  const id = extractIdPathParamFromUrl(config);
  const levelIndex = levelsList.findIndex((c) => c.id === id);
  const level = JSON.parse(config.data);
  meetingsList[levelIndex] = { ...meetingsData[levelIndex], ...level };
  console.log(meetingsList[levelIndex]);
  return [200, level];
};

export const removeLevel = (config) => {
  const id = extractIdPathParamFromUrl(config);
  levelsList = levelsList.filter((c) => `${c.id}` !== id);
  return [204, null];
};
