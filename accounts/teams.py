from django.db import models
from django.utils import timezone
from rest_framework import serializers, viewsets
from django.core.exceptions import ObjectDoesNotExist
from .overide import IsAdmin, IsInstructor

from django.contrib.auth import get_user_model
User = get_user_model()


class Teams(models.Model):
	name = models.CharField(max_length=55)
	description = models.CharField(max_length=255)
	created_on = models.DateTimeField(default=timezone.now)
	created_by = models.CharField(max_length=255)
	members = models.ManyToManyField(User)


	def __str__(self):
		return self.name



class TeamsSerializer(serializers.ModelSerializer):
	class Meta:
		model=Teams
		fields="__all__"



class TeamsView(viewsets.ModelViewSet):
	queryset = Teams.objects.all()
	serializer_class = TeamsSerializer

	def get_permissions(self):
		if self.action in ['list', 'create', 'destroy', 'update', 'partial_update']:
			permissions_classes = [IsAdmin, IsInstructor]
		elif self.action in ['retrieve']:
			permissions_classes = [IsAdmin, IsInstructor, CanView]
		else:
			return super().get_permissions()

		return [permissions() for permissions in permissions_classes]

	def perform_create(self, serializer):
		serializer.save(created_by=self.request.user)



