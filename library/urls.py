from django.urls import path, include
from rest_framework import routers

from .views import *

router=routers.DefaultRouter()
router.register('author',AuthorView,basename='author')
router.register('genre',GenreView,basename='genre')
router.register('language',LanguageView,basename='language')
router.register('book',BookView,basename='book')
router.register('difficulty',LevelDifficultyView,basename='difficulty')
router.register('bookmark',BookmarkView,basename='bookmark')

urlpatterns = [
    path('', include(router.urls)),
]