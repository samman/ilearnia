import React, { useState, useContext } from "react";
import logo from "../assets/img/logo/logo-alt.png";
import { NavLink } from "react-router-dom";
import { AuthContext } from "../contexts/Auth";

const Sidebar = () => {
  const [isSidebarToggled, toogleSidebar] = useState(false);
  const authContext = useContext(AuthContext);
  const {loggedInUser} = authContext;

  return (
    <div
      className={`Sidebar wrapper ${
        isSidebarToggled ? "toggled" : ""
      } bg-gradient-primary`}
    >
      {/* Sidebar */}
      <ul
        className={`navbar-nav sidebar sidebar-dark accordion ${
          isSidebarToggled ? "toggled" : ""
        }`}
        id="accordionSidebar"
      >
        {/* Sidebar - Brand */}
        <NavLink
          className="sidebar-brand d-flex align-items-center justify-content-center"
          to="/"
        >
          <div className="sidebar-brand-icon w-32">
            <img
              src={logo}
              alt="logo"
              className="w-100-p"
              style={{ filter: "invert(1)" }}
            ></img>
          </div>
          <div className="sidebar-brand-text mx-3">
            {process.env.REACT_APP_NAME}
          </div>
        </NavLink>

        {/* Divider */}
        {/* <hr className="sidebar-divider my-0" /> */}

        {/* Nav Item - Dashboard */}
        {/* <li className="nav-item active">
                    <a className="nav-link" href="index.html">
                        <i className="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li> */}

        {/* Divider */}
        {/* <hr className="sidebar-divider" /> */}

        {/* Heading */}
        {/* <div className="sidebar-heading">
                    Interface
                </div> */}

        {/* Nav Item - Pages Collapse Menu */}
        <li className="nav-item">
          <NavLink
            className="nav-link collapsed"
            to="/dashboard"
            data-toggle="collapse"
            data-target="#collapseTwo"
            aria-expanded="true"
            aria-controls="collapseTwo"
            activeClassName="active"
          >
            <i className="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </NavLink>

          {loggedInUser?.is_admin && (
            <NavLink
              className="nav-link collapsed"
              to="/departments"
              data-toggle="collapse"
              data-target="#collapseTwo"
              aria-expanded="true"
              aria-controls="collapseTwo"
              activeClassName="active"
            >
              <i className="fas fa-building"></i>
              <span>Departments</span>
            </NavLink>
          )}
          {loggedInUser?.is_admin && (
            <NavLink
              className="nav-link collapsed"
              to="/teams"
              data-toggle="collapse"
              data-target="#collapseTwo"
              aria-expanded="true"
              aria-controls="collapseTwo"
              activeClassName="active"
            >
              <i className="fas fa-users"></i>
              <span>Teams</span>
            </NavLink>
          )}
          {loggedInUser?.is_admin && (
            <NavLink
              className="nav-link collapsed"
              to="/users"
              data-toggle="collapse"
              data-target="#collapseTwo"
              aria-expanded="true"
              aria-controls="collapseTwo"
              activeClassName="active"
            >
              <i className="fas fa-user-friends"></i>
              <span>Users</span>
            </NavLink>
          )}

          <NavLink
            className="nav-link collapsed"
            to="/meetings"
            data-toggle="collapse"
            data-target="#collapseTwo"
            aria-expanded="true"
            aria-controls="collapseTwo"
            activeClassName="active"
          >
            <i className="fas fa-video"></i>
            <span>Meetings</span>
          </NavLink>

          <NavLink
            className="nav-link collapsed"
            to="/exams"
            data-toggle="collapse"
            data-target="#collapseTwo"
            aria-expanded="true"
            aria-controls="collapseTwo"
            activeClassName="active"
          >
            <i className="fas fa-video"></i>
            <span>Exams</span>
          </NavLink>
          {/* <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Custom Components:</h6>
                            <a className="collapse-item" href="buttons.html">Buttons</a>
                            <a className="collapse-item" href="cards.html">Cards</a>
                        </div>
                    </div> */}
        </li>

        {/* Nav Item - Utilities Collapse Menu */}
        {/* <li className="nav-item">
                    <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                        aria-expanded="true" aria-controls="collapseUtilities">
                        <i className="fas fa-fw fa-wrench"></i>
                        <span>Utilities</span>
                    </a>
                    <div id="collapseUtilities" className="collapse" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Custom Utilities:</h6>
                            <a className="collapse-item" href="utilities-color.html">Colors</a>
                            <a className="collapse-item" href="utilities-border.html">Borders</a>
                            <a className="collapse-item" href="utilities-animation.html">Animations</a>
                            <a className="collapse-item" href="utilities-other.html">Other</a>
                        </div>
                    </div>
                </li> */}

        {/* Divider */}
        {/* <hr className="sidebar-divider" /> */}

        {/* Heading */}
        {/* <div className="sidebar-heading">
                    Addons
                </div> */}

        {/* Nav Item - Pages Collapse Menu */}
        {/* <li className="nav-item">
                    <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                        aria-expanded="true" aria-controls="collapsePages">
                        <i className="fas fa-fw fa-folder"></i>
                        <span>Pages</span>
                    </a>
                    <div id="collapsePages" className="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Login Screens:</h6>
                            <a className="collapse-item" href="login.html">Login</a>
                            <a className="collapse-item" href="register.html">Register</a>
                            <a className="collapse-item" href="forgot-password.html">Forgot Password</a>
                            <div className="collapse-divider"></div>
                            <h6 className="collapse-header">Other Pages:</h6>
                            <a className="collapse-item" href="404.html">404 Page</a>
                            <a className="collapse-item" href="blank.html">Blank Page</a>
                        </div>
                    </div>
                </li> */}

        {/* Nav Item - Charts */}
        {/* <li className="nav-item">
                    <a className="nav-link" href="charts.html">
                        <i className="fas fa-fw fa-chart-area"></i>
                        <span>Charts</span></a>
                </li> */}

        {/* Nav Item - Tables */}
        {/* <li className="nav-item">
                    <a className="nav-link" href="tables.html">
                        <i className="fas fa-fw fa-table"></i>  
                        <span>Tables</span></a>
                </li> */}

        {/* Divider */}
        <hr className="sidebar-divider d-none d-md-block" />

        {/* Sidebar Toggler (Sidebar) */}
        <div className="text-center d-none d-md-inline mt-auto">
          <button
            className="rounded-circle border-0"
            id="sidebarToggle"
            onClick={() => toogleSidebar(!isSidebarToggled)}
          ></button>
        </div>
      </ul>
    </div>
  );
};

export default Sidebar;
