import React from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";

const LoginForm = ({ onSubmit, isLoading }) => {
  const { register, handleSubmit, errors } = useForm();
  const history = useHistory();

  return (
    <>
      <form
        className="user needs-validation"
        onSubmit={handleSubmit(onSubmit)}
        noValidate
      >
        <div className="form-group">
          <input
            type="email"
            className="form-control curve form-control-user"
            id="inputEmail"
            name="email"
            aria-describedby="emailHelp"
            placeholder="Email"
            ref={register({
              required: true,
              pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
            })}
          />
          <div className="errors text-center mt-2">
            {errors.email && errors.email.type === "required" && (
              <span className="text-danger">Email is required</span>
            )}
            {errors.email && errors.email.type === "pattern" && (
              <span className="text-danger">Invalid email</span>
            )}
          </div>
        </div>
        <div className="form-group">
          <input
            type="password"
            className={"form-control curve form-control-user"}
            id="inputPassword"
            name="password"
            placeholder="Password"
            ref={register({ required: true })}
          />
          <div className="errors text-center mt-2">
            {errors.password && errors.password.type === "required" && (
              <span className="text-danger">Password is required</span>
            )}
          </div>
        </div>

        <button
          type="submit"
          className="btn bg-primary text-light btn-user btn-block curve"
          disabled={isLoading}
        >
          Login
        </button>

        <button
          type="submit"
          className="btn bg-secondary text-light btn-user btn-block curve"
          disabled={isLoading}
          onClick={()=>history.push("/signup")}
        >
          Signup
        </button>
      </form>
      <hr />
      {isLoading && (
        <div className="d-flex align-items-center justify-content-center">
          <div
            className="spinner-border spinner-border-sm ml-2"
            role="status"
          ></div>
        </div>
      )}
      {/* <div className="text-center">
        <a className="small" href="forgot-password.html">
          Forgot Password?
        </a>
      </div> */}
    </>
  );
};

export default LoginForm;
