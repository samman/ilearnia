import axios from "axios";

function getAllTeams(params) {
  const token = localStorage.getItem("jwt_token");
  return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/team/`, {
    headers: {
      authorization: `Token ${token}`,
    },
    params
  });
}

function createTeam(data) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/team/`, data, {
    headers: {
      authorization: `Token ${token}`,
    },
  });
}

function updateTeam(teamId, data) {
  const token = localStorage.getItem("jwt_token");
  return axios.put(
    `${process.env.REACT_APP_API_BASE_URL}/api/team/${teamId}/`,
    data,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function removeTeam(teamId) {
  const token = localStorage.getItem("jwt_token");
  return axios.delete(
    `${process.env.REACT_APP_API_BASE_URL}/api/team/${teamId}/`,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

export { getAllTeams, createTeam, updateTeam, removeTeam };
