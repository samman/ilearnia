import React from "react";

const Dropper = ({
  accept,
  form,
  validation,
  name,
  additionalClass,
  children,
  onChange,
}) => {
  return (
    <div className={`dropper ${additionalClass}`}>
      {children}
      <input
        type="file"
        accept={accept}
        ref={form && form(validation)}
        name={name}
        onChange={onChange}
        className="h-100-p"
      />
    </div>
  );
};

export default Dropper;
