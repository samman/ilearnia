import React from "react";

const ExamDetailsForm = ({
  register,
  errors,
  departments,
  onCancel,
  moveToSecondPage,
  visible,
}) => {
  return (
    <div hidden={!visible}>
      <div className="row">
        <div className="form__group field mb-4">
          <input
            type="input"
            className="form__field"
            placeholder="Name"
            name="name"
            id="name"
            ref={register({
              required: true,
            })}
          />
          {errors.name?.type === "required" && (
            <span className="text-danger">Exam title is required</span>
          )}
          <label htmlFor="name" className="form__label">
            Exam Title
          </label>
        </div>
        <div className="form-group col-sm-12">
          <label htmlFor="input-duration" className="m-b-5 f-w-600">
            Departments
          </label>
          {departments?.length ? (
            departments.map((dept) => (
              <div className="form-check" key={dept.id}>
                <input
                  type="checkbox"
                  className="form-check-input"
                  id={`${dept.id}-radio`}
                  name="department"
                  ref={register({ required: true })}
                  value={dept.id}
                />
                <label
                  className="form-check-label"
                  htmlFor={`${dept.id}-radio`}
                >
                  {dept.dept_name}
                </label>
              </div>
            ))
          ) : (
            <div>
              <span className="font-italic">
                No departments exists. Please first create a department.
              </span>
            </div>
          )}
          {errors.department && errors.department.type === "required" && (
            <span className="text-danger">
              At least one department is required
            </span>
          )}
        </div>
      </div>
      <div className="d-flex flex-row justify-content-between align-items-center p-3 bg-white">
        <button
          className="btn btn-primary d-flex align-items-center btn-danger"
          type="button"
          onClick={onCancel}
        >
          <i className="fa fa-angle-left mt-1 mr-1"></i>&nbsp;Cancel
        </button>
        <button
          className="btn btn-primary border-success align-items-center btn-success"
          type="button"
          onClick={moveToSecondPage}
        >
          Next<i className="fa fa-angle-right ml-2"></i>
        </button>
      </div>
    </div>
  );
};

export default ExamDetailsForm;
