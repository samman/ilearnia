import axios from "axios";

function createMeeting(data){
    const token = localStorage.getItem("jwt_token");
    return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/zoom/`, data, {
        headers: {
            authorization: `Token ${token}`
        }
    })

}

function getAllMeetings(params){
    const token = localStorage.getItem("jwt_token");
    return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/zoom/`, {
        headers: {
            authorization: `Token ${token}`
        },
        params        
    })

}

function removeMeeting(meetingId){
    const token = localStorage.getItem("jwt_token");
    return axios.delete(`${process.env.REACT_APP_API_BASE_URL}/api/zoom/${meetingId}/`, {
        headers: {
            authorization: `Token ${token}`
        }
    })
}

export { createMeeting, getAllMeetings, removeMeeting }
