import React from "react";
import Dropper from "../../../components/Dropper";

const ExamQuestionsCSVForm = ({
  isCSVUploadLoading,
  register,
  watch,
  errors,
  moveToFirstPage,
  visible,
  onOpenExamForm,
}) => {
  const csvFiles = watch("csv");

  return (
    <div className="card-body" hidden={!visible}>
      <h5 className="card-title">Upload CSV File</h5>
      <h6 className="card-subtitle mb-2 text-muted">
        upload a csv file to create new exam
      </h6>
      <div className="d-flex flex-column h-300 justify-content-between mt-4">
        <div>
          <Dropper
            accept=".csv"
            name="csv"
            additionalClass="h-200 w-80-p"
            form={register}
            validation={{ required: true }}
          >
            {csvFiles?.length ? (
              <p className="h3">{csvFiles[0]?.name}</p>
            ) : (
              <span>Select a CSV file</span>
            )}
          </Dropper>
          {errors.csv?.type === "required" && (
            <div className="text-center">
              <span className="text-danger">A CSV file is required</span>
            </div>
          )}
        </div>
        <div className="d-flex flex-row justify-content-between align-items-center p-3 bg-white">
          <button
            className="btn btn-primary d-flex align-items-center btn-danger"
            type="button"
            onClick={moveToFirstPage}
          >
            <i className="fa fa-angle-left mt-1 mr-1"></i>&nbsp;Go Back
          </button>
          <button
            className="btn btn-primary border-success align-items-center btn-success"
            type="submit"
            disabled={isCSVUploadLoading}
          >
            Save
            {isCSVUploadLoading && (
              <div
                className="spinner-border spinner-border-sm ml-2"
                role="status"
              >
                <span className="sr-only">Loading...</span>
              </div>
            )}
            <i className="fa fa-angle-right ml-2"></i>
          </button>
        </div>
        <div className="d-flex justify-content-end">
          <p className="btn d-inline text-info mt-4" onClick={onOpenExamForm}>
            Open exam form instead
          </p>
        </div>
      </div>
    </div>
  );
};

export default ExamQuestionsCSVForm;
