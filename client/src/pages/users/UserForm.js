import React, { useEffect, useState, useContext } from "react";
import { useForm } from "react-hook-form";
import Dropper from "../../components/Dropper";
import "./UserForm.css";

const UserForm = ({ user, departments, onSubmit, onClose, isSaveLoading }) => {
  const mode = user.email ? "update" : "create";

  // Hooks
  const [profilepic, setProfilepic] = useState(null);
  const [profilepicIsUpdated, setProfilepicIsUpdated] = useState(false);
  const [profilepicIsSet, setProfilepicIsSet] = useState(!!user.image || false);

  const { register, handleSubmit, watch, errors, getValues } = useForm(
    !!user && {
      defaultValues: user,
    }
  );

  useEffect(() => {
    if (profilepicIsUpdated) {
      const profilepic = getValues("profilepic")[0];
      setProfilepic(profilepic);
      profilepic && setProfilepicIsSet(true);
      setProfilepicIsUpdated(false);
    }
  }, [profilepicIsUpdated]);

  const password = watch("password");

  return (
    <div>
      <div className="row m-l-0 m-r-0">
        <div className="col-sm-4 bg-gradient-primary user-profile">
          <div className="card-block text-center text-white h-100-p d-flex align-items-center">
            <Dropper
              name="profilepic"
              form={register}
              accept="profilepic/*"
              onChange={() => setProfilepicIsUpdated(true)}
              additionalClass={`h-200 ${profilepicIsSet ? "dropper-filled" : ""
                }`}
            >
              {!!user.image || !!profilepic ? (
                <img
                  src={
                    !!profilepic ? URL.createObjectURL(profilepic) : user.image
                  }
                  alt=""
                  className="w-100-p dropper-img"
                />
              ) : (
                  <span>Select Profile Picture</span>
                )}
            </Dropper>
          </div>
        </div>
        {/* {mode === "create" && <UserForm onSubmit={onSubmit} onClose={onClose} />}
            {mode === "update" && <UserUpdateForm user={user} onSubmit={onSubmit} onClose={onClose} />} */}
        <div className="col-sm-8" style={{ minHeight: "40vh" }}>
          <div className="card-block">
            <h6 className="m-b-20 p-b-5 b-b-default f-w-600">
              Basic Information
            </h6>

            <form className="row" noValidate onSubmit={handleSubmit(onSubmit)}>
              <input name="id" readOnly hidden ref={register({})} />

              <div className="row mx-0 w-100-p">
                <div className="form-group col-sm-12">
                  <label htmlFor="input-employee-id" className="m-b-5 f-w-600">
                    Employee ID
                  </label>
                  <input
                    type="text"
                    className="form-control text-muted f-w-400"
                    id="input-employee-id"
                    name="employee_id"
                    placeholder="Enter employee id"
                    ref={register()}
                    disabled={mode === "update"}
                  />
                </div>

                <div className="row mx-0 w-100-p">
                  <div className="form-group col-sm-12">
                    <label htmlFor="input-fullname" className="m-b-5 f-w-600">
                      Full Name
                    </label>
                    <input
                      type="text"
                      className="form-control text-muted f-w-400"
                      id="input-fullname"
                      name="fullname"
                      placeholder="Enter fullname"
                      ref={register()}
                      disabled={mode === "update"}
                    />
                  </div>
                  <div className="form-group col-sm-12">
                    <label htmlFor="input-email" className="m-b-5 f-w-600">
                      Email
                    </label>
                    <div className="input-group">
                      <input
                        type="email"
                        className="form-control text-muted f-w-400"
                        id="input-email"
                        name="email"
                        placeholder="Enter email address"
                        ref={register({
                          required: true,
                          pattern: /^[a-z0-9](\.?[a-z0-9]){1,}@prabhubank.com$/,
                        })}
                        required
                        disabled={mode === "update"}
                      />
                    </div>
                    {errors.email?.type === "required" && (
                      <span className="text-danger">Email is required</span>
                    )}
                    {errors.email?.type === "pattern" && (
                      <span className="text-danger">
                        Please enter a valid prabhu bank email
                      </span>
                    )}
                  </div>

                  {mode === "create" && (
                    <>
                      {/* For creating password */}
                      <div className="form-group col-sm-6">
                        <label
                          htmlFor="input-password"
                          className="m-b-5 f-w-600"
                        >
                          Password
                        </label>
                        <div className="input-group">
                          <input
                            type="password"
                            className="form-control text-muted f-w-400"
                            id="input-password"
                            name="password"
                            placeholder="Enter password"
                            ref={register({
                              required: true,
                              minLength: 8,
                            })}
                          />
                        </div>
                        {errors.password?.type === "required" && (
                          <span className="text-danger">
                            Password is required
                          </span>
                        )}
                        {errors.password?.type === "minLength" && (
                          <span className="text-danger">
                            Password must have at least 8 characters
                          </span>
                        )}
                      </div>
                      <div className="form-group col-sm-6">
                        <label
                          htmlFor="input-confirm-password"
                          className="m-b-5 f-w-600"
                        >
                          Confirm Password
                        </label>
                        <div className="input-group">
                          <input
                            type="password"
                            className="form-control text-muted f-w-400"
                            id="input-confirm-password"
                            name="confirm_password"
                            placeholder="Re-enter password"
                            ref={register({
                              required: true,
                              validate: (value) => value === password,
                            })}
                          />
                        </div>
                        {errors.confirm_password?.type === "required" && (
                          <span className="text-danger">
                            Confirm password is required
                          </span>
                        )}
                        {errors.confirm_password?.type === "validate" && (
                          <span className="text-danger">
                            Password and Confirm Password doesn't match
                          </span>
                        )}
                      </div>
                    </>
                  )}
                </div>

                <div className="form-group col-sm-6">
                  <label htmlFor="input-role" className="m-b-5 f-w-600">
                    Roles
                  </label>
                  <div className="form-check">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="internal-instructor-checkbox"
                      name="is_internalInstructor"
                      ref={register}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="internal-instructor-checkbox"
                    >
                      Internal Instructor
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="external-instructor-checkbox"
                      name="is_externalInstructor"
                      ref={register}
                    />
                    <label className="form-check-label" htmlFor="exampleCheck3">
                      External Instructor
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="hod-checkbox"
                      name="is_staff"
                      ref={register}
                    />
                    <label className="form-check-label" htmlFor="hod-checkbox">
                      HOD
                    </label>
                  </div>
                  <div className="form-check">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="admin-checkbox"
                      name="is_admin"
                      ref={register}
                    />
                    <label
                      className="form-check-label"
                      htmlFor="admin-checkbox"
                    >
                      Admin
                    </label>
                  </div>
                </div>

                {
                  departments && (
                    <div className="form-group col-sm-6">
                      <label htmlFor="input-department" className="m-b-5 f-w-600">
                        Department
                      </label>
                      {departments.map(dept => (
                        <div className="form-check" key={dept.id}>
                          <input
                            type="radio"
                            className="form-check-input"
                            id={`${dept.id}-radio`}
                            name="department"
                            ref={register}
                            value={dept.id}
                          />
                          <label className="form-check-label" htmlFor={`${dept.id}-radio`}>
                            {dept.dept_name}
                          </label>
                        </div>
                      ))}
                    </div>
                  )
                }

                <div className="form-group col-sm-12">
                  <label htmlFor="input-contact-number" className="m-b-5 f-w-600">
                    Contact Number
                  </label>
                  <input
                    type="number"
                    className="form-control text-muted f-w-400"
                    id="input-contact-number"
                    name="contact_number"
                    ref={register({ minLength: 7, maxLength: 10 })}
                    placeholder="Enter contact number"
                  />
                  {["minLength", "maxLength"].includes(
                    errors.contact_number?.type
                  ) && (
                      <span className="text-danger">
                        Contact number must have 7 to 10 characters
                      </span>
                    )}
                </div>
                <div className="form-group col-sm-12 d-flex justify-content-between">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={isSaveLoading}
                  >
                    Save
                    {isSaveLoading && (
                      <div
                        className="spinner-border spinner-border-sm ml-2"
                        role="status"
                      >
                        <span className="sr-only">Loading...</span>
                      </div>
                    )}
                  </button>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={onClose}
                  >
                    Close
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserForm;
