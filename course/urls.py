from django.urls import path, include
from .views import *
from rest_framework import routers

router=routers.DefaultRouter()
router.register('instructor', InstructorView, basename='instructor')
router.register('category', CategoryView, basename='category')
router.register('sub_category', SubCategoryView, basename='sub_category')
router.register('course', CourseView, basename='course')
router.register('lesson', LessonView, basename='lesson')
router.register('lesson_document', LessonVideoDocumentView, basename='lesson_document')
router.register('syllabus', SyllabusView, basename='syllabus')



urlpatterns = [
	path('', include(router.urls)),
]