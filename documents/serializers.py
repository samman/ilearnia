from rest_framework.exceptions import ValidationError

from .models import Documents, Zoom, Team
from rest_framework import serializers


class DocumentsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Documents
		fields = '__all__'


class TeamSerializer(serializers.ModelSerializer):
	id = serializers.IntegerField(required=False)
	class Meta:
		model=Team
		fields=['id','created_on','created_by','name','agenda','description','department','members']
		extra_kwargs = {
			'id':{'read_only':True},
			'created_by': {'read_only': True},
			'created_on': {'read_only': True},

		}


class ZoomSerializer(serializers.ModelSerializer):
	# team=TeamSerializer()

	class Meta:
		model=Zoom
		fields=['id','created_on','created_by','meeting_id','topic','agenda',
				'start_time','meeting_type','timezone','start_url','join_url','duration','schedule_for','occurance_id','team']

		extra_kwargs = {
			'created_by': {'read_only': True},
			'meeting_id':{'read_only': True},
			'start_url':{'read_only': True},
			'join_url': {'read_only': True},
			'occurance_id':{'read_only':True},

		}


	def validate(self, attrs):
		"""
		VALIDATING START_TIME FEILD ACCORDING TO MEETING TYPE
		i.e. SCHEDULED MEETING TYPE SHOULD MENTION START_TIME ATTRS/FIELD
		"""
		if attrs["meeting_type"]=="Instant" or attrs["meeting_type"]=="Recurring Instant":
			try:
				if attrs["start_time"]!="":
					raise ValidationError("Start time should not be declare if meeting is not schedule type")
			except :
				pass

		elif attrs["meeting_type"]=="Scheduled" or attrs["meeting_type"]=="Recurring Scheduled":

				if 'start_time'not in attrs or attrs["start_time"]=="":
					raise ValidationError("Start time should be declared for schedule type meeting")
		else:
			pass

		return attrs


	# def create(self, validated_data):
	# 	"""
	# 	OPT TEAM OBJECTS 
	# 	PASSING TEAM/DEPARTMENT OBJECTS WHILE CREATING ZOOM MEETING
	# 	"""
	# 	team_validated_data=validated_data.pop('team')

	# 	if 'id' not in team_validated_data:
	# 		memb=team_validated_data.pop('members')
	# 		dep=team_validated_data.pop('department')

	# 		t=Team.objects.create(**team_validated_data)

	# 		for d in dep:
	# 			t.department.add(d.id)

	# 		for m in memb:
	# 			t.members.add(m.id)
				
	# 		t.save()
	# 	else:
	# 		t=Team.objects.get(id=team_validated_data['id'])
	# 	zoom=Zoom.objects.create(**validated_data,team=t)
	# 	return zoom



	# def update(self, instance, validated_data):

	# 	"""
	# 	UPDATING TEAM MEMBERS AND DEPARTMENT WHILE UPDATING ZOOM OBJETCS
	# 	"""
	
	# 	team=validated_data.pop('team')
	# 	instance.save()

		
	# 	if Team.objects.filter(id=team["id"]).exists():
	# 		t=Team.objects.get(id=team["id"])
	# 		t.name=team.get('name',t.name)
	# 		t.agenda=team.get('agenda',t.agenda)
	# 		t.description=team.get('description',t.description)
	# 		depart=team.get('department')
	# 		# t.members_set=team.get('members',t.members)
	# 		if depart != None:
	# 			t.department.set([])
	# 			for dep in depart:
	# 				t.department.add(dep.id)
	# 		else:
	# 			t.department=team.get('department',t.department)

	# 		mem=team.get('members')
	# 		if mem !=None:
	# 			t.members.set([])
	# 			for m in mem:
	# 				t.members.add(m.id)
	# 		else:
	# 			t.members=team.get('department',t.members)

	# 		t.save()
	# 	return instance


class UserHostMeetingSerializer(serializers.ModelSerializer):
	"""
	DATA TO PROVIDE FOR ONLY SCHEDULED FOR USER i.e. HOST USER
	"""
	team=TeamSerializer()
	class Meta:
		model=Zoom
		fields=['created_on','meeting_id','topic','agenda','start_time','meeting_type','start_url','duration','occurance_id','team']
		extra_kwargs = {
			'created_by': {'read_only': True},
			'meeting_id':{'read_only': True},
			'start_url':{'read_only': True},
			'join_url': {'read_only': True},
			'occurance_id':{'read_only':True},

		}
