import { useContext } from "react";
import { APIErrorContext } from "./contexts/APIError";

export const capitalize = (str) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

export const CatchAsync = (fn, callbackFunction) => {
  const { addError } = useContext(APIErrorContext);
  return (...params) => {
    fn(...params).catch((err) => {
      addError(err);
      if(callbackFunction) callbackFunction(err);
    });
  };
};
