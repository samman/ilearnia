import React, { useState, useContext, useEffect } from "react";

// Components
import LoginForm from "./LoginForm";
import OTPForm from "../../components/OTPForm";

import { otp } from "../../api/auth";
import { login, resendVerification } from "../../api/auth";

import { AuthContext } from "../../contexts/Auth";
import { Redirect } from "react-router-dom";

// Assets
import logo from "../../assets/img/logo/logo.svg";

function Login() {
  const [page, setPage] = useState(1);
  const [errorFromServer, setErrorFromServer] = useState(null);
  const [credentials, setCredentials] = useState({ email: "", password: "" });
  const [isLoading, setIsLoading] = useState(false);
  const [isOptLoading, setIsOtpLoading] = useState(false);

  const { setIsLoggedIn } = useContext(AuthContext);

  const handleLoginFromSubmit = async (data) => {
    try {
      setCredentials(data);
      setIsLoading(true);
      const res = await login(data);
      localStorage.setItem("jwt_token", res.data.token);
      setIsLoggedIn(true);
      return <Redirect to="/dashboard" />;
    } catch (err) {
      setIsLoading(false);
      setErrorFromServer(err.response?.data?.error);
    }
  };

  const handleOTPSubmit = async (otpcode) => {
    try {
      setIsOtpLoading(true);
      const res = await otp(otpcode);
      setIsOtpLoading(false);
      localStorage.setItem("jwt_token", res.data.token);
      setIsLoggedIn(true);
      return <Redirect to="/dashboard" />;
    } catch (err) {
      setIsOtpLoading(false);
      setErrorFromServer(err.response.data.detail || err.response.data.error);
    }
  };

  const resendCode = async () => {
    try {
      await resendVerification(credentials.email);
    } catch (err) {
      setErrorFromServer(err.response.data.error);
    }
  };

  useEffect(() => {
    setErrorFromServer(null);
  }, [page]);

  return (
    <div className="Login container">
      <div className="row justify-content-center">
        <div className="col-xl-10 col-lg-12 col-md-9">
          <div className="card o-hidden border-0 shadow-lg my-5">
            <div className="card-body p-0">
              <div className="row">
                <div className="col-lg-6 d-none d-lg-block bg-login-image">
                  <img src={logo} alt="logo" className="w-100-p h-100-p"></img>
                </div>
                <div className="col-lg-6">
                  {errorFromServer ? (
                    <div
                      className="bg-primary w-75-p mx-auto text-center text-light mt-4 p-2"
                      style={{ borderRadius: "4px" }}
                    >
                      {errorFromServer == "Please verify your email" ? (
                        <div>
                          {errorFromServer}{" "}
                          <button
                            onClick={resendCode}
                            className="btn btn-warning btn-sm"
                          >
                            Resend
                          </button>
                        </div>
                      ) : (
                        errorFromServer
                      )}
                    </div>
                  ) : (
                    ""
                  )}
                  <div className="p-5">
                    {page === 1 && (
                      <>
                        <div className="text-center">
                          <h1 className="h4 text-gray-900 mb-4">
                            Welcome To Prabhu Bank
                          </h1>
                        </div>
                        <LoginForm
                          onSubmit={handleLoginFromSubmit}
                          isLoading={isLoading}
                        />
                      </>
                    )}
                    {page === 2 && (
                      <>
                        <div className="text-center">
                          <h1 className="h4 text-gray-900 mb-4">
                            Enter OTP Code
                          </h1>
                        </div>
                        <OTPForm
                          onGoBack={() =>
                            setPage((currentPage) => currentPage - 1)
                          }
                          onOTPSubmit={handleOTPSubmit}
                          isOptLoading={isOptLoading}
                        />
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
