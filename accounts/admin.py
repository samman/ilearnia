from django.contrib import admin
from .models import User, EmailOTP, Department

# Register your models here.
admin.site.register(User)
admin.site.register(EmailOTP)
admin.site.register(Department)
