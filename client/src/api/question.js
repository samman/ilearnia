import axios from "axios";

function getQuestion(questionId) {
  const token = localStorage.getItem("jwt_token");
  return axios.get(
    `${process.env.REACT_APP_API_BASE_URL}/api/question/${questionId}/`,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function createQuestions(data) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(
    `${process.env.REACT_APP_API_BASE_URL}/api/question/`,
    data,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function getExamQuestions(examId) {
  const token = localStorage.getItem("jwt_token");
  return axios.get(
    `${process.env.REACT_APP_API_BASE_URL}/api/examquestion/${examId}/particular/`,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function createExamQuestion(data) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(
    `${process.env.REACT_APP_API_BASE_URL}/api/examquestion/`,
    data,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function uploadCSV(data) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/csv/`, data, {
    headers: {
      authorization: `Token ${token}`,
    },
  });
}

function submitAnswer(data) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(
    `${process.env.REACT_APP_API_BASE_URL}/api/examuser/`,
    data,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

export {
  createQuestions,
  createExamQuestion,
  uploadCSV,
  getExamQuestions,
  getQuestion,
  submitAnswer,
};
