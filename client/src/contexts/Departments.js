import { createContext, useEffect, useState } from "react";
import { getAllDepartments } from "../api/department";
import { CatchAsync } from "../util";

export const DepartmentsContext = createContext();

export const DepartmentsContextProvider = (props) => {
  const [departments, setDepartments] = useState([]);
  const [isDepartmentsManipulated, setIsDepartmentsManipulated] = useState(
    true
  );
  const [isDepartmentsLoading, setIsDepartmentsLoading] = useState(false);

  useEffect(CatchAsync(async () => {
      if(isDepartmentsManipulated){
          setIsDepartmentsLoading(true);
          const res = await getAllDepartments();
          setDepartments(res.data.results);
          setIsDepartmentsLoading(false);
      }
  }), [isDepartmentsManipulated]);

  return (
    <DepartmentsContext.Provider
      value={{
        departments,
        setDepartments,
        isDepartmentsManipulated,
        setIsDepartmentsManipulated,
        isDepartmentsLoading,
        setIsDepartmentsLoading,
      }}
    >
      {props.children}
    </DepartmentsContext.Provider>
  );
};
