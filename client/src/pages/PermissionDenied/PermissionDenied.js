import React from "react";
import "./PermissionDenied.css";

const PermissionDenied = () => {

    return(
        <div id="main">
    	    <div class="fof">
        		<h1>Permission Denied</h1>
    	    </div>
        </div>
    )

}

export default PermissionDenied;