import React, { useState, useContext, useEffect } from "react";
import { useForm, useFieldArray } from "react-hook-form";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { DepartmentsContext, QuestionLevelContext } from "../../../contexts";
import { createExam } from "../../../api/exam";
import {
  createQuestions,
  createExamQuestion,
  uploadCSV,
} from "../../../api/question";
import ExamDetailsForm from "./ExamDetailsForm";
import "./CreateExamForm.scss";
import ExamQuestionsForm from "./ExamQuestionsForm";
import { CatchAsync } from "../../../util";
import ExamQuestionsCSVForm from "./ExamQuestionsCSVForm";

const initQuestion = {
  question: "",
  correct_answer: "",
  choise1: "",
  choise2: "",
  choise3: "",
  choise4: "",
  choice_type: "",
  level: "",
  concise_description: "",
};

const CreateExamForm = ({ exam }) => {
  const history = useHistory();
  const [page, setPage] = useState(1);
  const [isSaveLoading, setIsSaveLoading] = useState(false);
  const { control, register, errors, handleSubmit, trigger, watch } = useForm({
    defaultValues: exam,
  });
  const [mode, setMode] = useState("csv");
  const {
    fields: questions,
    append: appendQuestion,
    remove: removeQuestion,
  } = useFieldArray({
    control,
    name: "questions",
  });
  const departmentsContextValues = useContext(DepartmentsContext)
  const { departments: allDepartments } = departmentsContextValues ? departmentsContextValues : [];
  const questionLevelContextValues = useContext(QuestionLevelContext);
  const { questionLevels } = questionLevelContextValues ? questionLevelContextValues : [];

  const moveToSecondPage = async () => {
    const fieldsToTrigger = ["name", "department"];
    if (await trigger(fieldsToTrigger)) setPage(2);
  };

  const onSubmit = (row) => {
    if (mode === "csv") createWithCSV(row);
    if (mode === "form") createWithForm(row);
  };

  const createWithCSV = CatchAsync(
    async (row) => {
      const data = { name: row.csv[0].name, upload: row.csv[0] };
      const formData = new FormData();
      for (let key in data) {
        if (data[key]) formData.append(key, data[key]);
      }
      setIsSaveLoading(true);
      const res = await uploadCSV(formData);
      const csvId = res.data.id;
      const department=typeof row.department === "string" ? [row.department] : row.department;
      await createExam({ ...row, csv_file: csvId, department });
      setIsSaveLoading(false);
      Swal.fire({
        icon: "success",
        title: "Exam successfully created",
        showConfirmButton: false,
        timer: 1500,
      });
      history.push("/exams");
    },
    () => {
      setIsSaveLoading(false);
    }
  );

  const createWithForm = CatchAsync(
    async (row) => {
      setIsSaveLoading(true);
      const examRes = await createExam(row);
      const examResId = examRes.data.id;
      const questionResIds = [];
      for (let question of row.questions) {
        const res = await createQuestions({
          ...question,
          correct_answer: [question.correct_answer],
        });
        questionResIds.push(res.data.id);
      }
      for (let questionResId of questionResIds) {
        await createExamQuestion({ exam: examResId, question: questionResId });
      }
      setIsSaveLoading(false);
      Swal.fire({
        icon: "success",
        title: "Exam successfully created",
        showConfirmButton: false,
        timer: 1500,
      });
      history.push("/exams");
    },
    () => {
      setIsSaveLoading(false);
    }
  );

  const moveToFirstPage = () => {
    setPage(1);
  };

  const onCancel = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will loose all entries!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.isConfirmed) {
        history.goBack();
      }
    });
  };

  return (
    <div className="container mt-sm-5 my-1">
      <div className="container mt-5">
        <div className="d-flex justify-content-center row">
          <div className="col-md-10 col-lg-10">
            <div className="border shadow">
              <div className="p-3 border-bottom">
                <form className="exam-form" onSubmit={handleSubmit(onSubmit)}>
                  <ExamDetailsForm
                    register={register}
                    errors={errors}
                    departments={allDepartments}
                    history={history}
                    moveToSecondPage={moveToSecondPage}
                    visible={page === 1}
                    onCancel={onCancel}
                  />
                  {mode === "form" && (
                    <ExamQuestionsForm
                      register={register}
                      questions={questions}
                      errors={errors}
                      remove={removeQuestion}
                      append={appendQuestion}
                      initQuestion={initQuestion}
                      moveToFirstPage={moveToFirstPage}
                      questionLevels={questionLevels}
                      watch={watch}
                      visible={page === 2}
                      isSaveLoading={isSaveLoading}
                      onOpenUploadCSV={() => setMode("csv")}
                    />
                  )}
                  {mode === "csv" && (
                    <ExamQuestionsCSVForm
                      isCSVUploadLoading={isSaveLoading}
                      register={register}
                      watch={watch}
                      errors={errors}
                      moveToFirstPage={moveToFirstPage}
                      visible={page === 2}
                      onOpenExamForm={() => setMode("form")}
                    />
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreateExamForm;
