import React, { useContext, useEffect, useState } from "react";
import {
    getAllDepartments,
    createDepartment,
    removeDepartment,
    updateDepartment,
} from "../../api/department";
import DepartmentTable from "./DepartmentTable";
import Dialog from "../../components/Dialog";
import DepartmentForm from "./DepartmentForm";
import Searchbar from "../../components/Searchbar";
import { CatchAsync } from "../../util";
import { AuthContext, DepartmentsContext } from "../../contexts";

const initDepartment = {
    dept_name: ""
};

const searchOptions = [
    {
        name: "Department Name",
        selector: "dept_name",
    }
];

const DepartmentList = () => {
    const { loggedInUser } = useContext(AuthContext);
    const [rows, setRows] = useState([]);
    const [isDialogOpen, setDialogOpen] = useState(false);
    const [isDeptListManipulated, setDeptListManipulated] = useState(true);
    const [selected, setSelected] = useState(initDepartment);
    const [filterBy, setFilterBy] = useState(searchOptions[0]);
    const [searchTerm, setSearchTerm] = useState("");
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [totalRows, setTotalRows] = useState(0);
    const [isDeptDetailLoading, setisDeptDetailLoading] = useState(false);
    const [isSaveLoading, setisSaveLoading] = useState(false);
    const { setIsDepartmentsManipulated } = useContext(DepartmentsContext);

    useEffect(
        CatchAsync(
            async () => {
                if (isDeptListManipulated) {
                    const params = {
                        search: searchTerm,
                        filter_by: filterBy.selector,
                        limit: perPage,
                        offset: (page - 1) * perPage,
                    };
                    setisDeptDetailLoading(true);
                    const res = await getAllDepartments(
                        params
                    );
                    setRows(res.data.results);
                    setIsDepartmentsManipulated(res.data.results);
                    setTotalRows(res.data.count);
                    setisDeptDetailLoading(false);
                }
                setDeptListManipulated(false);
            },
            () => setisDeptDetailLoading(false)
        ),
        [isDeptListManipulated]
    );

    const openCreateDialog = () => {
        openDialog(initDepartment);
    };

    const openDialog = (row) => {
        setSelected(row);
        setDialogOpen(true);
    };

    const closeDialog = () => {
        setDialogOpen(false);
        setSelected(initDepartment);
    };

    const create = CatchAsync(
        async (row) => {
            setisSaveLoading(true);
            await createDepartment(row);
            setisSaveLoading(false);
            setDialogOpen(false);
            setDeptListManipulated(true);
        },
        () => {
            setisSaveLoading(false);
            setDialogOpen(false);
        }
    );

    const update = CatchAsync(
        async (row) => {
            setisSaveLoading(true);
            await updateDepartment(row.id, row);
            setisSaveLoading(false);
            setDialogOpen(false);
            setDeptListManipulated(true);
        },
        () => {
            setisSaveLoading(false);
            setDialogOpen(false);
        }
    )

    const remove = CatchAsync(async (row) => {
        await removeDepartment(row.id);
        setDeptListManipulated(true);
    });

    const handleFilter = (term) => {
        setSearchTerm(term);
        setDeptListManipulated(true);
    };

    const handleFilterBy = (option) => {
        setFilterBy(option);
        setDeptListManipulated(true);
    };

    // useEffect(()=>{
    //   setisDeptDetailLoading(true);

    //   const params = {
    //     search: searchTerm,
    //     filter_by: filterBy.selector,
    //     limit: perPage,
    //     offset: (page - 1) * perPage,
    //   };

    //   getAllDepartments(localStorage.getItem("jwt_token"), params).then((res) => {
    //     setRows(prepareData(res.data.results));
    //     setTotalRows(res.data.count);
    //   });

    //   setisDeptDetailLoading(false);  
    // }, [page])

    const handlePageChange = (page) => {
        setPage(page);
        setDeptListManipulated(true);
    };

    const handlePerRowsChange = async (newPerPage, page) => {
        setisDeptDetailLoading(true);

        const params = {
            search: searchTerm,
            filter_by: filterBy.selector,
            limit: newPerPage,
            offset: (page - 1) * newPerPage,
        };

        getAllDepartments(params).then((res) => {
            setRows(res.data.results);
            setTotalRows(res.data.count);
        });

        setPerPage(newPerPage);

        setisDeptDetailLoading(false);
    };

    return (
        <div className="user-list">
            <Dialog
                open={isDialogOpen}
                handleClose={closeDialog}
                additionalClass="form-modal"
            >
                <DepartmentForm
                    department={selected}
                    onSubmit={selected.id ? update : create}
                    onClose={closeDialog}
                    isSaveLoading={isSaveLoading}
                />
            </Dialog>
            <div className="card shadow mb-4">
                <div className="card-header py-3">
                    <Searchbar
                        options={searchOptions}
                        selected={filterBy.name}
                        onChange={handleFilter}
                        onFilterChange={handleFilterBy}
                    />
                    <button
                        className="btn btn-primary btn-circle btn tr-sticky-add-btn"
                        onClick={openCreateDialog}
                    >
                        <i className="fas fa-plus"></i>
                    </button>
                </div>
                <div className="card-body">
                    <DepartmentTable
                        isDeptDetailLoading={isDeptDetailLoading}
                        handlePerRowsChange={handlePerRowsChange}
                        handlePageChange={handlePageChange}
                        rows={rows}
                        perPage={perPage}
                        onDelete={remove}
                        onRowClicked={(row) => openDialog(row)}
                        onClose={closeDialog}
                        totalRows={totalRows}
                        loading={isDeptDetailLoading}
                        loggedInUser={loggedInUser}
                    />
                </div>
            </div>
        </div>
    );
};

export default DepartmentList;
