from django.db import models
from .validators import validate_file_extension ,validate_image_extension
from django.contrib.auth import get_user_model
import requests, PyPDF2, io
from django.contrib.sites.shortcuts import get_current_site
User=get_user_model()

# Create your models here.
class Author(models.Model):
    first_name=models.CharField(max_length=50)
    last_name=models.CharField(max_length=50)


    def __str__(self):
        return self.first_name + " " + self.last_name

class Genre(models.Model):
    name=models.CharField(max_length=50,unique=True)

    def __str__(self):
        return self.name

class Language(models.Model):
    name=models.CharField(max_length=50,unique=True,default="English")

    def __str__(self):
            return self.name

class LevelDifficulty(models.Model):
    name=models.CharField(max_length=50,unique=True,default="Easy")

    def __str__(self):
            return self.name

class Book(models.Model):
    title=models.CharField(max_length=50)
    summary=models.TextField(max_length=500,null=True,blank=True)
    description=models.TextField(null=True,blank=True)
    thumbnail=models.ImageField(upload_to='library/thumbnail/%Y/%m/%d/',validators=[validate_image_extension],null=True,blank=True)
    author=models.ForeignKey(Author,on_delete=models.CASCADE)
    genre=models.ForeignKey(Genre,on_delete=models.CASCADE)
    Language=models.ForeignKey(Language,on_delete=models.CASCADE)
    level=models.ForeignKey(LevelDifficulty,on_delete=models.CASCADE,null=True)
    upload_files=models.FileField(upload_to='library/file/%Y/%m/%d/',validators=[validate_file_extension],null=True,blank=True)
    published_date=models.DateField(auto_now_add=True)
    updated_date=models.DateField(null=True,blank=True)
    pages=models.IntegerField(default=0)

    # def save(self, *args, **kwargs): 
    #     # import pdb;pdb.set_trace()
    #     # current_site=get_current_site(self.request).domain
    #     url='http://127.0.0.1:8000'+ '/media/library/file/2021/02/24/' +self.upload_files.name
    #     # import pdb;pdb.set_trace()
    #     response = requests.get(url)
    #     import pdb;pdb.set_trace()
    #     with io.BytesIO(response.content) as open_pdf_file:
    #         read_pdf = PyPDF2.PdfFileReader(open_pdf_file)
    #         num_pages = read_pdf.getNumPages()
    #     self.pages = num_pages
    #     super(Book, self).save(*args, **kwargs)
    
    

    def __str__(self):
            return self.title

class Bookmark(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    book=models.ManyToManyField(Book)

    def __str__(self):
            return str(self.user)

