import React from "react";
import { useHistory } from "react-router-dom";

function VerificationSuccessful({handleBackToLogin}) {
  const history = useHistory();

  return (
    <div className="w-100-p h-100-p text-center">
      <i
        className="fas fa-check-circle text-success mt-4"
        style={{ fontSize: "200px" }}
      ></i>
      <h1 className="m-4">Verification Successful</h1>
      <button
        className="btn btn-primary"
        onClick={handleBackToLogin}
      >
        Login
      </button>
    </div>
  );
}

export default VerificationSuccessful;
