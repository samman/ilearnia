import { http } from "../http";

export const getAllUsers = (filterBy = '', searchTerm = '', page = 1, perPage = 10) => {
  let params = {};
  if (filterBy && searchTerm) {
    params.filterBy = filterBy;
    params.searchTerm = searchTerm;
  }
  if (page) {
    params.page = page
    params.perPage = perPage
  }
  if (perPage) {
    params.perPage = perPage
  }
  return http.get("/api/users", {params});
};

export const getUser = (id) => {
  return http.get(`/users/${id}`);
};

export const createUser = (data) => {
  return http.post("/users", data);
};

export const updateUser = (id, data) => {
  return http.put(`/users/${id}`, data);
};

export const removeUser = (id) => {
  return http.delete(`/users/${id}`);
};