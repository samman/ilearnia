import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { AuthContext } from "../contexts/Auth";

import Dropdown from "../components/Dropdown";

const Navbar = () => {
  const { loggedInUser, setIsLoggedIn } = useContext(AuthContext);
  const history = useHistory();

  const handleLogout = () => {
    localStorage.removeItem("jwt_token");
    setIsLoggedIn(false);
    return <Redirect to="/login" />;
  };

  const redirectToProfile = () => {
    history.push({
      pathname: `/users/${loggedInUser.id}`,
      state: {
        user: loggedInUser,
      },
    });
  };

  return (
    <div>
      <nav className="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
        <ul className="navbar-nav ml-auto">
          {loggedInUser ? (
            <Dropdown
              title={loggedInUser?.email}
              items={[
                {
                  id: "profile-item",
                  label: "View Profile",
                  action: redirectToProfile,
                  iconClass: "fas fa-user-alt fa-sm fa-fw mr-2 text-gray-400",
                },
                {
                  id: "change-password-item",
                  label: "Change Password",
                  action: () => history.push("/change_password"),
                  iconClass: "fas fa-key fa-sm fa-fw mr-2 text-gray-400",
                },
                {
                  id: "logout-item",
                  label: "Logout",
                  action: handleLogout,
                  iconClass:
                    "fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400",
                },
              ]}
              image={loggedInUser?.image && `${process.env.REACT_APP_API_BASE_URL}${loggedInUser.image}`}
            />
          ) : (
            <button className="btn btn-light" onClick={handleLogout}>
              Logout
            </button>
          )}
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
