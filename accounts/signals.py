from django.db import models
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver
from django.utils import timezone

import logging
log = logging.getLogger(__name__)

class UserLog(models.Model):
	"""
	models for user Log i.e. Login, Logout
	"""
	action = models.CharField(max_length=255)
	ip = models.GenericIPAddressField(null=True)
	email = models.CharField(max_length=255, null=True)
	last_login = models.DateTimeField(null=True, blank=True)
	last_logout = models.DateTimeField(null=True, blank=True)
	user_id = models.CharField(max_length=255, null=True)


	def __str__(self):
		return self.email

def get_client_ip(request):
	x_forwarded_for = request.META.get('HTTP_X_REAL_IP')
	if x_forwarded_for:
		ip = x_forwarded_for.split(', ')[0]
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip

@receiver(user_logged_in)
def user_logged_in_callback(sender, request, user, **kwargs):

	user_id = request.user.id
	ip = get_client_ip(request)
	email = request.user.email
	now = timezone.now()
	UserLog.objects.create(action='user_logged_in', user_id=user_id, ip=ip, email=email, last_login=now)

@receiver(user_logged_out)
def user_logged_out_callback(sender, request, user, **kwargs):
	user_id = request.user.id
	ip = get_client_ip(request)
	email = request.user.email
	now = timezone.now()
	UserLog.objects.create(action='user_logged_out', user_id=user_id, ip=ip, email=email, last_logout=now)

