from django.urls import path, include
from rest_framework import routers

from .views import *


router=routers.DefaultRouter()
router.register('exam',ExamView,basename='exam')
router.register('question',QuestionAnswerView,basename='question')
router.register('examuser',ExamUserView,basename='examuser')
router.register('result',ResultView,basename='result')
router.register('level',LevelView,basename='level')
router.register('examquestion',ExamQuestionView,basename='examquestion')
router.register('csv',CsvView,basename='csv')


urlpatterns = [
    path('', include(router.urls)),
]