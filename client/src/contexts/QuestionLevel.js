import React, { createContext, useState, useEffect } from "react";
import { getAllQuestionLevels } from "../api/level";
import { CatchAsync } from "../util";

export const QuestionLevelContext  = createContext();

export const QuestionLevelContextProvider = (props) => {
    const [questionLevels, setQuestionLevels] = useState([]);
    const [isQuestionLevelsManipulated, setIsQuestionLevelsManipulated] = useState(
      true
    );
    const [isQuestionLevelsLoading, setIsQuestionLevelsLoading] = useState(false);
  
    useEffect(CatchAsync(async () => {
        if(isQuestionLevelsManipulated){
            setIsQuestionLevelsLoading(true);
            const res = await getAllQuestionLevels();
            setQuestionLevels(res.data.results);
            setIsQuestionLevelsLoading(false);
        }
    }), [isQuestionLevelsManipulated]);
  
    return (
      <QuestionLevelContext.Provider
        value={{
          questionLevels,
          setQuestionLevels,
          isQuestionLevelsManipulated,
          setIsQuestionLevelsManipulated,
          isQuestionLevelsLoading,
          setIsQuestionLevelsLoading,
        }}
      >
        {props.children}
      </QuestionLevelContext.Provider>
    );
}