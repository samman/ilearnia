import React, { useContext } from "react";

// App Component
import App from "./App";

// Contexts
import {
  AuthContextProvider,
  AuthContext,
  APIErrorContextProvider,
  DepartmentsContextProvider,
  TeamsContextProvider,
  QuestionLevelContextProvider,
} from "./contexts";

// Router
import { BrowserRouter } from "react-router-dom";

function AppWrapper() {
  return (
    <AuthContextProvider>
      <BrowserRouter>
        <APIErrorContextProvider>
          <AdminContextProvider>
            <TeamsContextProvider>
              <App />
            </TeamsContextProvider>
          </AdminContextProvider>
        </APIErrorContextProvider>
      </BrowserRouter>
    </AuthContextProvider>
  );
}

const AdminContextProvider = ({ children }) => {
  const { loggedInUser } = useContext(AuthContext);
  if (loggedInUser?.is_admin) {
    return (
      <DepartmentsContextProvider>
        <QuestionLevelContextProvider>{children}</QuestionLevelContextProvider>
      </DepartmentsContextProvider>
    );
  } else {
    return children;
  }
};

export default AppWrapper;
