import React, { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { getAllExaminees } from "../../../api/exam";
import { getUserResult, publishResult } from "../../../api/result";
import ExamParticipantsTable from "./ExamParticipantsTable";
import Searchbar from "../../../components/Searchbar";
import Dialog from "../../../components/Dialog";
import { CatchAsync } from "../../../util";
import Swal from "sweetalert2";

const searchOptions = [
  {
    name: "Email",
    selector: "email",
  },
];

const probableRoles = {
  is_admin: "active",
  is_externalInstructor: "external instructor",
  is_internalInstructor: "internal instructor",
  is_hod: "hod",
};

const ExamParticipants = (props) => {
  // Hooks
  const [rows, setRows] = useState([]);
  const [isUsersListManpulated, setUsersListManipulated] = useState(true);
  const [filterBy, setFilterBy] = useState(searchOptions[0]);
  const [searchTerm, setSearchTerm] = useState("");
  const history = useHistory();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [totalRows, setTotalRows] = useState(0);
  const [isUserDataLoading, setIsUserDataLoading] = useState(false);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [userResults, setUserResults] = useState();

  const examId = props.match.params?.id;
  if (!examId) history.push("/exams");

  const prepareData = (datas) => {
    const rows = datas?.map((data) => {
      // Role
      let userRole = [];
      Object.keys(probableRoles).forEach((prole) => {
        data[prole] && userRole.push(probableRoles[prole]);
      });
      !userRole.length && userRole.push("learner");
      const role = userRole.join(", ");

      // Phone Numbers
      return { ...data, role };
    });
    return rows;
  };

  useEffect(
    CatchAsync(
      async () => {
        if (isUsersListManpulated) {
          const params = {
            search: searchTerm,
            filter_by: filterBy.selector,
            limit: perPage,
            offset: (page - 1) * perPage,
          };
          setIsUserDataLoading(true);
          const res = await getAllExaminees(examId, params);
          setRows(prepareData(res.data.results));
          setTotalRows(res.data.count);
          setIsUserDataLoading(false);
        }
        setUsersListManipulated(false);
      },
      () => setIsUserDataLoading(false)
    ),
    [isUsersListManpulated]
  );

  const redirectToUserDetails = (row) => {
    history.push({
      pathname: `/users/${row.id}`,
      state: {
        user: row,
      },
    });
  };

  const handleFilter = (term) => {
    setSearchTerm(term);
    setUsersListManipulated(true);
  };

  const handleFilterBy = (option) => {
    setFilterBy(option);
    setUsersListManipulated(true);
  };

  const handlePageChange = (page) => {
    setPage(page);
    setUsersListManipulated(true);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setIsUserDataLoading(true);

    const params = {
      search: searchTerm,
      filter_by: filterBy.selector,
      limit: newPerPage,
      offset: (page - 1) * newPerPage,
    };

    getAllExaminees(examId, params).then((res) => {
      setRows(prepareData(res.data.results));
      setTotalRows(res.data.count);
    });

    setPerPage(newPerPage);

    setIsUserDataLoading(false);
  };

  const showUserScore = CatchAsync(async (user) => {
    const res = await getUserResult(user.id, examId);
    setUserResults(res.data);
    setIsDialogOpen(true);
  });

  const publishUserResult = CatchAsync(async (user) => {
    await publishResult(examId, user.id);
    Swal.fire({
      icon: "success",
      title: `Result for ${user.fullname} has been published`,
      showConfirmButton: false,
      timer: 1500,
    });
  });

  return (
    <div className="user-list">
      <Dialog open={isDialogOpen} handleClose={() => setIsDialogOpen(false)}>
        <div className="px-4">
          {userResults?.length ? (
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Attempt</th>
                  <th scope="col">Score</th>
                </tr>
              </thead>
              <tbody>
                {userResults.map((result, index) => (
                  <tr key={`row-${index}`}>
                    <th scope="row">{index + 1}</th>
                    <td>{result.score}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          ) : (
            <div className="p-4 text-center">
              <h3 className="text-primary">User has not yet taken the exam</h3>
            </div>
          )}
          <button
            className="btn btn-primary my-2 float-right"
            onClick={() => setIsDialogOpen(false)}
          >
            close
          </button>
        </div>
      </Dialog>
      <div className="card shadow mb-4">
        <div className="card-header py-3">
          <Searchbar
            options={searchOptions}
            selected={filterBy.name}
            onChange={handleFilter}
            onFilterChange={handleFilterBy}
          />
        </div>
        <div className="card-body">
          <ExamParticipantsTable
            isUserDataLoading={isUserDataLoading}
            handlePerRowsChange={handlePerRowsChange}
            handlePageChange={handlePageChange}
            rows={rows}
            perPage={perPage}
            onRowClicked={redirectToUserDetails}
            totalRows={totalRows}
            loading={isUserDataLoading}
            onViewResult={showUserScore}
            onPublishResult={publishUserResult}
          />
        </div>
      </div>
    </div>
  );
};

export default ExamParticipants;
