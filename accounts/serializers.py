from rest_framework import serializers, exceptions
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate

from .models import UserModifyRecord, Department
from .signals import UserLog
User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','created_date','employee_id','email','fullname', 'password','contact_number','department','image', 'is_active', 'is_staff', 'is_admin', 'is_internalInstructor', 'is_externalInstructor']
        extra_kwargs = {
            'password': {'write_only': True},
            'created_by':{'read_only':True},
            'created_date':{'read_only':True},
            'department':{'read_only':True},
            'is_active':{'read_only':True},
            'is_staff':{'read_only':True},
            'is_admin':{'read_only':True},
            'is_internalInstructor':{'read_only':True},
            'is_externalInstructor':{'read_only':True},
        }

    # def get_extra_kwargs(self):
    #     import pdb;pdb.set_trace()
    #     extra_kwargs = super(UserSerializer, self).get_extra_kwargs()
    #     action = self.context['view'].action
    #     if action in ['update', 'partial_update']:
    #         kwargs = extra_kwargs.get('email', {})
    #         kwargs['read_only'] = True
    #         extra_kwargs['email'] = kwargs
    #     return extra_kwargs

    def validate_image(self, image):
        try:
            if not image.name.endswith(('.jpg','.jpeg','.png')):
                raise exceptions.ValidationError("Only png,jpg and jpeg images are accepted")

            return image
        except:
            pass

    def validate_email(self, email):
        if not email.endswith('@prabhubank.com'):
            raise serializers.ValidationError("Only @prabhubank.com email addresses allowed")

        return email



class UserHodSerializer(UserSerializer):
    class Meta(UserSerializer.Meta):

        fields=UserSerializer.Meta.fields+['branch']

        extra_kwargs = {
            
            'is_internalInstructor':{'read_only':False},
            'is_externalInstructor':{'read_only':False},
        }


class UserAdminSerializer(UserHodSerializer):
    class Meta(UserHodSerializer.Meta):
        fields=UserHodSerializer.Meta.fields+['is_verified']


        extra_kwargs = {
    
            'department':{'read_only':False},
            
            'is_staff':{'read_only':False},
            'is_admin':{'read_only':False},
            'is_verified':{'read_only':False},

        }


class UserModifiySerializer(serializers.ModelSerializer):
    class Meta:
        model=UserModifyRecord
        fields='__all__'



class DepartmentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Department
		fields = ('id', 'dept_name', 'created_date', 'modified_date', )
		read_only_fields = ('id', 'created_date', 'modified_date', )

class EmailVerificationSerializer(serializers.Serializer):
	token = serializers.CharField(max_length=255)

	class Meta:
		model = User
		fields = ['token']

class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255)
    password = serializers.CharField(max_length=255)

    def validate(self, data):
        email = data.get('email')
        password = data.get('password')

        if email and password:
            # import pdb;pdb.set_trace()
            user = authenticate(email=email, password=password)

            if user:
                if user.is_active and user.is_verified:
                    data["user"] = user
                else:
                    msg="Please verify your email"
                    raise serializers.ValidationError(msg)

            else:
                msg="The email and password is not registered"
                raise serializers.ValidationError(msg)
        else:
            msg = "Please provide email and password"
            raise serializers.ValidationError(msg)

        return data
        
class UserLogSerializer(serializers.ModelSerializer):

	class Meta:
		model = UserLog
		fields = "__all__"