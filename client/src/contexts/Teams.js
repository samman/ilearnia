import { createContext, useEffect, useState } from "react";
import { getAllTeams } from "../api/team";
import { CatchAsync } from "../util";

export const TeamsContext = createContext();

export const TeamsContextProvider = (props) => {
  const [teams, setTeams] = useState([]);
  const [isTeamsManipulated, setIsTeamsManinuplate] = useState(true);
  const [isTeamsLoading, setIsTeamsLoading] = useState(false);

  useEffect(CatchAsync(async () => {
    if (isTeamsManipulated) {
      setIsTeamsLoading(true);
      const res = await getAllTeams();
      setTeams(res.data.results);
      setIsTeamsLoading(false);
    }
  }), []);

  return (
    <TeamsContext.Provider
      value={{
        teams,
        setTeams,
        isTeamsManipulated,
        setIsTeamsManinuplate,
        isTeamsLoading,
        setIsTeamsLoading,
      }}
    >
      {props.children}
    </TeamsContext.Provider>
  );
};
