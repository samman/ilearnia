//React imports
import React from "react";
import DataTable from "react-data-table-component";

const MeetingTable = (props) => {
  const handleDelete = (row) => props.onDelete(row);

  const columns = [
    {
      name: "Topic",
      selector: "topic",
      sortable: true,
    },
    {
      name: "Start Time",
      selector: "start_time",
      wrap: true,
      sortable: true,
      // cell: row => new Intl.DateTimeFormat("en-US", { dateStyle: 'full', timeStyle: 'long' }).format(new Date(row.start_time)),
      cell: (row) =>
        row.start_time
          ? row.start_time.split("T").join(" ").split("Z")
          : "Ongoing",
    },
    {
      name: "Duration",
      selector: "duration",
      sortable: true,
      center: true,
    },
    {
      name: "Action",
      button: true,
      minWidth: "30%",
      cell: (row) => (
        <div>
          <button
            className="btn btn-secondary btn-sm btn-icon-split"
            href={row.join_url}
            target="_blank"
            rel="noopener noreferrer"
          >
            <span className="icon text-white-50">
              <i className="fas fa-sign-in-alt"></i>
            </span>
            <span className="text">Join Now</span>
          </button>
          <button
            className="btn btn-primary btn-sm btn-icon-split ml-1"
            href={row.start_url}
            target="_blank"
            rel="noopener noreferrer"
          >
            <span className="icon text-white-50">
              <i className="fas fa-sign-in-alt"></i>
            </span>
            <span className="text">Start Now</span>
          </button>
          {props.loggedInUser?.is_admin && <a
            className="btn btn-primary btn-sm ml-1"
            role="button"
            onClick={() => handleDelete(row)}
          >
            <i className="fas fa-trash"></i>
          </a>}
        </div>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
    },
  ];

  // const handleRowClick = (row) => {
  //   props.openEditDialog(row)
  // }

  return (
    <DataTable
      noHeader={true}
      pagination={true}
      paginationServer={true}
      columns={columns}
      data={props.rows}
      highlightOnHover={true}
      pointerOnHover={true}
      onRowClicked={props.onRowClicked}
      paginationPerPage={props.perPage}
      onChangeRowsPerPage={props.handlePerRowsChange}
      onChangePage={props.handlePageChange}
      progressPending={props.loading}
      paginationTotalRows={props.totalRows}
    />
  );
};

export default MeetingTable;
