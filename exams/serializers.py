from rest_framework import serializers


from .models import *


class CsvSerializer(serializers.ModelSerializer):

    class Meta:
        model=Csv
        fields='__all__'

class LevelSerializer(serializers.ModelSerializer):

    class Meta:
        model=Level
        fields='__all__'


class ExamUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model=Exam
        fields=['id','name','description','exam_type','duration','start_time','result_publish','department']


class ExamSerializer(serializers.ModelSerializer):

    class Meta:
        model=Exam
        fields='__all__'


        extra_kwargs = {
            'created_by':{'read_only':True}
        }



class QuestionAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model=QuestionAnswer
        fields=['id','CHOICE_TYPE','question','choice1','choice2','choice3','choice4','conside','choice_type','level']

class QuestionAnswerAdminSerializer(QuestionAnswerSerializer):
    class Meta(QuestionAnswerSerializer.Meta):
        fields=QuestionAnswerSerializer.Meta.fields+['correct_answer']

class ExamQuestionSerializer(serializers.ModelSerializer):

    class Meta:
        model=ExamQuestion
        fields='__all__'


class ExamUserSerializer(serializers.ModelSerializer):

    class Meta:
        model=ExamUser
        fields=['id','user','question_answer','question','short_description']
        extra_kwargs = {

            'user':{'read_only':True}
        }


class ResultSerializer(serializers.ModelSerializer):

    class Meta:
        model=Result
        fields='__all__'


    # def create(self, validated_data):
    #     user=self.context['request'].user
    #     import pdb; pdb.set_trace();
    #     if not user.is_staff:
    #         result=Result.objects.create(
    #             user=user.id,
    #             **validated_data
    #         )
    #         return Result(**validated_data)
