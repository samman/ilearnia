from rest_framework.test import APITestCase,APIClient
from django.urls import reverse
from faker import Faker
from rest_framework_simplejwt import tokens
from accounts.views import UserLoginOTPView
from ..models import EmailOTP
from requests import Response
import json
import pandas as pd

# from .test_views import TestViews


class TestSetUp(APITestCase):
    def setUp(self):
        
        self.register_url='/api/users/'
        self.login_url='/api/v1/login/'
        self.verify_otp="/api/v1/verify-otp/"
        self.exam_url="/api/exam/"
        self.department_url="/api/department/"
        self.question_url="/api/question/"
        self.examquestion_url="/api/examquestion/"
        self.level_url="/api/level/"
        self.examuser_url="/api/examuser/"
        self.result_url="/api/result/"
        self.csv_url="/api/csv/"

        self.fake=Faker()

        city = pd.DataFrame([['ktm', 'pardesh-3'], ['pkr', 'pardesh-4']], columns=['City', 'State'])
        city.to_csv('city.csv')
        f= open('city.csv','r')
        payload = {}
        payload['content'] = f.read()
        js = json.dumps(payload)
        
       
        

        self.user_data={
            
            # 'username':self.fake.email().split('@')[0],
            'id':str(self.fake.pyint()),
            'email':self.fake.first_name()+'@prabhubank.com',
            'password':self.fake.email(),
            
        }
        self.change_password_url="/api/change-password/"+self.user_data['id']+"/"
        # self.otp_code={
        #     "otpcode" : "508587"
        # }
        new_password=self.fake.email()
        self.change_password={
            'old_password':self.user_data['password'],
            'new_password':new_password,
            'confirm_password':new_password,
        }
        self.department={
            "dept_name":self.fake.first_name(),
        }
        self.departments={
            "dept_name":self.fake.first_name(),
        }
        self.exam={
            "name":self.fake.first_name(),
            "exam_type":"strict",
            "duration":self.fake.pyint(),
            "result_publish": "Automatic",
            "department":[1]
        }
        
        self.exams={
            "name":self.fake.first_name(),
            "exam_type":"strict",
            "duration":self.fake.pyint(),
            "result_publish": "Automatic",
            "department":[3]
        }
        self.examine={
            "name":self.fake.first_name(),
            "exam_type":"strict",
            "duration":self.fake.pyint(),
            "result_publish": "Automatic",
            "department":[5]
        }
        self.examination={
            "name":self.fake.first_name(),
            "exam_type":"strict",
            "duration":self.fake.pyint(),
            "result_publish": "Automatic",
            "department":[4]
        }
        correct=self.fake.first_name()
        self.question={
            "question":self.fake.name(),
            "choice1":self.fake.first_name(),
            "choice2":self.fake.first_name(),
            "choice3":self.fake.first_name(),
            "choice4":correct,
            "correct_answer": [correct]
        }
        self.examquestion={
            "exam": 2,
            "question":1
        }
        self.examquestions={
            "exam":3,
            "question":2
        }
        self.level={
            "name":self.fake.name(),
            "reward":self.fake.pyint()
        }
        self.examuser={
            "question_answer": [self.fake.name()],
            "question": 2,
            "short_description":self.fake.name()
        }
        self.result={
            "exam": 4,
            "user": 10
        }
        self.csv={
            "name": self.fake.name(),
            "upload": f
        }
        # import pdb; pdb.set_trace()
        return super().setUp()
        
        
    
    def tearDown(self):
        return super().tearDown()