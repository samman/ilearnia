// import React from "react";
import React, { useEffect, useState, useContext } from "react";
import {
  createQuestionLevel,
  getAllQuestionLevels,
  removeQuestionLevel,
} from "../../../api/level";
import { CatchAsync } from "../../../util";

import QuestionLevelList from "./QuestionLevelList";
import QuestionLevelForm from "./QuestionLevelForm";

import { QuestionLevelContext } from "../../../contexts";

const initLevel = {
  name: "",
  reward: null,
};

const searchOptions = [
  {
    name: "Level Title",
    selector: "name",
  },
];

const QuestionLevel = (props) => {
  const [rows, setRows] = useState([]);
  const [filterBy, setFilterBy] = useState(searchOptions[0]);
  const [searchTerm, setSearchTerm] = useState("");
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [totalRows, setTotalRows] = useState(0);
  const [isSaveLoading, setIsSaveLoading] = useState(false);
  const {
    isQuestionLevelsManipulated,
    setIsQuestionLevelsManipulated,
    isQuestionLevelsLoading,
    setIsQuestionLevelsLoading,
  } = useContext(QuestionLevelContext);

  useEffect(
    CatchAsync(
      async () => {
        if (isQuestionLevelsManipulated) {
          const params = {
            search: searchTerm,
            filter_by: filterBy.selector,
            limit: perPage,
            offset: (page - 1) * perPage,
          };
          setIsQuestionLevelsLoading(true);
          const res = await getAllQuestionLevels(params);
          setRows(res.data.results);
          setTotalRows(res.data.count);
          setIsQuestionLevelsLoading(false);
        }
        setIsQuestionLevelsManipulated(false);
      },
      () => setIsQuestionLevelsLoading(false)
    ),
    [isQuestionLevelsManipulated]
  );

  // So that the rows will be populated on render
  useEffect(() => setIsQuestionLevelsManipulated(true), [])

  const handleFilter = (term) => {
    setSearchTerm(term);
    setIsQuestionLevelsManipulated(true);
  };

  const handleFilterBy = (option) => {
    setFilterBy(option);
    setIsQuestionLevelsManipulated(true);
  };

  const handlePageChange = (page) => {
    setPage(page);
    setIsQuestionLevelsManipulated(true);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setIsQuestionLevelsLoading(true);

    const params = {
      search: searchTerm,
      filter_by: filterBy.selector,
      limit: newPerPage,
      offset: (page - 1) * newPerPage,
    };

    getAllQuestionLevels(params).then((res) => {
      setRows(res.data.results);
      setTotalRows(res.data.count);
    });

    setPerPage(newPerPage);

    setIsQuestionLevelsLoading(false);
  };

  const create = CatchAsync(
    async (row) => {
      setIsSaveLoading(true);
      await createQuestionLevel(row);
      setIsSaveLoading(false);
      setIsQuestionLevelsManipulated(true);
    },
    () => {
      setIsSaveLoading(false);
    }
  );

  const remove = CatchAsync(async (row) => {
    await removeQuestionLevel(row.id);
    setIsQuestionLevelsManipulated(true);
  });

  return (
    <>
      <div className="row">
        <div className="col-sm-8">
          <QuestionLevelList
            isQuestionLevelsLoading={isQuestionLevelsLoading}
            handlePerRowsChange={handlePerRowsChange}
            handlePageChange={handlePageChange}
            rows={rows}
            perPage={perPage}
            onDelete={remove}
            onRowClicked={(row) => {}}
            totalRows={totalRows}
            loading={isQuestionLevelsLoading}
            searchOptions={searchOptions}
            filterBy={filterBy}
            handleFilter={handleFilter}
            handleFilterBy={handleFilterBy}
            initLevel={initLevel}
            {...props}
          />
        </div>
        <div className="col-sm-4">
          <div className="card m-2 shadow">
            <div className="card-body">
              <h5 className="card-title">Create Level</h5>
              <QuestionLevelForm
                onSubmit={create}
                isSaveLoading={isSaveLoading}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="row bg-primary">
        <button
          type="button"
          className="btn tr-sticky-dialog-close"
          onClick={props.onClose}
        >
          <i className="fas fa-times"></i>
        </button>
      </div>
    </>
  );
};

export default QuestionLevel;
