import React from "react";

function VerificationFailed({ message, handleResend, isResendLoading }) {
  return (
    <div className="w-100-p h-100-p text-center">
      <i
        className="fas fa-times-circle text-primary mt-4"
        style={{ fontSize: "200px" }}
      ></i>
      <h1 className="m-4">Verification Failed</h1>
      <h3 className="font-weight-bold text-info p-4">{message}</h3>
      <button className="btn btn-primary" onClick={handleResend}>
        Resend Verfication Email
        {isResendLoading && (
          <div className="spinner-border spinner-border-sm ml-2" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        )}
      </button>
    </div>
  );
}

export default VerificationFailed;
