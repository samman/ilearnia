import React, { createContext, useState, useEffect } from "react";
import {getUserProfile} from "../api/auth";

export const AuthContext = createContext();

export const AuthContextProvider = props => {

    const [isLoggedIn, setIsLoggedIn] = useState(localStorage.getItem('jwt_token') ? true : false);

    const [loggedInUser, setLoggedInUser ] = useState(null);

    useEffect(() => {
        (async()=>{
            if(isLoggedIn === true){
                const res = await getUserProfile(localStorage.getItem('jwt_token'));
                setLoggedInUser(res.data);
            }
        })()
    }, [isLoggedIn])

    return (
        <AuthContext.Provider value={{isLoggedIn, setIsLoggedIn, loggedInUser, setLoggedInUser}}>
            {props.children}
        </AuthContext.Provider>
    )

}