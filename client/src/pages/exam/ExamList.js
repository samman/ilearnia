import React from "react";
import ExamTable from "./ExamTable";
import Searchbar from "../../components/Searchbar";

const ExamList = (props) => {
  return (
    <div className="user-list">
      <div className="card shadow mb-4">
        <div className="card-header py-3">
          <Searchbar
            options={props.searchOptions}
            selected={props.filterBy.name}
            onChange={props.handleFilter}
            onFilterChange={props.handleFilterBy}
          />
          {props.loggedInUser?.is_admin && (
            <div className="buttons tr-sticky-div d-flex w-20-p justify-content-around">
              <button
                className="btn btn-success btn shadow"
                onClick={props.onLevelClick}
              >
                Question Levels
              </button>
              <button
                className="btn btn-primary btn-circle btn shadow"
                onClick={props.onAddClick}
              >
                <i className="fas fa-plus"></i>
              </button>
            </div>
          )}
        </div>
        <div className="card-body">
          <ExamTable
            isDeptDetailLoading={props.isDeptDetailLoading}
            handlePerRowsChange={props.handlePerRowsChange}
            handlePageChange={props.handlePageChange}
            rows={props.rows}
            perPage={props.perPage}
            onDelete={props.onDelete}
            onViewParticipants={props.onViewParticipants}
            onRowClicked={(row) => {}}
            totalRows={props.totalRows}
            loading={props.isDeptDetailLoading}
            loggedInUser={props.loggedInUser}
            onJoin={props.onJoin}
          />
        </div>
      </div>
    </div>
  );
};

export default ExamList;
