import codecs
import csv

from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from accounts.overide import IsAdminUser, CanView, OwnView
from accounts.serializers import UserSerializer
from .serializers import *


class CsvView(viewsets.ModelViewSet):
    """
        Views for Csv file which return response of csv file check staus

    """

    queryset = Csv.objects.all()
    serializer_class = CsvSerializer
    permission_classes = [IsAdminUser]

    def perform_create(self, serializer):
        """
            overiding the method to validated the csv file data
        """

        data=[]
        csvReader = csv.DictReader(codecs.iterdecode(serializer.validated_data['upload'], 'unicode_escape'))

        for rows in csvReader:
            for val in rows:
                if rows[val]==None:
                    data.append(rows)
                    break

        if len(data)>0:
            raise ValidationError(data)
        else:
            serializer.save()


class LevelView(viewsets.ModelViewSet):
    """
        Define the level of question with it marks.
        Should be same as to upload in csv file.
    """

    queryset = Level.objects.all()
    serializer_class = LevelSerializer

    def get_permissions(self):
        """
            Defining permission to each action requested by user
        """

        if self.action in ['create','list','update','partial_update','delete']:
            permission_classes=[IsAdminUser]

        elif self.action in ['retrieve']:
            permission_classes=[IsAuthenticated]

        else:
            return super().get_permissions()

        return [permissions() for permissions in permission_classes]


class ExamView(viewsets.ModelViewSet):
    """
        Create a exam with defined examineer.
    """

    queryset=Exam.objects.all()
    serializer_class = ExamSerializer


    def get_serializer_class(self):
        """
            overiding the method to response serilaizer according to the action
        """

        if self.action=='update' or self.action=='partial_update':
            return ExamUpdateSerializer
        else:
            return ExamSerializer


    def get_permissions(self):
        """
            Defining permission to each action requested by user
        """

        if self.action in ['create','list','update','partial_update','delete']:
            permission_classes=[IsAdminUser]

        elif self.action in ['retrieve']:
            permission_classes=[CanView]

        else:
            return super().get_permissions()

        return [permissions() for permissions in permission_classes]


    def perform_create(self, serializer):
        """
            overiding the method to create a question objects and associated the question with exam if csv file is being uploaded
        """
        if 'csv_file' in serializer.validated_data:
            if serializer.validated_data['csv_file'] is not None:
                data = []
                csvReader = csv.DictReader(codecs.iterdecode(serializer.validated_data['csv_file'].upload, 'utf-8'))

                for rows in csvReader:

                    if QuestionAnswer.objects.filter(question=rows['Question']).exists():
                        data.append(QuestionAnswer.objects.get(question=rows['Question']))

                    else:
                        question_model=QuestionAnswer(question=rows['Question'],choice1=rows['Choice1'],choice2=rows['Choice2'],\
                                                      choice3=rows['Choice3'],choice4=rows['Choice4'],correct_answer=[rows['Correct_answer']],\
                                                      conside=rows['Concise Description'],choice_type=rows['Type'],level=Level.objects.get(name=rows['Level']))
                        question_model.save()
                        data.append(question_model)

        serializer.save(created_by=self.request.user)
        if serializer.data['csv_file'] is not None:
            for i in range(len(data)):

                examquestion=ExamQuestion(exam=Exam.objects.get(id=serializer.data['id']),question=data[i])
                examquestion.save()


        # mail part to send an email for all the department user selected in exam

        to=list(User.objects.filter(department__in=serializer.data['department']).values_list('email',flat=True))

        # for em in user_email:
        #     to.append(em)

        subject, from_email = 'Exam Information', 'no-reply@prabhubank.com'
        current_site = get_current_site(self.request).domain
        relativelink = '/api/exam/'
        absurl = 'http://' + current_site + relativelink + str(serializer.data['id'])+'/'
        email_body = 'Exam Information' + '\n'
        html_content = email_body + f'<a href="{absurl}">Click Here to see detail of exam </a> '
        msg = EmailMultiAlternatives(subject, html_content, from_email, to)
        msg.attach_alternative(html_content, "text/html")
        msg.send()


    def perform_update(self, serializer):
        """
            overiding the method to send the mail to updated user in examination.
        """
        if 'department' in serializer.validated_data:
            obj=self.get_object()
            exam_depart=list(Exam.objects.get(id=obj.id).department.all())

            depart_obj=serializer.validated_data['department']


            if exam_depart!=depart_obj:

                subject, from_email = 'Exam Information', 'no-reply@prabhubank.com'
                current_site = get_current_site(self.request).domain
                relativelink = '/api/exam/'+str(obj.id)+'/'
                absurl = 'http://' + current_site + relativelink
                email_body = 'Exam Information' + '\n'


                remove=list(set(exam_depart)-set(depart_obj))
                add=list(set(depart_obj)-set(exam_depart))

                if len(remove)>0:

                    old_examuser_mail = list(User.objects.filter(department__in=remove).values_list('email', flat=True))
                    html_content = email_body + f'!!! Your examination has been cancelled or have been removed from the exam '
                    msg = EmailMultiAlternatives(subject, html_content, from_email, old_examuser_mail)
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

                if len(add)>0:
                    new_examuser_mail = list(User.objects.filter(department__in=add).values_list('email', flat=True))
                    html_content = email_body + f'<h3>You have been added in exam</h3>\n<a href="{absurl}">Click Here to see detail of exam </a> '
                    msg = EmailMultiAlternatives(subject, html_content, from_email, new_examuser_mail)
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()

        serializer.save()


    @action(methods=['get'], detail=False, permission_classes=[IsAuthenticated])
    def user(self, request):
        """
            Decorater to view all the enrolled exam  of user.
        """
        exam = Exam.objects.filter(department=self.request.user.department)
        serializers = ExamSerializer(exam, many=True)
        if not exam:
            return Response({'message':'You have not been enrolled in any exam'})
        else:
            return Response(serializers.data)

    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated])
    def exam_user(self, request, pk):
        """
            Decorater to view all the enrolled exam  of user.
        """
        exam = Exam.objects.filter(id=pk)
        user=User.objects.filter(department__in=exam[0].department.all())
        page = self.paginate_queryset(user)
        if page is not None:

            serializers = UserSerializer(page, many=True)
            return self.get_paginated_response(serializers.data)
        else:
            return Response({'message': 'No results found'})


class QuestionAnswerView(viewsets.ModelViewSet):
    """
        return the response of question objects only for Admin user.
    """

    queryset = QuestionAnswer.objects.all()
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        """
        METHOD TO PROVIDE DATA ACCORIDING TO USER TYPE
        """
        user=self.request.user
        
        try:
            if user.is_admin:
                return QuestionAnswerAdminSerializer
            else:
                return QuestionAnswerSerializer
        except:
            return QuestionAnswerSerializer


class ExamQuestionView(viewsets.ModelViewSet):
    """
        return the response of question associated with every exam only for admin user.
    """

    queryset = ExamQuestion.objects.all()
    serializer_class = ExamQuestionSerializer
    permission_classes = [IsAdminUser]


    @action(methods=['get'], detail=True, permission_classes=[IsAuthenticated])
    def particular(self, request, pk):
        """
            Decorater to view all the questionof particular exam.
        """
        user_attempted_obj=ExamUser.objects.filter(user=self.request.user.id)
        exam_question_obj=ExamQuestion.objects.filter(exam=pk)
        exam_attemped_obj=user_attempted_obj.filter(question__in=exam_question_obj)

        if not exam_attemped_obj:
            question = ExamQuestion.objects.filter(exam=pk)
            serializers = ExamQuestionSerializer(question, many=True)

            return Response(serializers.data)
        else:
            return Response({'detail':'Exam already attempted'})


class ExamUserView(viewsets.ModelViewSet):
    """
        View to takes user examination data and make objects of every question attempted by the user
    """

    queryset = ExamUser.objects.all()
    serializer_class = ExamUserSerializer

    def get_permissions(self):
        if self.action in ['create']:
            permission_classes=[IsAuthenticated]

        elif self.action in ['update','partial_update']:
            permission_classes=[OwnView]

        elif self.action in ['retrieve']:
            permission_classes=[CanView]

        elif self.action in ['list']:
            permission_classes=[IsAdminUser]

        else:
            return super().get_permissions()

        return [permissions() for permissions in permission_classes]


    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

    @action(methods=['get'], detail=True, permission_classes=[CanView])
    def particular(self, request, pk):
        """
            Decorater to view all the question attempted by user.
            Here, in 0 index of pk the user ID is passes and
            in 1 index of pk exam Id is passed
        """

        user_attemp_question = ExamUser.objects.filter(user=(pk[0]))
        question=ExamQuestion.objects.filter(exam=pk[2]+pk[3])
        user_exam_question=user_attemp_question.filter(question__in=question)

        serializers = ExamUserSerializer(user_exam_question, many=True)

        if not user_attemp_question:
            return Response({"message": "User hasnot attempted any exam"})
        elif not user_exam_question:
            return Response({"message":"User hasnot attempted this particular type exam"})
        else:
            return Response(serializers.data)


class ResultView(viewsets.ModelViewSet):
    """
        Result view for viewing results
    """


    queryset = Result.objects.all()
    serializer_class = ResultSerializer

    def get_permissions(self):
        if self.action in ['create']:
            permission_classes=[IsAuthenticated]

        elif self.action in ['retrive']:
            permission_classes=[CanView]

        elif self.action in ['list','update','partial_update']:
            permission_classes=[IsAdminUser]

        else:
            return super().get_permissions()

        return [permissions() for permissions in permission_classes]

    def perform_create(self, serializer):

        exam = serializer.validated_data['exam']
        user=serializer.validated_data['user']

        question=ExamQuestion.objects.filter(exam=exam)
        user_obj=ExamUser.objects.filter(user=user)
        exam_attemped=user_obj.filter(question__in=question)


        result_publish=exam.result_publish
        count = 0
        for attemp in exam_attemped:
            level = attemp.question.question.level
            answer=attemp.question_answer
            selection_type=attemp.question.question.choice_type
            right_answer_question=attemp.question.question.correct_answer


            # element_num=len(answer)
            # if selection_type=='Multiple Selection' and element_num<2:
            #     answer.append("")
            #
            # if selection_type=='Multiple Selection':
            #
            #     for user_answer in answer:
            #         for que_answer in right_answer_question:
            #             if user_answer==que_answer:
            #                 count=count+1
            # else:

            for user_answer in answer:
                if user_answer in right_answer_question:

                    marks=level.reward
                    count=count+marks

        if exam.result_publish=='Automatic':
            result_status=True
        else:
            result_status=False

        serializer.save(score=count,result_status=result_status)
        user_mail=User.objects.get(id=serializer.data['user']).email
        if result_publish=='Manual':
            subject, from_email ,to= 'Exam Information', 'no-reply@prabhubank.com',user_mail
            current_site = get_current_site(self.request).domain
            relativelink = '/api/exam/'
            absurl = 'http://' + current_site + relativelink + str(serializer.data['id']) + '/'
            email_body = 'Exam Information' + '\n'
            html_content = email_body + f'Thank You for attending the exam .The result will be shortly published by the head of the department '
            msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()

        else:
            subject, from_email,to = 'Exam Information', 'no-reply@prabhubank.com',user_mail
            current_site = get_current_site(self.request).domain
            relativelink = '/api/result/'
            absurl = 'http://' + current_site + relativelink + str(serializer.data['id']) + '/'
            email_body = 'Exam Information' + '\n'
            html_content = email_body + f'<h1>The total marks obtain is {count}</h1>\n<a href="{absurl}">Click Here to see detail of result </a> '
            msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()




    @action(methods=['get'], detail=False, permission_classes=[IsAdminUser])
    def exam_pending_results(self, request, *args, **kwargs):
        """
            Decorater to fetch the exam whose result are not published yet
        """

        exam_pending_result = Result.objects.filter(result_status=False).values_list('exam',flat=True)
        exam=Exam.objects.filter(id__in=exam_pending_result)
        serializers = ExamSerializer(exam, many=True)

        return Response(serializers.data)



    @action(methods=['get'], detail=True, permission_classes=[IsAdminUser])
    def publish_pending_results(self, request,pk):
        """
            Decorater to publish the result of exam by passing the exam id and sending mail to the user.
        """
       
        exam_pending_result = Exam.objects.get(id=pk)
        pending=Result.objects.filter(exam=exam_pending_result)
        pending_result=pending.filter(result_status=False)
        if pending_result:
            for result in pending_result:
                result.result_status=True
                result.save()

                subject, from_email, to = 'Exam Information', 'no-reply@prabhubank.com', result.user.email
                current_site = get_current_site(self.request).domain
                relativelink = '/api/result/'
                absurl = 'http://' + current_site + relativelink + str(result.id) + '/'
                email_body = 'Exam Information' + '\n'
                html_content = email_body + f'<h1>The total marks obtain is {result.score}</h1>\n<a href="{absurl}">Click Here to see detail of result </a> '
                msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()

            return Response(serializers.data)
        else:

            return Response({"status":"There is no pending result to be published"})


    @action(methods=['get'], detail=False)
    def pending_results(self, request, *args, **kwargs):
        """
            Decorater to view all the unpublish result of user.
        """
        pending_result = Result.objects.filter(result_status=False)
        serializers = ResultSerializer(pending_result, many=True)

        return Response(serializers.data)


    @action(methods=['get'], detail=False)
    def user_result(self, request, *args, **kwargs):
        """
            Decorater to view the published result of requested user.
        """
        result=Result.objects.filter(user=self.request.user.id)
        published_result = result.filter(result_status=True)
        serializers = ResultSerializer(published_result, many=True)

        if not result:
            return Response({"message":"You have no result published yet"})
        else:
            return Response(serializers.data)

    @action(methods=['get'], detail=True)
    def exam_user_result(self, request, pk):
        """
            Decorater to view the published result of particular exam of user.
        """
        result = Result.objects.filter(user=self.request.user.id)
        published_result = result.filter(result_status=True)
        particular_exam=published_result.filter(exam=pk)
        serializers = ResultSerializer(particular_exam, many=True)

        if not result:
            return Response({"message": "You have no result published yet for his particular exam"})
        else:
            return Response(serializers.data)


    @action(detail=False, methods=['get'], url_path='particularuser/(?P<user_id>[^/.]+)/(?P<exam_id>[^/.]+)', url_name='particularuser', permission_classes=[IsAdminUser])
    def particularuser(self, request, user_id=None, exam_id=None):
        result=Result.objects.filter(user=user_id, exam=exam_id)
        serializers=ResultSerializer(result, many=True)
        return Response(serializers.data)
