import React from 'react'
import InputGroup from 'react-bootstrap/InputGroup'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import Button from 'react-bootstrap/Button'
import FormControl from 'react-bootstrap/FormControl'

const Searchbar = (props) => {

  return (
    <InputGroup className="outline-secondary w-50-p">
      {props.options &&
        <DropdownButton
          as={InputGroup.Prepend}
          variant="outline-secondary"
          title={props.selected}
          id="searchby-dropdown"
        >
          {props.options.map(option => (
            <Dropdown.Item key={`searchby-${option.selector}`} onClick={() => props.onFilterChange(option)}>{option.name}</Dropdown.Item>
          ))}
        </DropdownButton>
      }
      <FormControl onChange={(event) => props.onChange(event.target.value)} />
      <InputGroup.Append>
        <Button variant="secondary">
          <i className="fas fa-search"></i>
        </Button>
      </InputGroup.Append>
    </InputGroup>
  )
}

export default Searchbar