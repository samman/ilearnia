import axios from "axios";

function login(formData) {
    // return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/v1/login/`, formData)
    return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/login/`, formData)
}

function signup(formData){
    return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/users/`, formData)
}

function getUserProfile() {
    const token = localStorage.getItem("jwt_token");
    return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/users/profile/`, {
        headers: {
            authorization: `Token ${token}`,
            'Content-Type': 'multipart/form-data'
        }
    })
}

function otp(data) {
    return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/v1/verify-otp/`, {
        otpcode: data
    })
}

function changePassword(userId, data){
    const token = localStorage.getItem("jwt_token");
    return axios.put(`${process.env.REACT_APP_API_BASE_URL}/api/change-password/${userId}/ `, data, {
        headers: {
            authorization: `Token ${token}`
        }
    })
}
function resendVerification(data) {
    return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/re-verifyemail/`, { email: data})
}

function verifyEmail(verificationToken){
    return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/verify-email/`, {
        params: {
            token: verificationToken
        }
    })
}

export { login, signup, getUserProfile, otp, resendVerification, changePassword, verifyEmail };
