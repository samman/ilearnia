import React, { useState } from "react";

// import OtpInput from 'react-otp-input';
import OTPInput from "./OTPInput";

const OTPForm = ({ onOTPSubmit, onGoBack, isOptLoading }) => {
  const [otpcode, setOtpcode] = useState("");

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <OTPInput
        autoFocus
        isNumberInput
        length={6}
        className="otpContainer"
        inputClassName="otpInput"
        onChangeOTP={(val) => setOtpcode(val)}
      />
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <button
          onClick={() => onOTPSubmit(otpcode)}
          className="btn btn-primary"
          disabled={isOptLoading}
        >
          Submit
          {isOptLoading && (
            <div
              className="spinner-border spinner-border-sm ml-2"
              role="status"
            >
            </div>
          )}
        </button>
        <button onClick={onGoBack} className="btn btn-secondary">
          Go back
        </button>
      </div>
    </div>
  );
};

export default OTPForm;
