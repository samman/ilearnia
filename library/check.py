import requests, PyPDF2, io
import pdb;pdb.set_trace()
url = 'http://127.0.0.1:8000/media/library/file/2021/02/24/AJP_TU_Solution_2070-76.pdf'
response = requests.get(url)
with io.BytesIO(response.content) as open_pdf_file:
    read_pdf = PyPDF2.PdfFileReader(open_pdf_file)
    num_pages = read_pdf.getNumPages()
    print(num_pages)