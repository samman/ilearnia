import React, { useContext, useEffect, useState } from "react";

import Dialog from "../../components/Dialog";
import MeetingForm from "./MeetingForm";
import MeetingTable from "./MeetingTable";
import Searchbar from "../../components/Searchbar";
import {
  createMeeting,
  getAllMeetings,
  removeMeeting,
} from "../../api/meeting";
import { CatchAsync } from "../../util";
import { AuthContext, TeamsContext } from "../../contexts";

const initMeeting = {
  topic: "",
  type: 2,
  start_time: "",
  duration: "",
  timezone: "",
};

const searchOptions = [
  {
    name: "Topic",
    selector: "topic",
  },
];

const MeetingList = () => {
  const { loggedInUser } = useContext(AuthContext);
  const { teams } = useContext(TeamsContext);
  const [rows, setRows] = useState([]);
  const [isDialogOpen, setDialogOpen] = useState(false);
  const [isMeetingListManipulated, setMeetingListManipulated] = useState(true);
  const [selected, setSelected] = useState(initMeeting);
  const [filterBy, setFilterBy] = useState(searchOptions[0]);
  const [searchTerm, setSearchTerm] = useState("");
  const [perPage, setPerPage] = useState(10);
  const [isCreateLoading, setIsCreateLoading] = useState(false);
  const [isMeetingDataLoading, setIsMeetingDataLoading] = useState(false);
  const [totalRows, setTotalRows] = useState(0);

  useEffect(
    CatchAsync(
      async () => {
        if (isMeetingListManipulated) {
          const params = {
            search: searchTerm,
            filter_by: filterBy.selector,
            limit: perPage,
            offset: 0 * perPage,
          };

          setIsMeetingDataLoading(true);
          const res = await getAllMeetings(
            localStorage.getItem("jwt_token"),
            params
          );
          setRows(res.data.results);
          setIsMeetingDataLoading(false);
          setTotalRows(res.data.count);
        }
        setMeetingListManipulated(false);
      },
      () => setIsMeetingDataLoading(false)
    ),
    [isMeetingListManipulated]
  );

  const openCreateDialog = () => {
    openDialog(null);
  };

  const openEditDialog = (row) => {
    openDialog(row);
  };

  const openDialog = (row) => {
    setDialogOpen(true);
    setSelected(row);
  };

  const closeDialog = () => {
    setDialogOpen(false);
    setSelected(initMeeting);
  };

  const create = CatchAsync(
    async (row) => {
      if (row.meeting_type === "Scheduled") {
        row.start_time = `${row.date}T${row.time}`;
      }
      if (!row.agenda) delete row.agenda;
      setIsCreateLoading(true);
      console.log(row)
      await createMeeting(row);
      setMeetingListManipulated(true);
      setIsCreateLoading(false);
      closeDialog();
    },
    () => setIsCreateLoading(false)
  );

  const remove = (row) => {
    removeMeeting(localStorage.getItem("jwt_token"), row.id).then(() => {
      setMeetingListManipulated(true);
    });
  };

  const handleFilter = (term) => {
    setSearchTerm(term);
    setMeetingListManipulated(true);
  };

  const handleFilterBy = (option) => {
    setFilterBy(option);
    setMeetingListManipulated(true);
  };

  const paginateMeetings = (page) => {
    setIsCreateLoading(true);

    const params = {
      search: searchTerm,
      filter_by: filterBy.selector,
      limit: perPage,
      offset: (page - 1) * perPage,
    };

    getAllMeetings(localStorage.getItem("jwt_token"), params).then((res) => {
      setRows(res.data.results);
      setTotalRows(res.data.count);
    });

    setIsCreateLoading(false);
  };

  const handlePageChange = (page) => {
    paginateMeetings(page);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setIsCreateLoading(true);

    const params = {
      search: searchTerm,
      filter_by: filterBy.selector,
      limit: newPerPage,
      offset: (page - 1) * newPerPage,
    };

    getAllMeetings(localStorage.getItem("jwt_token"), params).then((res) => {
      setRows(res.data.results);
      setTotalRows(res.data.count);
    });

    setPerPage(newPerPage);

    setIsCreateLoading(false);
  };

  return (
    <div className="meeting-list">
      <Dialog
        open={isDialogOpen}
        handleClose={closeDialog}
        additionalClass="form-modal"
      >
        <MeetingForm
          user={selected}
          onClose={closeDialog}
          onSubmit={create}
          isCreateLoading={isCreateLoading}
          teams={teams}
        />
      </Dialog>
      <div className="card shadow mb-4">
        <div className="card-header py-3">
          <div className="d-flex justify-content-between align-items-center">
            <Searchbar
              options={searchOptions}
              selected={filterBy.name}
              onChange={handleFilter}
              onFilterChange={handleFilterBy}
            />
            {loggedInUser?.is_admin && (
              <button
                className="btn btn-primary btn-circle btn tr-sticky-add-btn"
                onClick={openCreateDialog}
              >
                <i className="fas fa-plus"></i>
              </button>
            )}
          </div>
        </div>
        <div className="card-body">
          <MeetingTable
            rows={rows}
            openEditDialog={openEditDialog}
            onClose={closeDialog}
            onDelete={remove}
            handlePerRowsChange={handlePerRowsChange}
            handlePageChange={handlePageChange}
            perPage={perPage}
            totalRows={totalRows}
            loggedInUser={loggedInUser}
            loading={isMeetingDataLoading}
          />
        </div>
      </div>
    </div>
  );
};

export default MeetingList;
