import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import { useForm } from "react-hook-form";
import Dropper from "../../components/Dropper";
import { signup } from "../../api/auth";
import logo from "../../assets/img/logo/logo.svg";
import "./Signup.css";

const Signup = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [profilepic, setProfilepic] = useState(null);
  const [profilepicIsUpdated, setProfilepicIsUpdated] = useState(false);
  const [profilepicIsSet, setProfilepicIsSet] = useState(false);
  const [message, setMessage] = useState(null);
  const history = useHistory();

  const { register, handleSubmit, watch, errors, getValues } = useForm();

  useEffect(() => {
    if (profilepicIsUpdated) {
      const profilepic = getValues("profilepic")[0];
      setProfilepic(profilepic);
      profilepic && setProfilepicIsSet(true);
      setProfilepicIsUpdated(false);
    }
  }, [profilepicIsUpdated]);

  const password = watch("password");

  const performSignup = async (row) => {
    try {
      row.image = row.profilepic[0];

      const formData = new FormData();
      for (let key in row) {
        if (row[key]) formData.append(key, row[key]);
      }
      setIsLoading(true);
      await signup(formData);
      setIsLoading(false);
      setMessage({
        status: "success",
        body: "Please check your email for verification.",
      });
    } catch (err) {
      setIsLoading(false);
      if(err.response?.data){
        setMessage({
          status: "error",
          body: Object.values(err.response.data).reduce(
            (accumulator, current) => (accumulator += current[0])
          )
        });
      }
    }
  };

  return (
    <div className="Signup container">
      <div className="row justify-content-center">
        <div className="col-xl-10 col-lg-12 col-md-9">
          <div className="card o-hidden border-0 shadow-lg">
            <div className="card-body p-0">
              <div>
                {message?.status === "success" ? (
                  <div
                    className="mx-auto d-flex flex-column align-items-center justify-content-around p-2 mb-3"
                    style={{ height: "50vh", width: "50vw" }}
                  >
                    <h1>{message.body}</h1>
                    <button className="btn btn-primary" onClick={()=>history.push("/login")}>Back To Login</button>
                  </div>
                ) : (
                  <>
                    <div className="row m-l-0 m-r-0">
                      <div className="col-lg-6 d-flex align-items-center justify-content-center bg-login-image bg-primary">
                        <div
                          className="row w-100-p"
                          style={{ backgroundColor: "white" }}
                        >
                          <img
                            src={logo}
                            alt="logo"
                            className="w-100-p h-100-p"
                          ></img>
                        </div>
                      </div>
                      <div
                        className="col-lg-6 p-3"
                        style={{ minHeight: "40vh" }}
                      >
                        {message?.status === "error" && (
                          <div
                            className="bg-primary w-75-p mx-auto text-center text-light p-2 mb-3"
                            style={{ borderRadius: "4px" }}
                          >
                            {message.body}
                          </div>
                        )}
                        <Dropper
                          name="profilepic"
                          form={register}
                          accept="profilepic/*"
                          onChange={() => setProfilepicIsUpdated(true)}
                          additionalClass={`h-200 w-80-p ${
                            profilepicIsSet ? "dropper-filled" : ""
                          }`}
                        >
                          {profilepic ? (
                            <img
                              src={URL.createObjectURL(profilepic)}
                              alt=""
                              className="w-100-p dropper-img"
                            />
                          ) : (
                            <span>Select Profile Picture</span>
                          )}
                        </Dropper>
                        <div className="card-block">
                          <form
                            className="row"
                            numberValidate
                            onSubmit={handleSubmit(performSignup)}
                          >
                            <input
                              name="id"
                              readOnly
                              hidden
                              ref={register({})}
                            />

                            <div className="row mx-0 w-100-p">
                              <div className="form-group col-sm-12">
                                <label
                                  htmlFor="input-employee-id"
                                  className="m-b-5 f-w-600"
                                >
                                  Employee ID
                                </label>
                                <input
                                  type="text"
                                  className="form-control text-muted f-w-400"
                                  id="input-employee-id"
                                  name="employee_id"
                                  placeholder="Enter employee id"
                                  ref={register()}
                                />
                              </div>

                              <div className="row mx-0 w-100-p">
                                <div className="form-group col-sm-12">
                                  <label
                                    htmlFor="input-fullname"
                                    className="m-b-5 f-w-600"
                                  >
                                    Full Name
                                  </label>
                                  <input
                                    type="text"
                                    className="form-control text-muted f-w-400"
                                    id="input-fullname"
                                    name="fullname"
                                    placeholder="Enter fullname"
                                    ref={register()}
                                  />
                                </div>
                                <div className="form-group col-sm-12">
                                  <label
                                    htmlFor="input-email"
                                    className="m-b-5 f-w-600"
                                  >
                                    Email
                                  </label>
                                  <div className="input-group">
                                    <input
                                      type="email"
                                      className="form-control text-muted f-w-400"
                                      id="input-email"
                                      name="email"
                                      placeholder="Enter email address"
                                      ref={register({
                                        required: true,
                                        pattern: /^[a-z0-9](\.?[a-z0-9]){1,}@prabhubank.com$/,
                                      })}
                                      required
                                    />
                                  </div>
                                  {errors.email?.type === "required" && (
                                    <span className="text-danger">
                                      Email is required
                                    </span>
                                  )}
                                  {errors.email?.type === "pattern" && (
                                    <span className="text-danger">
                                      Please enter a valid prabhu bank email
                                    </span>
                                  )}
                                </div>
                                <>
                                  {/* For creating password */}
                                  <div className="form-group col-sm-6">
                                    <label
                                      htmlFor="input-password"
                                      className="m-b-5 f-w-600"
                                    >
                                      Password
                                    </label>
                                    <div className="input-group">
                                      <input
                                        type="password"
                                        className="form-control text-muted f-w-400"
                                        id="input-password"
                                        name="password"
                                        placeholder="Enter password"
                                        ref={register({
                                          required: true,
                                          minLength: 8,
                                        })}
                                      />
                                    </div>
                                    {errors.password?.type === "required" && (
                                      <span className="text-danger">
                                        Password is required
                                      </span>
                                    )}
                                    {errors.password?.type === "minLength" && (
                                      <span className="text-danger">
                                        Password must have at least 8 characters
                                      </span>
                                    )}
                                  </div>
                                  <div className="form-group col-sm-6">
                                    <label
                                      htmlFor="input-confirm-password"
                                      className="m-b-5 f-w-600"
                                    >
                                      Confirm Password
                                    </label>
                                    <div className="input-group">
                                      <input
                                        type="password"
                                        className="form-control text-muted f-w-400"
                                        id="input-confirm-password"
                                        name="confirm_password"
                                        placeholder="Re-enter password"
                                        ref={register({
                                          required: true,
                                          validate: (value) =>
                                            value === password,
                                        })}
                                      />
                                    </div>
                                    {errors.confirm_password?.type ===
                                      "required" && (
                                      <span className="text-danger">
                                        Confirm password is required
                                      </span>
                                    )}
                                    {errors.confirm_password?.type ===
                                      "validate" && (
                                      <span className="text-danger">
                                        Password and Confirm Password doesn't
                                        match
                                      </span>
                                    )}
                                  </div>
                                </>
                              </div>

                              <div className="form-group col-sm-12">
                                <label
                                  htmlFor="input-contact-number"
                                  className="m-b-5 f-w-600"
                                >
                                  Contact Number
                                </label>
                                <input
                                  type="number"
                                  className="form-control text-muted f-w-400"
                                  id="input-contact-number"
                                  name="contact_number"
                                  ref={register({
                                    minLength: 7,
                                    maxLength: 10,
                                  })}
                                  placeholder="Enter contact number"
                                />
                                {["minLength", "maxLength"].includes(
                                  errors.contact_number?.type
                                ) && (
                                  <span className="text-danger">
                                    Contact number must have 7 to 10 characters
                                  </span>
                                )}
                              </div>
                              <div className="form-group col-sm-12 d-flex justify-content-between">
                                <button
                                  type="submit"
                                  className="btn btn-primary"
                                  disabled={isLoading}
                                >
                                  Sign Up
                                  {isLoading && (
                                    <div
                                      className="spinner-border spinner-border-sm ml-2"
                                      role="status"
                                    >
                                      <span className="sr-only">
                                        Loading...
                                      </span>
                                    </div>
                                  )}
                                </button>
                                <button
                                  type="button"
                                  className="btn btn-secondary"
                                  onClick={() => history.push("/login")}
                                >
                                  Login
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Signup;
