from django.contrib import admin
from documents.models import Team, Zoom

admin.site.register(Team)
admin.site.register(Zoom)