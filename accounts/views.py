from django.contrib.auth import get_user_model, logout,login,authenticate
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAdminUser
from .overide import IsOwnUser
from django.urls import reverse
from .utils import Util
import jwt
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.hashers import make_password

from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from django.core.exceptions import ObjectDoesNotExist
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.exceptions import APIException
from django.shortcuts import get_object_or_404

from django.views.decorators.csrf import csrf_exempt
from .overide import IsAdmin, CanView, IsAdminUser

from .serializers import UserSerializer, EmailVerificationSerializer, UserLoginSerializer, UserModifiySerializer, UserAdminSerializer, UserHodSerializer, DepartmentSerializer, UserLogSerializer
from .models import UserModifyRecord, Department, EmailOTP
from .signals import UserLog
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.throttling import AnonRateThrottle

from django.core.mail import send_mail, EmailMultiAlternatives


User = get_user_model()



class UserRegisterView(viewsets.ModelViewSet):
	"""
	User view for CRUD operation
	"""

	queryset = User.objects.all()
	serializer_class = UserSerializer
	filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
	filterset_fields = ['email', 'fullname','department__dept_name']
	search_fields = ['email', 'fullname','department__dept_name']
	ordering_fields = ['email', 'fullname','department__dept_name']
	lookup_field = 'id'
	

	def get_serializer_class(self):
		"""
		METHOD TO PROVIDE DATA ACCORIDING TO USER TYPE
		"""
		user=self.request.user

		try:
			if user.is_admin:
				return UserAdminSerializer
			elif user.is_staff:
				return UserHodSerializer
			else:
				return UserSerializer
		except:
			return UserSerializer


	def get_queryset(self):
		"""
		PROVIDING LIST OF USERS IN RELATED DEPARTMENT FOR HOD
		"""
		try:
			user=self.request.user

			if user.is_admin==False and user.is_staff==True:

				user_list=User.objects.filter(department=self.request.user.department)
				return user_list

			user_list=User.objects.all()
			return user_list
		except ObjectDoesNotExist:
			return Response( {"status":status.HTTP_204_NO_CONTENT})

	def get_permissions(self):

		"""
		Providing Permissions According To UserType
		"""

		if self.action in ['create']:
			permissions_classes = [AllowAny]
			
		elif self.action in ['destroy']:
			permissions_classes = [IsAdminUser]

		elif self.action in ['list']:
			permissions_classes = [IsAdminUser]

		elif self.action == 'retrieve':
			permissions_classes = [CanView]

		elif self.action in ['update', 'partial_update']:
			permissions_classes = [CanView]

		else:
			return super().get_permissions()

		return [permissions() for permissions in permissions_classes]


	
	def update(self, request, *args, **kwargs):
		partial = kwargs.pop('partial', True)
		instance = self.get_object()
		serializer = self.get_serializer(instance, data=request.data, partial=partial)

		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		response_data = {
			"message": "Successfully Updated!! ",
			"user": serializer.data
		}
		return Response(response_data, status=status.HTTP_200_OK)


	def list (self, request, *args, **kwargs):

		queryset = self.filter_queryset(self.get_queryset())

		page = self.paginate_queryset(queryset)
		if page is not None:
			serializer = self.get_serializer(page, many=True)
			for result in serializer.data:
				result.pop('password')

			return self.get_paginated_response(serializer.data)

		serializer = self.get_serializer(queryset, many=True)

		for result in serializer.data:

			result.pop('password')
		return Response(serializer.data)

	def perform_create(self, serializer):

		serializer.validated_data['password'] = make_password((serializer.validated_data['password']))
		user = serializer.save(created_by=self.request.user.id, is_active=True)
		user_data = serializer.data
		headers = self.get_success_headers(serializer.data)
		user = User.objects.get(email=user_data['email'])
		token = RefreshToken.for_user(user).access_token
		subject, from_email, to = 'Verfiy your email', 'no-repl@prabhubank.com', user.email
		email_body='Please verfiy your email - ' + user.email
		current_site = get_current_site(self.request).domain
		relativelink = reverse('verify-email')
		absurl = 'http://'+current_site +"/verify" + "?token=" + str(token) + "&email=" + user.email
		html_content=f'<p>{email_body}</p>\n<a href="{absurl}">Click here</a>'
		msg=EmailMultiAlternatives(subject, email_body, from_email, [to])
		msg.attach_alternative(html_content, "text/html")
		msg.send()


		response_data = {
			"success": "true",
			"message": "Successfully Registered!! Please check your email for verification.",
			"user": user_data
		}
		return Response(response_data, status=status.HTTP_201_CREATED, headers=headers)

	def perform_update(self, serializer):

		user=serializer.save()
		user_obj= User.objects.get(id=user.id)
		modify=UserModifyRecord(modified_by=self.request.user.id, modified_user=user_obj)
		modify.save()




	def perform_destroy(self, instance):

		"""
		USERS ARE NOT ALLOWED TO DELETE THEMSELVES
		ADMIN ARE NOT ALLOWED TO DELETE OTHER ADMIN
		HOD ARE NOT ALLOWED TO DELETE OTHER HOD
		"""

		user=self.request.user
		if user==instance:
			return Response({'message':'You are not allowed to deleted yourself'})
		elif user.is_staff and not user.is_admin and instance.is_staff:
			return Response({'message':'Department head is not allowed to deleted department head'})
		elif user.is_staff and user.is_admin and instance.is_admin and not user.is_superuser:
			return Response({'message':'Admin is not allowed to deleted Admin'}) 
		else:
			instance.delete()



	def retrieve(self, request, *args, **kwargs):

		"""
		TO GET DATA OF SPECIFIC USER
		"""

		try:
			user=self.get_object()
			serializer=self.get_serializer(user)
			return Response(serializer.data,status=status.HTTP_200_OK)
		except:
			return Response ({'status': status.HTTP_401_UNAUTHORIZED,'message':'You donot have permission to perform this action'})

	@action(methods=['get'], detail=False)
	def profile(self, request, *args, **kwargs):

		"""
		METHOD TO GET PROFILE OF USER
		"""

		try:
			user = request.user
			if user.is_admin:
				serializer = UserAdminSerializer(user)
			elif user.is_staff:
				serializer = UserHodSerializer(user)
			else:
				serializer = UserSerializer(user)

			return Response(serializer.data,status=status.HTTP_200_OK)

		except:
			return Response({'status': 'No detail found for the request user'})


	@action(methods=['get'], detail=True)
	def soft(self, request, id):
		"""
		SOFT DELETING USER
		"""
		user=User.objects.get(id=id)
		if user.is_active:
			user.is_active=False
			user.is_verified=False
			user.save()
			return Response({"status": "User has been successfully deleted"})
		else:
			return Response({"status": "User has been already deleted"})


	@action(methods=['get'], detail=False, permission_classes=[IsAdminUser])
	def active(self, request):
		"""
		LIST OF USERS WHICH ARE ACTIVE
		"""
		users=User.objects.all()
		data=users.filter(is_active=True)
		serializer=UserSerializer(data, many=True)
		return Response(serializer.data, status=200)

class DepartmentView(viewsets.ModelViewSet):
	"""
	Department View,Permisssions
	"""
	queryset = Department.objects.all()
	serializer_class = DepartmentSerializer
	permission_classes = (IsAdminUser, )
	filter_backends = [DjangoFilterBackend, SearchFilter]
	filterset_fields = ['dept_name']
	search_fields = ['dept_name']

	@action(methods=['GET'], detail=True, permission_classes=[IsAdminUser])
	def depthead(self, request, *args, **kwargs):
		users=User.objects.filter(department=kwargs['pk'])
		hod=users.filter(is_staff=True)
		serializer=UserHodSerializer(hod, many=True)
		return Response(serializer.data)

	@action(methods=['get'], detail=False, permission_classes=[IsAdminUser])
	def member(self, request, *args, **kwargs):
		member = User.objects.filter(department=request.user.department)
		serializer = UserHodSerializer(member, many=True)
		return Response(serializer.data)

	def destroy(self, request, *args, **kwargs):
		instance = self.get_object()
		try:
			self.perform_destroy(instance)
			return Response({'detail': 'successfully deleted'}, status=status.HTTP_204_NO_CONTENT)
		except:
			return Response({'detail': 'Please delete all related instances'}, status=status.HTTP_400_BAD_REQUEST)


class VerifyEmail(APIView):
	"""
	View For EmailVerification for user
	"""
	serializer_class = EmailVerificationSerializer

	def get(self, request):
		token = request.GET.get('token')
		try:

			payload = jwt.decode(token, settings.SECRET_KEY)
			user = User.objects.get(id=payload['user_id'])
			if not user.is_verified:
				user.is_verified = True
				user.save()
				email=user.email

				email_superuser=list(User.objects.filter(is_superuser=True).values_list('email', flat=True))

				send_mail(
					'New user Registration',
					'User '+email+' has registered on '+get_current_site(self.request).domain,
					'no-reply@prabhubank.com',
					email_superuser,
					fail_silently=False,
				)
			return Response(
				{'success': 'Email Successfully activated', 'status': status.HTTP_200_OK}, 
				status=status.HTTP_200_OK)
		except jwt.ExpiredSignatureError as identifier:
			return Response(
				{'detail': 'Activation Expired', 'status': status.HTTP_400_BAD_REQUEST}, 
				status=status.HTTP_400_BAD_REQUEST)
		except jwt.exceptions.DecodeError as identifier:
			return Response(
				{'detail': 'Invalid token', 'status': status.HTTP_400_BAD_REQUEST}, 
				status=status.HTTP_400_BAD_REQUEST)

class UserLoginOTPView(APIView):
	"""
	View For LOGIN OTP System for user
	"""
	throttle_classes = [AnonRateThrottle]

	def post(self, request, *arg, **kwargs):
		serializer = UserLoginSerializer(data=request.data)

		serializer.is_valid(raise_exception=True)
		user = serializer.validated_data['user']
		login(request, user)
		#request.session['session_id'] = request.session.session_key

		otp_code = EmailOTP.objects.filter(email=user.email).values_list('otp_code')
		subject = 'OTP Verification'
		message = 'Please use below digits to verify your one time password ' + ''.join(otp_code[0])

		data = {
			'email_body': message,
			'to_email': user.email,
			'email_subject': subject
		}
		Util.send_email(data)

		return Response({'message': 'Please verify your OTP from email'})


class UserLoginView(APIView):
	"""
	View for Login sytem without implemeting OTP System
	"""
	throttle_classes = [AnonRateThrottle]

	def post(self, request, *args, **kwargs):
		serializer = UserLoginSerializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		user=serializer.validated_data['user']
		login(request, user)
		token,created=Token.objects.get_or_create(user=user)

		response=Response({
			'token': token.key,
			'email': user.email,
			'is_active':user.is_active,
			'is_staff':user.is_staff,
			'is_admin':user.is_active,
			'is_internalInstructor':user.is_internalInstructor,
			'is_externalInstructor':user.is_externalInstructor
		}, status=status.HTTP_200_OK)
		response.set_cookie('auth_token', token, httponly=True, samesite='Lax')
		return response


class UserLogoutView(APIView):
	"""
	View for Logout System for user
	"""
	authentication_classes = (TokenAuthentication,)

	def post(self, request):
	    logout(request)
	    return Response({'detail': "Successfully logout"}, status=204)



class UserLogView(viewsets.ModelViewSet):
	"""
	View for Login, Logout Log of Users
	"""
	queryset = UserLog.objects.all()
	serializer_class = UserLogSerializer
	filter_backends = [DjangoFilterBackend, SearchFilter]
	filterset_fields = ['email']
	search_fields = ['email']

	def get_permissions(self):
		if self.action in ['list']:
			self.permission_classes = (IsAdminUser, )

		return super(self.__class__, self).get_permissions()


class ResendEmailVerification(APIView):
	"""
	View for Resending Email Verification for user
	"""
	permission_classes = [AllowAny]

	def post(self, request):
		user = get_object_or_404(User, email=request.data['email'])

		if user.is_verified == True:
			return Response({'detail': 'This email is already verified'}, status=status.HTTP_400_BAD_REQUEST)
		else:
			try:
				token = RefreshToken.for_user(user).access_token
				subject, from_email, to = 'Verfiy your email', 'no-repl@prabhubank.com', user.email
				email_body='Please verfiy your email - ' + user.email
				current_site = get_current_site(self.request).domain
				relativelink = reverse('verify-email')
				absurl = current_site + "/verify" + "?token=" + str(token) + "&email=" + user.email
				html_content=f'<p>{email_body}</p>\n<a href="{absurl}">Click here</a>'
				msg=EmailMultiAlternatives(subject, email_body, from_email, [to])
				msg.attach_alternative(html_content, "text/html")
				msg.send()
				return Response({'message': 'Email confirmation sent'}, status=status.HTTP_201_CREATED)
			except APIException:
				return Response({'detail': 'This email does not exist, please create a new account'}, status=status.HTTP_403_FORBIDDEN)


class UserModifyRecordView(viewsets.ModelViewSet):

	"""
	View for user modification log
	"""

	queryset = UserModifyRecord.objects.all()
	serializer_class = UserModifiySerializer


	def get_permissions(self):

		if self.action =='list':
			permissions_classes = [IsAdminUser]

		elif self.action=='retrieve':
			permissions_classes = [CanView]
		else:
			return super().get_permissions()

		return [permissions() for permissions in permissions_classes]

