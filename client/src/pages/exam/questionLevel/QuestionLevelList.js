import React from "react";
import LevelTable from "./QuestionLevelTable";
import Searchbar from "../../../components/Searchbar";

const QuestionLevelList = (props) => {
  return (
    <div className="user-list p-4">
      <div className="card shadow mb-4">
        <div className="card-header py-3">
          <Searchbar
            options={props.searchOptions}
            selected={props.filterBy.name}
            onChange={props.handleFilter}
            onFilterChange={props.handleFilterBy}
          />
        </div>
        <div className="card-body">
          <LevelTable
            isDeptDetailLoading={props.isDeptDetailLoading}
            handlePerRowsChange={props.handlePerRowsChange}
            handlePageChange={props.handlePageChange}
            rows={props.rows}
            perPage={props.perPage}
            onDelete={props.onDelete}
            totalRows={props.totalRows}
            loading={props.isDeptDetailLoading}
            loggedInUser={props.loggedInUser}
          />
        </div>
      </div>
    </div>
  );
};

export default QuestionLevelList;
