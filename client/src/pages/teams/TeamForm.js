import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { DepartmentsContext } from "../../contexts/Departments";
import { TeamsContext } from "../../contexts/Teams";
import UserList from "./UserList";
import { createTeam } from "../../api/team";
import { CatchAsync } from "../../util";

const TeamForm = (props) => {
  const { team } = props.team;
  const [selectedUsers, setSelectedUser] = useState(team ? team.members : []);
  const history = useHistory();
  const [isSaveLoading, setIsSaveLoding] = useState(false);
  const { departments } = useContext(DepartmentsContext);
  const { setIsTeamsManinuplate } = useContext(TeamsContext);

  const { register, handleSubmit, watch, errors } = useForm({
    defaultValues: team || undefined,
  });

  const selectedDepts = watch("department");

  const onSubmit = CatchAsync(
    async (data) => {
      delete data.id;
      setIsSaveLoding(true);
      const department=typeof data.department === "string" ? [data.department] : data.department;
      await createTeam({
        ...data,
        members: selectedUsers.map((user) => user.id),
        department,
      });
      setIsTeamsManinuplate(true);
      setIsSaveLoding(false);
      history.push("/teams");
    },
    () => {
      setIsSaveLoding(false);
    }
  );

  const toogleUserSelection = (user) => {
    if (selectedUsers.includes(user)) {
      setSelectedUser((prev) => prev.filter((item) => item !== user));
    } else {
      setSelectedUser((prev) => [...prev, user]);
    }
  };

  // useEffect(() => setValue("members", selectedUsers), [selectedUsers]);

  return (
    <div>
      <div className="row m-l-0 m-r-0">
        <div className="col-sm-6" style={{ minHeight: "20vh" }}>
          <div className="card-block shadow">
            <form className="row" noValidate onSubmit={handleSubmit(onSubmit)}>
              <input name="id" readOnly hidden ref={register({})} />

              <input ref={register} name="members" hidden />

              <div className="row mx-0 w-100-p">
                <div className="form-group col-sm-12">
                  <label htmlFor="input-name" className="m-b-5 f-w-600">
                    Team Name
                  </label>
                  <input
                    type="text"
                    className="form-control text-muted f-w-400"
                    id="input-name"
                    name="name"
                    placeholder="Enter team name"
                    ref={register({
                      required: true,
                    })}
                  />
                  {errors.name?.type === "required" && (
                    <span className="text-danger">Team name is required</span>
                  )}
                </div>
                <div className="form-group col-sm-12">
                  <label htmlFor="input-agenda" className="m-b-5 f-w-600">
                    Agenda
                  </label>
                  <textarea
                    type="text"
                    className="form-control text-muted f-w-400"
                    id="input-agenda"
                    name="agenda"
                    placeholder="Enter agenda"
                    ref={register({ required: true })}
                  />
                  {errors.agenda?.type === "required" && (
                    <span className="text-danger">Agenda is required</span>
                  )}
                </div>
                <div className="form-group col-sm-12">
                  <label htmlFor="input-description" className="m-b-5 f-w-600">
                    Description
                  </label>
                  <textarea
                    type="text"
                    className="form-control text-muted f-w-400"
                    id="input-description"
                    name="description"
                    placeholder="Enter description"
                    ref={register({ required: true })}
                  />
                  {errors.description?.type === "required" && (
                    <span className="text-danger">
                      Description is required
                    </span>
                  )}
                </div>
                <div className="form-group col-sm-12">
                  <label htmlFor="input-description" className="m-b-5 f-w-600">
                    Departments
                  </label>
                  {departments.map((dept) => (
                    <div className="form-check" key={dept.id}>
                      <input
                        type="checkbox"
                        className="form-check-input"
                        id={`${dept.id}-radio`}
                        name="department"
                        ref={register({ required: true })}
                        value={dept.id}
                      />
                      <label
                        className="form-check-label"
                        htmlFor={`${dept.id}-radio`}
                      >
                        {dept.dept_name}
                      </label>
                    </div>
                  ))}
                  {errors.department?.type === "required" && (
                    <span className="text-danger">
                      Department is required
                    </span>
                  )}
                </div>
                <div className="form-group col-sm-12">
                  <label htmlFor="input-description" className="m-b-5 f-w-600">
                    Users
                    <small id="emailHelp" className="form-text text-muted">
                      Select users form the table on right.
                    </small>
                  </label>
                  <ul className="list-group overflow-auto h-100">
                    {selectedUsers.map((user) => (
                      <li
                        key={user.id}
                        className="list-group-item bg-dark text-light"
                      >
                        {user.email}
                      </li>
                    ))}
                  </ul>
                </div>
                <div className="form-group col-sm-12 d-flex justify-content-between">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={isSaveLoading}
                  >
                    Save
                    {isSaveLoading && (
                      <div
                        className="spinner-border spinner-border-sm ml-2"
                        role="status"
                      >
                        <span className="sr-only">Loading...</span>
                      </div>
                    )}
                  </button>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={() => history.goBack()}
                  >
                    Go Back
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="col-sm-6" style={{ minHeight: "20vh" }}>
          <UserList
            onRowClicked={toogleUserSelection}
            selectedUsers={selectedUsers}
            selectedDepts={selectedDepts}
          />
        </div>
      </div>
    </div>
  );
};

export default TeamForm;