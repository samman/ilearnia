import React, { useContext, useEffect, useState } from "react";
import { APIErrorContext } from "../contexts/APIError";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const APIErrorAlert = () => {
  const { error, removeError } = useContext(APIErrorContext);
  const activeDuration = 5000;

  useEffect(() => {
    if (error) {
      let errorMsg =
        `${error.response?.status}`.startsWith("4") && !!error.response?.data
          ? Object.values(error.response.data).join(" ")
          : "Sorry, something went wrong";

      if(error.response?.status===404) errorMsg ="Sorry, something went wrong";

      toast.error(errorMsg);
      removeError();
    }
  }, [error]);

  return <ToastContainer autoClose={activeDuration} />;
};

export default APIErrorAlert;
