from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Exam)
admin.site.register(Result)
admin.site.register(QuestionAnswer)
admin.site.register(ExamUser)
admin.site.register(Level)
admin.site.register(ExamQuestion)