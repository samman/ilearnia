import React, { useState, useEffect } from "react";
import "./Question.scss";
import { getLevel } from "../../../api/level";
import { CatchAsync } from "../../../util";

const Question = ({ question, register, index, insert, fields }) => {
  const [isLevelLoading, setIsLevelLoading] = useState(false);
  const [level, setLevel] = useState();

  useEffect(
    async () => {
      try {
        setIsLevelLoading(true);
        const res = await getLevel(question.level);
        setIsLevelLoading(false);
        setLevel(res.data);
      } catch {
        setIsLevelLoading(false);
      }
    },
    // () => setIsLevelLoading(false)
    // CatchAsync(
    //   ),
    []
  );

  return (
    <div className="question-container mt-sm-5 my-1">
      <div className="question p-3 border-bottom">
        <div className="question-options">
          <div className="d-flex">
            <p className="py-2 h5 question col-8">
              <b>Q. {question.question}</b>
            </p>
            <div className="d-flex col-4">
              <div className="col-6 d-flex">
                <p>
                  level:
                  {isLevelLoading ? (
                    <span
                      className="spinner-border text-secondary spinner-border-sm ml-2"
                      role="status"
                    ></span>
                  ) : (
                    <span className="ml-2">{level?.name || "-"}</span>
                  )}
                </p>
              </div>
              <div className="col-6">
                <p>
                  reward:
                  {isLevelLoading ? (
                    <span
                      className="spinner-border text-secondary spinner-border-sm ml-2"
                      role="status"
                    ></span>
                  ) : (
                    <span className="ml-2">{level?.reward || "-"}</span>
                  )}
                </p>
              </div>
            </div>
          </div>
          <div className="ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3" id="options">
            {/* Hidden input for question id */}
            <input
              hidden
              name={`questions[${index}].question`}
              value={question.id}
              onChange={() => {}}
              ref={register()}
            ></input>{" "}
            <label className="options">
              {question.choice1}{" "}
              <input
                type="radio"
                name={`questions[${index}].question_answer`}
                ref={register()}
                value={question.choice1}
              />{" "}
              <span className="checkmark"></span>{" "}
            </label>{" "}
            <label className="options">
              {question.choice2}{" "}
              <input
                type="radio"
                name={`questions[${index}].question_answer`}
                ref={register()}
                value={question.choice2}
              />{" "}
              <span className="checkmark"></span>{" "}
            </label>{" "}
            <label className="options">
              {question.choice3}{" "}
              <input
                type="radio"
                name={`questions[${index}].question_answer`}
                ref={register()}
                value={question.choice3}
              />{" "}
              <span className="checkmark"></span>{" "}
            </label>{" "}
            <label className="options">
              {question.choice4}{" "}
              <input
                type="radio"
                name={`questions[${index}].question_answer`}
                ref={register()}
                value={question.choice4}
              />{" "}
              <span className="checkmark"></span>{" "}
            </label>{" "}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Question;
