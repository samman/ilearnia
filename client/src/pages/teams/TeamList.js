import React from "react";
import { useHistory } from "react-router-dom";
import TeamTable from "./TeamTable";
import Searchbar from "../../components/Searchbar";

const TeamsList = ({
  isTeamDataLoading,
  handlePerRowsChange,
  handlePageChange,
  rows,
  perPage,
  onDelete,
  totalRows,
  departments,
  searchOptions,
  filterBy,
  handleFilter,
  handleFilterBy,
  onAddClick,
}) => {
  // Hooks
  const history = useHistory();
  return (
    <div className="Team-list">
      <div className="card shadow mb-4">
        <div className="card-header py-3">
          <Searchbar
            options={searchOptions}
            selected={filterBy.name}
            onChange={handleFilter}
            onFilterChange={handleFilterBy}
          />
          <button
            className="btn btn-primary btn-circle btn tr-sticky-add-btn"
            onClick={onAddClick}
          >
            <i className="fas fa-plus"></i>
          </button>
        </div>
        <div className="card-body">
          <TeamTable
            isTeamDataLoading={isTeamDataLoading}
            handlePerRowsChange={handlePerRowsChange}
            handlePageChange={handlePageChange}
            rows={rows}
            perPage={perPage}
            onDelete={onDelete}
            totalRows={totalRows}
            loading={isTeamDataLoading}
            departments={departments}
          />
        </div>
      </div>
    </div>
  );
};

export default TeamsList;
