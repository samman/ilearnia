import React, { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useForm, useFieldArray } from "react-hook-form";
import Swal from "sweetalert2";
import Question from "../question/Question";
import "./FillExamForm.scss";
import { APIErrorContext, AuthContext } from "../../../contexts";

import {
  getExamQuestions,
  getQuestion,
  submitAnswer,
} from "../../../api/question";
import { publishResult, getResult } from "../../../api/result";
import { CatchAsync } from "../../../util";

const FillExamForm = ({ exam }) => {
  const history = useHistory();
  const [questions, setQuestions] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const { control, register, handleSubmit } = useForm();
  const { addError } = useContext(APIErrorContext);
  const { loggedInUser }  = useContext(AuthContext);

  const { fields, insert } = useFieldArray({
    control,
    name: "questions",
  });

  useEffect(
    async () => {
      try{
        if (!exam.id) {
          history.push("/exams");
        }
        setIsLoading(true);
        const examQuestions = await getExamQuestions(exam.id);
        const allQuestions = [];
        for (let { question: questionId } of examQuestions.data) {
          const res = await getQuestion(questionId);
          allQuestions.push(res.data);
        }
        setQuestions(allQuestions);
        setIsLoading(false);
      }catch(err){
        setIsLoading(false);
      }
    },
    []
  );

  const handleCancel = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will loose all progress!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.isConfirmed) {
        history.goBack();
      }
    });
  };

  const onSubmit = CatchAsync(async (data) => {
    const result = await Swal.fire({
      title: "Are you sure you want to submit your answers?",
      text: "This action cannot be undone!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    });
    if (result.isConfirmed) {
      for (let { question, question_answer } of data.questions) {
        await submitAnswer({ question, question_answer: [question_answer] });
      }
      Swal.fire("You will receive the results in your email when it gets published.")
      history.push("/exams");
    }
  });

  return (
    <div className="container mt-sm-5 my-1">
      <div className="container mt-5">
        <div className="d-flex justify-content-center row">
          <div className="col-md-10 col-lg-10">
            <div className="border shadow">
              <div className="questions p-3 border-bottom">
                <div className="d-flex flex-row justify-content-between align-items-center mcq">
                  <h4>{exam.name}</h4>
                </div>
                <form onSubmit={handleSubmit(onSubmit)}>
                  {questions?.length ? (
                    <>
                      {questions?.map((question, index) => (
                        <Question
                          question={question}
                          key={`question-${index}`}
                          fields={fields}
                          register={register}
                          index={index}
                          insert={insert}
                        />
                      ))}
                      <div className="d-flex flex-row justify-content-between align-items-center p-3 bg-white">
                        <button
                          className="btn btn-primary d-flex align-items-center btn-danger"
                          type="button"
                          onClick={handleCancel}
                        >
                          <i className="fa fa-angle-left mt-1 mr-1"></i>
                          &nbsp;Cancel
                        </button>
                        <button
                          className="btn btn-primary border-success align-items-center btn-success"
                          type="submit"
                        >
                          Submit<i className="fa fa-angle-right ml-2"></i>
                        </button>
                      </div>
                    </>
                  ) : (
                    <div className="text-center">
                      {isLoading ? (
                        <div className="d-flex justify-content-center align-items-center">
                          <h2 className="text-secondary m-2">Loading</h2>
                          <div
                            className="spinner-border text-secondary m-2"
                            role="status"
                          ></div>
                        </div>
                      ) : (
                        <h2 className="text-secondary">Sorry! No Questions</h2>
                      )}
                      <button
                        className="btn btn-primary d-flex align-items-center btn-danger mt-4"
                        type="button"
                        onClick={() => history.goBack()}
                      >
                        <i className="fa fa-angle-left mt-1 mr-1"></i>
                        &nbsp;Return
                      </button>
                    </div>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FillExamForm;
