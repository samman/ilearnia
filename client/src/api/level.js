import axios from "axios";

function getAllQuestionLevels(params) {
  const token = localStorage.getItem("jwt_token");
  return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/level/`, {
    headers: {
      authorization: `Token ${token}`,
    },
    params
  });
}

function getLevel(levelId){
  const token = localStorage.getItem("jwt_token");
  return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/level/${levelId}/`, {
    headers: {
      authorization: `Token ${token}`,
    },
  });
}

function createQuestionLevel(data) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/level/`, data, {
    headers: {
      authorization: `Token ${token}`,
    },
  });
}

function updateQuestionLevel(deptId, data) {
  const token = localStorage.getItem("jwt_token");
  return axios.put(
    `${process.env.REACT_APP_API_BASE_URL}/api/level/${deptId}/`,
    data,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function removeQuestionLevel(deptId) {
  const token = localStorage.getItem("jwt_token");
  return axios.delete(
    `${process.env.REACT_APP_API_BASE_URL}/api/level/${deptId}/`,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

export { getAllQuestionLevels, createQuestionLevel, updateQuestionLevel, removeQuestionLevel, getLevel };
