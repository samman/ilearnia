import React, { useEffect, useState } from "react";
import { getAllMeetings } from "../../api/meeting";
import { CatchAsync } from "../../util";

const Dashboard = () => {
  const [upcommingMeetings, setUpcommingMeetings] = useState([]);
  const [loading, setLoading] = useState(false);

  const retrieveLatestMeetings = CatchAsync(async () => {
    const params = {
      limit: 5,
    };
    setLoading(true);
    const res = await getAllMeetings(params);
    setLoading(false);
    const meetings = formatDate(res?.data?.results);
    setUpcommingMeetings(meetings);
  }, ()=>{
    setLoading(false);
  });

  useEffect(() => {
      retrieveLatestMeetings();
  }, []);

  const formatDate = (meetings) => {
    return meetings.map((meeting) => ({
      ...meeting,
      start_time: meeting.start_time?.split("T").join(" ").split("Z"),
    }));
  };

  return (
    <div className="dashboard mt-4">
      <div className="card border-left-primary shadow mb-4">
        <div className="card-header py-3">
          <h6 className="m-0 font-weight-bold text-primary">
            Upcoming Meetings
          </h6>
        </div>
        <div className="card-body">
          <div className="row no-gutters align-items-center">
            {loading && (
              <div className="spinner-border text-dark" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            )}
            <div className="col mr-2">
              {!loading && upcommingMeetings.length === 0 ? 'No upcoming meetings' : upcommingMeetings.map((meeting) => (
                <li className="list-group-item d-flex justify-content-between align-items-center" key={meeting.id}>
                  {meeting.topic}
                  <div className="d-flex align-items-center">
                    <span className="badge badge-primary badge-pill mx-3">
                      {meeting.start_time || "ongoing"}
                    </span>
                    <div>
                      <a
                        className="btn btn-secondary btn-sm btn-icon-split"
                        href={meeting.join_url}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <span className="icon text-white-50">
                          <i className="fas fa-sign-in-alt"></i>
                        </span>
                        <span className="text">Join Now</span>
                      </a>
                    </div>
                  </div>
                </li>
              ))}
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-xl-3 col-md-6 mb-4">
          <div className="card border-left-primary shadow h-100 py-2">
            <div className="card-body">
              <div className="row no-gutters align-items-center">
                <div className="col mr-2">
                  <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    Courses
                  </div>
                  <div className="h5 mb-0 font-weight-bold text-gray-800">
                    Upcoming
                  </div>
                </div>
                <div className="col-auto">
                  <i className="fas fa-pen fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-xl-3 col-md-6 mb-4">
          <div className="card border-left-primary shadow h-100 py-2">
            <div className="card-body">
              <div className="row no-gutters align-items-center">
                <div className="col mr-2">
                  <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    Library
                  </div>
                  <div className="h5 mb-0 font-weight-bold text-gray-800">
                    Upcoming
                  </div>
                </div>
                <div className="col-auto">
                  <i className="fas fa-book fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-xl-3 col-md-6 mb-4">
          <div className="card border-left-primary shadow h-100 py-2">
            <div className="card-body">
              <div className="row no-gutters align-items-center">
                <div className="col mr-2">
                  <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    Performance
                  </div>
                  <div className="h5 mb-0 font-weight-bold text-gray-800">
                    Upcoming
                  </div>
                </div>
                <div className="col-auto">
                  <i className="fas fa-running fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
