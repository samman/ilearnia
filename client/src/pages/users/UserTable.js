//React imports
import React from "react";
import DataTable from "react-data-table-component";

const UserTable = (props) => {
  const handleDelete = (row) => props.onDelete(row);

  const columns = [
    {
      name: "Full Name",
      selector: "fullname",
      sortable: true,
      format: row => row.fullname ? row.fullname : '-'
    },
    {
      name: "Role",
      selector: "role",
      sortable: true,
      format: row => row.role ? row.role : '-'
    },
    {
      name: "Email Address",
      selector: "email",
      sortable: true,
    },
    {
      name: "Phone Number",
      selector: "phone",
      sortable: true,
      format: row => row.phone ? row.phone : '-'
    },
    {
      name: "Department",
      selector: "department",
      sortable: true,
      format: row => row.department ? props.departments.find(dept=>dept.id===row.department)?.dept_name : '-'
    },
    {
      name: 'Action',
      button: true,
      cell: (row) =>
        !row.is_admin ?
        <div>
          <button className="btn btn-primary btn-sm ml-1" onClick={() => handleDelete(row)}>
            <i className="fas fa-trash"></i>
          </button>
        </div> : '',
      ignoreRowClick: true,
      allowOverflow: true,
    }
  ]

  return (
    <DataTable
      noHeader={true}
      pagination={true}
      paginationServer={true}
      columns={columns}
      data={props.rows}
      highlightOnHover={true}
      pointerOnHover={true}
      onRowClicked={props.onRowClicked}
      paginationPerPage={props.perPage}
      onChangeRowsPerPage={props.handlePerRowsChange}
      onChangePage={props.handlePageChange}
      progressPending={props.loading}
      paginationTotalRows={props.totalRows}
    />
  )
}

export default UserTable;
