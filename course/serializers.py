from rest_framework import serializers
from .models import *



class InstructorSerializer(serializers.ModelSerializer):
    class Meta:
        model=Instructor
        fields='__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model=Category
        fields='__all__'

        extra_kwargs = {
            'created_by': {'read_only': True},
        }


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model=SubCategory
        fields='__all__'

        extra_kwargs = {
            'created_by': {'read_only': True},
        }

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model=Course
        fields='__all__'

        extra_kwargs = {
            'created_by': {'read_only': True},
        }


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model=Lesson
        fields='__all__'

        extra_kwargs = {
            'created_by': {'read_only': True},
        }

class LessionVideoDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model=LessonVideoDocument
        fields='__all__'

        extra_kwargs = {
            'created_by': {'read_only': True},
        }

class SyllabusSerializer(serializers.ModelSerializer):
    class Meta:
        model=Syllabus
        fields='__all__'

