import os
from django.core.exceptions import ValidationError

def validate_file_extension(value):
        ext = os.path.splitext(value.name)[1]
        valid_extensions = ['.pdf']
        if not ext.lower() in valid_extensions:
            raise ValidationError(u'Unsupported file extension. Only pdf allowed.')

def validate_image_extension(value):
        ext = os.path.splitext(value.name)[1]
        valid_extensions = ['.jpeg','.jpg','.png']
        if not ext.lower() in valid_extensions:
            raise ValidationError(u'Unsupported image extension. Only jpeg, jpg and png allowed.')
    
