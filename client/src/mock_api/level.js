import { http } from "../http";

export const getAllQuestionLevels = () => {
  return http.get("/levels");
};

export const getQuestionLevel = (id) => {
  return http.get(`/levels/${id}`);
};

export const createQuestionLevel = (data) => {
  return http.post("/levels", data);
};

export const updateQuestionLevel = (id, data) => {
  return http.put(`/levels/${id}`, data);
};

export const removeQuestionLevel = (id) => {
  return http.delete(`/levels/${id}`);
};