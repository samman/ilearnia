from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication


class CanView(permissions.BasePermission):

    def has_permission(self, request, view):

        return bool(request.user )

    def has_object_permission(self, request, view, obj):
        try:

            if request.user == obj or request.user.is_admin or request.user.is_staff:
                return True
            return False
        except:
            return False


class OwnView(permissions.BasePermission):
    """
        Only use if trying to compare the request user and the object from different table
    """
    def has_permission(self, request, view):
        return bool(request.user)

    def has_object_permission(self, request, view, obj):
        try:
           
            if request.user == obj.user:
                return True
            return False
        except:
            return False


class IsAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.is_admin and request.user.is_staff and request.user.is_active:
            return True
        return False

class IsInstructor(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        try:

            if request.user.is_hod or request.user.is_admin or request.user.is_internalInstructor or request.user.is_externalInstructor:
                return True
            return False
        except:
            return False

class IsAdminUser(permissions.BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_staff)

    def has_object_permission(self, request, view, obj):
        try:

            if request.user.is_active and request.user.is_staff:
                return True
            return False
        except:
            return False


class IsOwnUser(permissions.BasePermission):
    """
    Allows access only to own users.
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.user == request.user


class TokenAuthSupportCookie(TokenAuthentication):
    """
    Extend the TokenAuthentication class to support cookie based authentication
    """
    def authenticate(self, request):
        # Check if 'auth_token' is in the request cookies.
        # Give precedence to 'Authorization' header.
        if 'auth_token' in request.COOKIES and 'HTTP_AUTHORIZATION' not in request.META:
            return self.authenticate_credentials(
                request.COOKIES.get('auth_token').encode("utf-8")
            )
        return super().authenticate(request)