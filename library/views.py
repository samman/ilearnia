from django.shortcuts import render
from rest_framework import viewsets
from .serializers import * 
from accounts.overide import IsAdminUser, CanView, OwnView
from .models import *
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import  get_object_or_404
import requests, PyPDF2, io

# Create your views here.
class AuthorView(viewsets.ModelViewSet):
   
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    permission_classes = [IsAdminUser]

class GenreView(viewsets.ModelViewSet):
   
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer
    permission_classes = [IsAdminUser]

class LanguageView(viewsets.ModelViewSet):
   
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer
    permission_classes = [IsAdminUser]

class LevelDifficultyView(viewsets.ModelViewSet):
   
    queryset = LevelDifficulty.objects.all()
    serializer_class = LevelDifficultySerializer
    permission_classes = [IsAdminUser]

class BookView(viewsets.ModelViewSet):
   
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['author','Language','genre','level']

    def perform_create(self, serializer):
        serializer.save()
        if str(serializer.validated_data['upload_files']) != 'None':
            # import pdb;pdb.set_trace()
            current_site=get_current_site(self.request).domain
            url='http://' + current_site + '/media/'+self.queryset.last().upload_files.name
            response = requests.get(url)
            with io.BytesIO(response.content) as open_pdf_file:
                read_pdf = PyPDF2.PdfFileReader(open_pdf_file)
                num_pages = read_pdf.getNumPages()
            serializer.save(pages=num_pages)
    
    def perform_update(self,serializer):
        serializer.save()
        # import pdb;pdb.set_trace()
        if str(serializer.validated_data['upload_files']) != 'None':
            current_site=get_current_site(self.request).domain
            url='http://' + current_site + '/media/'+Book.objects.filter(title=serializer.validated_data['title'])[0].upload_files.name
            response = requests.get(url)
            with io.BytesIO(response.content) as open_pdf_file:
                read_pdf = PyPDF2.PdfFileReader(open_pdf_file)
                num_pages = read_pdf.getNumPages()
            serializer.save(pages=num_pages)
        

    # def get_object(self):
    #     if self.action == 'create':
    #         # import pdb; pdb.set_trace()
    #         queryset = self.filter_queryset(self.get_queryset())
    #         filter_kwargs = {self.lookup_field: Book.objects.all().last().id}
    #         # import pdb; pdb.set_trace()
    #         obj = get_object_or_404(queryset, **filter_kwargs)
    #         self.check_object_permissions(self.request, obj)
    #         return obj
    #     else:
    #         return super(BookView, self).get_object()

    # def create(self, request, *args, **kwargs):
    #     # import pdb ; pdb.set_trace()
    #     if self.action != 'create':
    #         return super(BookView, self).create(request, *args, **kwargs)
    #     else:
    #         return super(BookView, self).update(request, *args, **kwargs)  

    

    def get_permissions(self):
        """
            Defining permission to each action requested by user
        """
        # import pdb;pdb.set_trace()
        if self.action in ['create','update','partial_update','delete']:
            permission_classes=[IsAdminUser]

        elif self.action in ['retrieve','list']:
            permission_classes=[IsAuthenticated]

        else:
            return super().get_permissions()

        return [permissions() for permissions in permission_classes]
    
    @action(methods=['get'], detail=False)
    def self_book(self, request, *args, **kwargs):
        books=Book.objects.filter(author=self.request.user.id)
        # import pdb;pdb.set_trace()
        serializers = BookSerializer(books,many=True)

    
        return Response(serializers.data)

class BookmarkView(viewsets.ModelViewSet):
   
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer
    permission_classes = [IsAuthenticated]
    # lookup_field='pk'

    def get_object(self):
        if self.action == 'create':
            # import pdb; pdb.set_trace()
            queryset = self.filter_queryset(self.get_queryset())
            filter_kwargs = {self.lookup_field: Bookmark.objects.filter(user=self.request.user.id)[0].id}
            # import pdb; pdb.set_trace()
            obj = get_object_or_404(queryset, **filter_kwargs)
            self.check_object_permissions(self.request, obj)
            return obj
        else:
            return super(BookmarkView, self).get_object()

    def create(self, request, *args, **kwargs):
        # import pdb; pdb.set_trace()
        if Bookmark.objects.filter(user=self.request.user.id).exists():
            return super(BookmarkView, self).update(request, *args, **kwargs)
        else:
            return super(BookmarkView, self).create(request, *args, **kwargs)

    def perform_update(self, serializer):
        # import pdb;pdb.set_trace()
        list1 = []
        list2=[]
        for y in range(0,len( Bookmark.objects.filter(user=self.request.user)[0].book.values())):
            sers=Bookmark.objects.filter(user=self.request.user)[0].book.values()[y]['id']
            list2.append(sers)
        for x in range(0,len(serializer.validated_data['book'])):
            ser=serializer.validated_data['book'][x].id
            list1.append(ser)
        # import pdb;pdb.set_trace()
        def Union(lst1, lst2): 
            final_list = list(set(lst1) | set(lst2)) 
            return final_list 
        def Intersection(lst1,lst2):
            return list(set(lst1) & set(lst2))
        ram=Union(list1,list2)
        shyam=Intersection(list1,list2)
        for e in shyam:
            if e in ram:
                ram.remove(e)
        serializer.save(book=ram)
        
        # import pdb;pdb.set_trace()
            
        

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    


    @action(methods=['get'], detail=False)
    def self_bookmark(self, request, *args, **kwargs):
        bookmark=Bookmark.objects.filter(user=self.request.user.id)
        serializers = BookmarkSerializer(bookmark,many=True)
        return Response(serializers.data)

    # @action(detail=True, methods=['PUT'])
    # def update_by_user(self, request , *args, **kwargs):
    #     update_bookmark=Bookmark.objects.filter(user=self.request.user.id)

    #     serializers=BookmarkSerializer(update_bookmark,many=True)
    #     serializer = self.get_serializer(instance, data=request.data, partial=partial)
    #     serilaizers.save()
    #     # import pdb; pdb.set_trace()
    #     return Response(serializers.data)
