import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { changePassword } from "../../api/auth";
import { CatchAsync } from "../../util";
import { AuthContext } from "../../contexts";

const ChangePasswordForm = () => {
  const { register, handleSubmit, errors, watch } = useForm();
  const [isPasswordChangeLoading, setIsPasswordChangeLoading] = useState(false);
  const {loggedInUser, setLoggedInUser} = useContext(AuthContext);
  const {setIsLoggedIn} = useContext(AuthContext);
  const history = useHistory();

  const newPassword = watch("new_password");

  const onSubmit = CatchAsync(
    async (data) => {
      setIsPasswordChangeLoading(true);
      await changePassword(
        loggedInUser.id,
        data
      );
      setIsPasswordChangeLoading(false);
      setLoggedInUser(null)
      setIsLoggedIn(false)
      await localStorage.removeItem("jwt_token");
      history.push("/login")
    },
    () => {
      setIsPasswordChangeLoading(false);
  }
  );

  return (
    <>
      <div className="col-sm-8 mx-auto" style={{ minHeight: "40vh" }}>
        <div className="card shadow-sm">
          <h6 className="card-header">Change Password</h6>

          <form
            className="row card-body"
            onSubmit={handleSubmit(onSubmit)}
            noValidate
          >
            <div className="form-group col-sm-12">
              <label htmlFor="input-old-password" className="m-b-5 f-w-600">
                Current Password
              </label>
              <div className="input-group">
                <input
                  type="password"
                  className="form-control text-muted f-w-400"
                  id="input-old-password"
                  name="old_password"
                  placeholder="Enter Password"
                  ref={register({ required: true })}
                />
              </div>
              {errors.old_password?.type === "required" && (
                <span className="text-danger">
                  Current password is required
                </span>
              )}
            </div>
            <div className="form-group col-sm-12">
              <label htmlFor="input-new-password" className="m-b-5 f-w-600">
                New Password
              </label>
              <div className="input-group">
                <input
                  type="password"
                  className="form-control text-muted f-w-400"
                  id="input-new-password"
                  name="new_password"
                  placeholder="Enter Password"
                  ref={register({ required: true, minLength: 8 })}
                />
              </div>
              {errors.new_password?.type === "required" && (
                <span className="text-danger">New Password is required</span>
              )}
              {errors.new_password?.type === "minLength" && (
                <span className="text-danger">
                  Password must have at least 8 characters
                </span>
              )}
            </div>
            <div className="form-group col-sm-12">
              <label htmlFor="input-confirm-password" className="m-b-5 f-w-600">
                Confirm Password
              </label>
              <div className="input-group">
                <input
                  type="password"
                  className="form-control text-muted f-w-400"
                  id="input-confirm-password"
                  name="confirm_password"
                  placeholder="Re-enter password"
                  ref={register({
                    required: true,
                    validate: (value) => value === newPassword,
                  })}
                />
              </div>
              {errors.confirm_password?.type === "required" && (
                <span className="text-danger">
                  Confirm password is required
                </span>
              )}
              {errors.confirm_password?.type === "validate" && (
                <span className="text-danger">
                  New Password and Confirm Password doesn't match
                </span>
              )}
            </div>
            <div className="form-group col-sm-12 d-flex justify-content-between">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={isPasswordChangeLoading}
              >
                Save
                {isPasswordChangeLoading && (
                  <div
                    className="spinner-border spinner-border-sm ml-2"
                    role="status"
                  >
                    <span className="sr-only">Loading...</span>
                  </div>
                )}
              </button>
              {/* <button
                type="button"
                className="btn btn-secondary"
                onClick={goToPrevPage}
              >
                Go Back
              </button> */}
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default ChangePasswordForm;
