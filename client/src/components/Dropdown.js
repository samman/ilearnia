import React, { useState } from "react";
import { Link } from "react-router-dom";

const Dropdown = ({ title, image, items }) => {
  const [isToggled, setToggled] = useState(false);

  return (
    <div className="Dropdown">
      {/* Nav Item - User Information */}
      <li className={`nav-item dropdown no-arrow ${isToggled && "show"}`}>
        <Link
          className="nav-link dropdown-toggle"
          id="userDropdown"
          role="button"
          onClick={() => setToggled(!isToggled)}
          data-toggle="dropdown"
          to="#"
          aria-haspopup="true"
          aria-expanded={isToggled ? true : false}
        >
          <span className="mr-2 d-none d-lg-inline text-gray-600 small">
            {title}
          </span>
          {image && (
            <img
              className="img-profile rounded-circle"
              src={image}
              alt="profile"
            />
          )}
        </Link>
        {/* Dropdown - User Information */}
        <div
          className={`dropdown-menu dropdown-menu-right shadow animated--grow-in ${
            isToggled && "show"
          }`}
          aria-labelledby="userDropdown"
        >
          {items.map((item) => (
            <Link
              className="dropdown-item"
              onClick={item.action}
              to={item.url || "#"}
              key={item.id}
            >
              <i className={item.iconClass}></i>
              {item.label}
            </Link>
          ))}
        </div>
      </li>
    </div>
  );
};

export default Dropdown;
