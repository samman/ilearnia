import React, { useContext, useEffect, useState } from "react";
import { Switch, Route, useHistory } from "react-router-dom";
import { getAllTeams, removeTeam } from "../../api/team";
import { CatchAsync } from "../../util";
import { DepartmentsContext } from "../../contexts/Departments";
import TeamForm from "./TeamForm";
import TeamList from "./TeamList";
import { AuthContext } from "../../contexts";

const initTeam = {
  name: "",
  agenda: "",
  description: "",
  department: [],
  members: [],
};

const searchOptions = [
  {
    name: "Name",
    selector: "name",
  },
];

const Team = ({ match }) => {
  // Hooks
  const [rows, setRows] = useState([]);
  const [isTeamsListManipulated, setIsTeamsListManipulated] = useState(true);
  const [filterBy, setFilterBy] = useState(searchOptions[0]);
  const [searchTerm, setSearchTerm] = useState("");
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [totalRows, setTotalRows] = useState(0);
  const [isTeamDataLoading, setIsTeamDataLoading] = useState(false);
  const history = useHistory();

  // Contexts
  const departments = useContext(DepartmentsContext)?.departments;
  const isDeptDetailLoading = useContext(DepartmentsContext)?.isDeptDetailLoading;
  const { loggedInUser } = useContext(AuthContext);

  useEffect(
    CatchAsync(
      async () => {
        if (isTeamsListManipulated) {
          const params = {
            search: searchTerm,
            filter_by: filterBy.selector,
            limit: perPage,
            offset: (page - 1) * perPage,
          };
          setIsTeamDataLoading(true);
          const res = await getAllTeams(params);
          setRows(res.data.results);
          setTotalRows(res.data.count);
          setIsTeamDataLoading(false);
        }
        setIsTeamsListManipulated(false);
      },
      () => setIsTeamDataLoading(false)
    ),
    [isTeamsListManipulated]
  );

  const remove = CatchAsync(async (row) => {
    await removeTeam(row.id);
    setIsTeamsListManipulated(true);
  });

  const handleFilter = (term) => {
    setSearchTerm(term);
    setIsTeamsListManipulated(true);
  };

  const handleFilterBy = (option) => {
    setFilterBy(option);
    setIsTeamsListManipulated(true);
  };

  const handlePageChange = (page) => {
    setPage(page);
    setIsTeamsListManipulated(true);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setIsTeamDataLoading(true);

    const params = {
      search: searchTerm,
      filter_by: filterBy.selector,
      limit: newPerPage,
      offset: (page - 1) * newPerPage,
    };

    getAllTeams(params).then((res) => {
      setRows(res.data.results);
      setTotalRows(res.data.count);
    });

    setPerPage(newPerPage);

    setIsTeamDataLoading(false);
  };

  return (
    <div>
      <Switch>
        <Route
          path={`${match.path}/create`}
          exact
          component={(props) => <TeamForm {...props} team={initTeam} />}
        />
        <Route
          path=""
          exact
          component={(props) => (
            <TeamList
              isDeptDetailLoading={isDeptDetailLoading}
              isTeamDataLoading={isTeamDataLoading}
              departments={departments}
              handlePerRowsChange={handlePerRowsChange}
              handlePageChange={handlePageChange}
              rows={rows}
              perPage={perPage}
              onDelete={remove}
              totalRows={totalRows}
              loading={isTeamDataLoading}
              loggedInUser={loggedInUser}
              searchOptions={searchOptions}
              filterBy={filterBy}
              handleFilter={handleFilter}
              handleFilterBy={handleFilterBy}
              initTeam={initTeam}
              onAddClick={() =>
                history.push({
                  pathname: `${props.match.path}/create`,
                })
              }
              {...props}
            />
          )}
        />
      </Switch>
    </div>
  );
};

export default Team;
