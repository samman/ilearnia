import React from 'react'
import Modal from 'react-bootstrap/Modal'

const Dialog = (props) => {
  const {
    open,
    handleClose,
    children,
    additionalClass
  } = props;

  return (
    <Modal size="lg" show={open} onHide={handleClose} animation={false} dialogClassName={additionalClass}>
      {children}
    </Modal>
  )
}

export default Dialog;