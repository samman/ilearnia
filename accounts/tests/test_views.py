from .test_setup import TestSetUp
from ..models import User
from ..models import EmailOTP
from rest_framework.test import force_authenticate
from requests import Response

class TestViews(TestSetUp):
    def test_user_cannot_register_with_no_data(self):
        res = self.client.post(self.register_url)
        # import pdb; pdb.set_trace()
        
        self.assertEqual(res.status_code,400)

    def test_user_can_register_correctly(self):
        res=self.client.post(self.register_url,self.user_data,format="json")
        
        # import pdb; pdb.set_trace()
        self.assertEqual(res.status_code,201)
    def test_login_with_unverified_user(self):
        self.client.post(self.register_url,self.user_data,format="json")
        res=self.client.post(self.login_url,self.user_data,format="json")
        # import pdb; pdb.set_trace()
        self.assertEqual(res.status_code,400)

    def test_login_with_verified_user(self):
        self.client.post(self.register_url,self.user_data,format="json")
        
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.save()
        res=self.client.post(self.login_url,self.user_data,format="json")
        # import pdb; pdb.set_trace()
        self.assertEqual(res.status_code,200)
    
    def test_change_password(self):
        self.client.post(self.register_url,self.user_data,format="json")
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.save()
        self.client.post(self.login_url,self.user_data,format="json")
        res=self.client.put(self.change_password_url,self.change_password,format="json")
        # import pdb; pdb.set_trace()
        self.assertEqual(res.status_code,200)
    
    # def test_login_otp_validate(self):
    #     # self.client.post(self.register_url,self.user_data,format="json")
    #     # user = User.objects.get(email=self.user_data['email'])
    #     # user.is_verified=True
    #     # user.save()
    #     # self.client.post(self.login_url,self.user_data,format="json")
    #     res=self.client.post(self.verify_otp,self.otp_code,format="json")
    #     import pdb; pdb.set_trace()
    
    def test_department(self):
        self.client.post(self.register_url,self.user_data,format="json")
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.is_admin=True
        user.is_staff=True
        user.save()
        self.client.post(self.login_url,self.user_data,format="json")
        res=self.client.post(self.department_url,self.department,format="json")
        # import pdb; pdb.set_trace()
        self.assertEqual(res.status_code,201)

        
    
    def test_create_exam(self):
        self.client.post(self.register_url,self.user_data,format="json")
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.is_admin=True
        user.is_staff=True
        user.save()
        self.client.post(self.login_url,self.user_data,format="json")
        self.client.post(self.department_url,self.department,format="json")
        res=self.client.post(self.exam_url,self.exam,format="json")
        # import pdb; pdb.set_trace()
        self.assertEqual(res.status_code,201)
    
    def test_questions(self):
        self.client.post(self.register_url,self.user_data,format="json")
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.is_admin=True
        user.is_staff=True
        user.save()
        self.client.post(self.login_url,self.user_data,format="json")
        res=self.client.post(self.question_url,self.question,format="json")
        self.assertEqual(res.status_code,201)
    
    def test_examquestion(self):
        self.client.post(self.register_url,self.user_data,format="json")
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.is_admin=True
        user.is_staff=True
        user.save()
        self.client.post(self.login_url,self.user_data,format="json")
        self.client.post(self.department_url,self.departments,format="json")
        self.client.post(self.exam_url,self.exams,format="json")
        self.client.post(self.question_url,self.question,format="json")
        res=self.client.post(self.examquestion_url,self.examquestion,format="json")
        self.assertEqual(res.status_code,201)
        # return res.data

    def test_level(self):
        self.client.post(self.register_url,self.user_data,format="json")
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.is_admin=True
        user.is_staff=True
        user.save()
        self.client.post(self.login_url,self.user_data,format="json")
        res=self.client.post(self.level_url,self.level,format="json")
        self.assertEqual(res.status_code,201)
    
    def test_examuser(self):
        self.client.post(self.register_url,self.user_data,format="json")
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.is_admin=True
        user.is_staff=True
        user.save()
        self.client.post(self.login_url,self.user_data,format="json")
        test=self.client.post(self.department_url,self.departments,format="json")
        test=self.client.post(self.exam_url,self.examination,format="json")
        test=self.client.post(self.question_url,self.question,format="json")
        self.client.post(self.examquestion_url,self.examquestions,format="json")
        res=self.client.post(self.examuser_url,self.examuser,format="json")
        self.assertEqual(res.status_code,201)

    def test_result(self):
        self.client.post(self.register_url,self.user_data,format="json")
        user = User.objects.get(email=self.user_data['email'])
        user.is_verified=True
        user.is_admin=True
        user.is_staff=True
        user.save()
        self.client.post(self.login_url,self.user_data,format="json")
        self.client.post(self.department_url,self.department,format="json")
        self.client.post(self.exam_url,self.examine,format="json")
        res=self.client.post(self.result_url,self.result,format="json")
        self.assertEqual(res.status_code,201)
    
    # def test_csv(self):
    #     self.client.post(self.register_url,self.user_data,format="json")
    #     user = User.objects.get(email=self.user_data['email'])
    #     user.is_verified=True
    #     user.is_admin=True
    #     user.is_staff=True
    #     user.save()
    #     self.client.post(self.login_url,self.user_data,format="json")
    #     res=self.client.post(self.csv_url,self.csv,format="json")
    #     self.assertEqual(res.status_code,201)

        






        





        


