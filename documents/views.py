import json
from json import JSONEncoder
from datetime import date
from datetime import timedelta
from django.utils import timezone

import jwt
import datetime
import requests


from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response


from accounts.models import User
from accounts.overide import CanView, IsAdmin
from accounts.serializers import UserHodSerializer
from .serializers import DocumentsSerializer, ZoomSerializer, TeamSerializer, UserHostMeetingSerializer
from rest_framework import viewsets, status
from .models import Documents, Zoom, Team
from django.core.mail import  EmailMultiAlternatives
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend



# Create your views here.
class DocumentsView(viewsets.ModelViewSet):
	queryset = Documents.objects.all()
	serializer_class = DocumentsSerializer
	
	def get_permissions(self):
		if self.action in ['create','destroy']:
			permissions_classes = [IsAuthenticated]
		elif self.action in ['list']:
			permissions_classes = [IsAdminUser]
		elif self.action == 'retrieve':
			permissions_classes = [CanView]
		elif self.action in ['update', 'partial_update']:
			permissions_classes = [CanView]
		else:
			return super().get_permissions()
		return [permissions() for permissions in permissions_classes]




class ZoomView(viewsets.ModelViewSet):
	queryset = Zoom.objects.all()
	serializer_class = ZoomSerializer
	filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
	filterset_fields = ['created_on', 'meeting_type', 'topic', 'agenda']
	search_fields = ['created_on', 'meeting_type', 'topic', 'agenda']
	ordering_fields = ['created_on', 'meeting_type', 'topic', 'agenda']

	def get_permissions(self):

		if self.action in ['create','destroy']:
			permissions_classes = [IsAdminUser, IsAdmin]

		elif self.action in ['list']:
			permissions_classes = [IsAuthenticated]

		elif self.action == 'retrieve':
			permissions_classes = [CanView]

		elif self.action in ['update', 'partial_update']:
			permissions_classes = [IsAdminUser]

		else:
			return super().get_permissions()

		return [permissions() for permissions in permissions_classes]


	def common(self):
		api_key = '4BQyHnQsTEWHsG8tzUdTIw'
		api_secret_key = 'hLTTJOnR2MTZMgB7taYykLio1YdzBYTkCcD6'

		payload = {
			'iss': api_key,
			'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=24),
		}
		token = jwt.encode(payload, api_secret_key).decode("utf-8")

		headers = {
			'authorization': "Bearer " + token,
			'content-type': "application/json"
		}
		return headers
	
	def perform_create(self, serializer):

		if serializer.validated_data["meeting_type"]=="Instant":
			self.type=1
		elif serializer.validated_data["meeting_type"]=="Scheduled":
			self.type=2
		elif serializer.validated_data["meeting_type"]=="Recurring Instant":
			self.type=3
		else:
			self.type=8


	# Get API Key and API Secret

		headers=self.common()
		url = 'https://api.zoom.us/v2/users/amar.shrestha@techrida.com/meetings'

		# duration
		try:
			duration=serializer.validated_data["duration"]
		except:
			duration=60
		#
		try:
			timezone=serializer.validated_data["timezone"]
		except:
			timezone='Asia/Kathmandu'

		try:
			schedule_for=serializer.validated_data["schedule_for"]
		except:
			schedule_for=None
		
		try:
			agenda=serializer.validated_data['agenda']
		except:
			agenda=None

		try:
			agenda=serializer.validated_data["agenda"]
		except: 
			agenda=None


		if self.type==1 or self.type==3:
			myData = {
				"topic": serializer.validated_data["topic"],
				"type": self.type,
				"duration":duration,
				"timezone": timezone,
				"agenda": agenda,
				"schedule_for": schedule_for

			}
		else:
			myData = {
				"topic": serializer.validated_data["topic"],
				"type": self.type,
				"duration": duration,
				"timezone": timezone,
				"start_time": serializer.validated_data["start_time"].isoformat(),
				"agenda": agenda,
				"schedule_for": schedule_for
			}


		x = requests.post(url, headers=headers, json=myData)
		serializer.save(
			created_by=self.request.user,
			meeting_id=x.json()["id"],
			start_url=x.json()["start_url"],
			join_url=x.json()["join_url"],
			occurance_id=x.json()["uuid"],
			schedule_for=schedule_for
			)
		user=serializer.data

		team=user['team']
		# team_department=Team.objects.filter(id=team).values_list('department',flat=True)

		# user_status=user_email.filter(is_staff=True).values_list('email',flat=True)
		member=Team.objects.filter(id=team).values_list('members',flat=True)
		user_email = User.objects.filter(id__in=member).values_list('email',flat=True)
		user_email_list=list(user_email)


		if myData['type']==1 or myData['type']== 3:
			email_body = 'You have been scheduled for a meeting. Please click on the link to join the meeting ' + x.json()["join_url"]
		else:
			email_body = 'You have been scheduled for a meeting. Please click on the link to join the meeting. '+'\n' + x.json()["join_url"]\
						+'\n'+'The meeting is scheduled meeting so, meeting start at time: '+user['start_time']

		
			subject, from_email, to = 'Meeting Notification', self.request.user.email, user_email_list
			msg = EmailMultiAlternatives(subject, email_body, from_email, to)
			msg.send()
		if schedule_for == None:
			pass
		else:
			email_body='Hi '+schedule_for+'\n'+'Please use below link to host zoom meeting.'+'\n'+user['start_url']
			subject, from_email, to = 'Your Meeting - Prabhubank', 'no-reply@prabhubank.com', [schedule_for]
			msg=EmailMultiAlternatives(subject, email_body, from_email, to)
			msg.send()


		return Response(serializer.data, status=status.HTTP_201_CREATED)


	def perform_update(self, serializer):

		"""
		SERIALIZER TO UPDATE ZOOM OBJECTS AND TEAM MEMBERS
		"""

		old_members = list(Team.objects.filter(id=serializer.validated_data["team"]["id"]).values_list('members', flat=True))
		new_members=[]
		data=serializer.validated_data
		obj=self.get_object()
		zoom = Zoom.objects.get(id=obj.id)
		mem_obj=data['team']
		if 'members' in mem_obj:

		
			for m in mem_obj['members']:
				new_members.append(m.id)
		
			if len(old_members)<len(new_members):
				user_email_id=list(set(new_members)-set(old_members))
		
		
				user_email = User.objects.filter(id__in=user_email_id).values_list('email', flat=True)
				user_email_list = list(user_email)

				if obj.meeting_type == 'Instant' or obj.meeting_type == 'Recurring Instant':
					email_body = 'You have been scheduled for a meeting. Please click on the link to join the meeting ' + \
								 zoom.join_url
				else:
					try:
						email_body = 'You have been scheduled for a meeting. Please click on the link to join the meeting. ' + '\n' + \
									 zoom.join_url + '\n' + 'The meeting is scheduled meeting so, meeting start at time: ' + \
									 str(serializer.validated_data["start_time"])
					except:
						email_body = 'You have been scheduled for a meeting. Please click on the link to join the meeting. ' + '\n' + \
									 zoom.join_url + '\n' + 'The meeting is scheduled meeting so, meeting start at time: ' + \
									 str(zoom.start_time)
			else:
				user_email_id = list(set(old_members) - set(new_members))
				user_email = User.objects.filter(id__in=user_email_id).values_list('email', flat=True)
				user_email_list = list(user_email)
				email_body = 'You have been cancelled from the meeting.'

			subject, from_email, to = 'Meeting Notification', self.request.user.email, user_email_list
			msg = EmailMultiAlternatives(subject, email_body, from_email, to)
			msg.send()

		zoom_data = serializer.save()

		if zoom_data.meeting_type=="Instant":
			self.type=1
		elif zoom_data.meeting_type=="Scheduled":
			self.type=2
		elif zoom_data.meeting_type=="Recurring Instant":
			self.type=3
		else:
			self.type=8


		headers=self.common()

		meeting_id=zoom_data.meeting_id

		url = 'https://api.zoom.us/v2/meetings/'+str(meeting_id)
		if self.type==2 and self.type==8:
			myData = {
				"topic": zoom_data.topic,
				"type": self.type,
				"duration": zoom_data.duration,
				"timezone": zoom_data.timezone,
				"start_time": zoom_data.start_time.isoformat(),
				"agenda": zoom_data.agenda,
				"schedule_for": zoom_data.schedule_for
			}
		else:
			myData = {
				"topic": zoom_data.topic,
				"type": self.type,
				"duration": zoom_data.duration,
				"timezone": zoom_data.timezone,
				"agenda": zoom_data.agenda,
				"schedule_for": zoom_data.schedule_for
			}

		x = requests.patch(url, headers=headers, json=myData)


		return Response(zoom_data,status=status.HTTP_200_OK)

	def perform_destroy(self, instance):

		headers=self.common()
		url = 'https://api.zoom.us/v2/meetings/' + str(instance.meeting_id)
		requests.delete(url, headers=headers)
		instance.delete()
		return Response({'message':'You have succesfully deleted the meeting'},status=status.HTTP_200_OK)



	@action(methods=['get'], detail=False)
	def meetings(self, request, *args, **kwargs):
		headers = self.common()
		url = 'https://api.zoom.us/v2/users/amar.shrestha@techrida.com/meetings'

		x = requests.get(url, headers=headers)

		return Response(x.json())

	@action(methods=['get'], detail=False)
	def member(self, request, *args, **kwargs):
		try:
			user=request.user
			team=Team.objects.filter(members=user).values_list('id',flat=True)
			meeting=Zoom.objects.filter(team__in=team)
			serializer=ZoomSerializer(meeting,many=True)
			return Response(serializer.data)
		except:
			return Response({'status':'You donot have any meeting to attend'})

	@action(methods=['get'], detail=False, permission_classes=[IsAdmin])		
	def instantmeeting(self, request, *args, **kwargs):
		zoom=Zoom.objects.filter(meeting_type="Instant")
		serializer=ZoomSerializer(zoom, many=True)
		return Response(serializer.data, status=200)

	@action(methods=['get'], detail=False, permission_classes=[IsAdmin])
	def scheduledmeeting(self, request, *args, **kwargs):
		zoom=Zoom.objects.filter(meeting_type="Scheduled")
		serializer=ZoomSerializer(zoom, many=True)
		return Response(serializer.data, status=200)

	@action(methods=['get'], detail=False, url_path='userhost')            
	def userhostinstant(self, request, *args, **kwargs):
		user=self.request.user
		twentyfour_h_ago=timezone.now()-timezone.timedelta(hours=24)
		previous_queryset=Zoom.objects.filter(schedule_for=user).filter(meeting_type='Instant').filter(created_on__lt=twentyfour_h_ago)
		all_queryset=Zoom.objects.filter(schedule_for=user).filter(meeting_type='Instant')
		queryset=list(set(all_queryset)-set(previous_queryset))
		serializer=UserHostMeetingSerializer(queryset, many=True)
		return Response(serializer.data, status=200)

	@action(methods=['get'], detail=False, url_path='userhostscheduled')            
	def userhostscheduled(self, request, *args, **kwargs):
		user=self.request.user
		zoom=Zoom.objects.filter(schedule_for=user).filter(meeting_type='Scheduled')
		serializer=UserHostMeetingSerializer(zoom, many=True)
		return Response(serializer.data, status=200)

class TeamView(viewsets.ModelViewSet):
	queryset = Team.objects.all()
	serializer_class = TeamSerializer



	def get_permissions(self):

		if self.action in ['create']:
			permissions_classes = [IsAdminUser, IsAdmin]

		elif self.action in ['list','destroy']:
			permissions_classes = [IsAdmin]

		elif self.action == 'retrieve':
			permissions_classes = [CanView]

		elif self.action in ['update', 'partial_update']:
			permissions_classes = [IsAdminUser]

		else:
			return super().get_permissions()

		return [permissions() for permissions in permissions_classes]



	def perform_create(self, serializer):
		serializer.save(created_by=self.request.user)

	def destroy(self, request, *args, **kwargs):
		instance=self.get_object()
		try:
			self.perform_destroy(instance)
			return Response({'detail': 'successfully deleted'}, status=status.HTTP_204_NO_CONTENT)
		except:
			return Response({'detail': 'Please delete all related instances'}, status=status.HTTP_400_BAD_REQUEST)



	@action(methods=['get'], detail=False,permission_classes=[IsAdminUser])
	def member(self, request, *args, **kwargs):
		try:
			user=request.user
			dep_user=User.objects.filter(member=request.user)
			serializer=UserHodSerializer(dep_user,many=True)
			return Response(serializer.data)
		except ObjectDoesNotExist:
			raise Http404

