import { http } from "../http";

export const getAllExams = () => {
  return http.get("/exams");
};

export const getExam = (id) => {
  return http.get(`/exams/${id}`);
};

export const createExam = (data) => {
  return http.post("/exams", data);
};

export const updateExam = (id, data) => {
  return http.put(`/exams/${id}`, data);
};

export const removeExam = (id) => {
  return http.delete(`/exams/${id}`);
};