import React, { useEffect } from "react";

import { useForm } from "react-hook-form";

const MeetingForm = ({
  meeting,
  onClose,
  onSubmit,
  isCreateLoading,
  teams,
}) => {
  const {
    register,
    handleSubmit,
    watch,
    getValues,
    setValue,
    errors,
  } = useForm(
    !!meeting && {
      defaultValues: { ...meeting, dob: meeting.dob.split("T")[0] },
    }
  );

  console.log(teams);

  const meetingType = watch("meeting_type");

  useEffect(() => {
    (async () => {
      if (meetingType === "Scheduled" && !getValues(["duration"])[0]) {
        setValue("duration", 60);
      }
    })();
  }, [meetingType]);

  return (
    <div>
      <div className="row m-l-0 m-r-0">
        <div className="col-sm-12">
          <div className="card-block">
            <h6 className="m-b-20 p-b-5 b-b-default f-w-600">
              Meeting Details
            </h6>

            <form className="row" noValidate onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group col-sm-12">
                <label htmlFor="input-topic" className="m-b-5 f-w-600">
                  Topic
                </label>
                <input
                  type="text"
                  className="form-control text-muted f-w-400"
                  id="input-topic"
                  name="topic"
                  placeholder="Enter topic"
                  ref={register({ required: true })}
                />
                {errors.topic?.type === "required" && (
                  <span className="text-danger">Topic is required</span>
                )}
              </div>

              <div className="form-group col-sm-12">
                <label htmlFor="input-team" className="m-b-5 f-w-600">
                  Team
                </label>
                <small id="teamHelp" class="form-text text-muted">
                  Create a new team if your desired team doesnot exists.
                </small>
                <select
                  className="form-control"
                  name="team"
                  ref={register({ required: true })}
                >
                  <option selected>-- Select --</option>
                  {teams.map((team) => (
                    <option value={team.id} key={team.id}>
                      {team.name}
                    </option>
                  ))}
                </select>
                {errors.team?.type === "required" && (
                  <span className="text-danger">Team is required</span>
                )}
              </div>

              <div className="form-group col-sm-12">
                <label htmlFor="input-type" className="m-b-5 f-w-600">
                  Meeting type
                </label>
                <select
                  className="form-control"
                  name="meeting_type"
                  ref={register({ required: true })}
                >
                  <option value="Instant">Instant</option>
                  <option value="Scheduled">Scheduled</option>
                </select>
                {errors.meeting_type?.type === "required" && (
                  <span className="text-danger">Meeting type is required</span>
                )}
              </div>

              {meetingType === "Scheduled" && (
                <>
                  <div className="form-group col-sm-6">
                    <label htmlFor="input-date" className="m-b-5 f-w-600">
                      Start Date
                    </label>
                    <input
                      type="date"
                      className="form-control text-muted f-w-400"
                      id="input-date"
                      name="date"
                      placeholder="Enter Start Date"
                      ref={register({ required: true })}
                      required
                    />
                    {errors.date && errors.date.type === "required" && (
                      <span className="text-danger">Date is required</span>
                    )}
                  </div>
                  <div className="form-group col-sm-6">
                    <label htmlFor="input-time" className="m-b-5 f-w-600">
                      Start Time
                    </label>
                    <input
                      type="time"
                      className="form-control text-muted f-w-400"
                      id="input-time"
                      name="time"
                      placeholder="Enter Start Time"
                      ref={register({ required: true })}
                      required
                    />
                    {errors.time && errors.time.type === "required" && (
                      <span className="text-danger">Time is required</span>
                    )}
                  </div>
                  <div className="form-group col-sm-12">
                    <label htmlFor="input-duration" className="m-b-5 f-w-600">
                      Duration
                    </label>
                    <div className="input-group mb-3">
                      <input
                        type="number"
                        className="form-control text-muted f-w-400"
                        id="input-duration"
                        name="duration"
                        placeholder="Enter duration"
                        ref={register({ value: 60 })}
                        required
                      />
                      <div className="input-group-append">
                        <span className="input-group-text">min</span>
                      </div>
                    </div>
                    {errors.duration && errors.duration.type === "required" && (
                      <span className="text-danger">Duration is required</span>
                    )}
                  </div>
                </>
              )}

              <div className="form-group col-sm-12">
                <label htmlFor="input-agenda" className="m-b-5 f-w-600">
                  Agenda
                </label>
                <textarea
                  className="form-control text-muted"
                  id="input-agenda"
                  name="agenda"
                  rows="3"
                  placeholder="Enter meeting agenda"
                  ref={register}
                ></textarea>
              </div>
              <div className="form-group col-sm-12 d-flex justify-content-between">
                <button
                  type="submit"
                  className="btn btn-primary"
                  disabled={isCreateLoading || !teams.length}
                >
                  Save
                  {!teams.length}
                  {isCreateLoading && (
                    <div
                      className="spinner-border spinner-border-sm ml-2"
                      role="status"
                    >
                      <span className="sr-only">Loading...</span>
                    </div>
                  )}
                </button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  onClick={onClose}
                >
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MeetingForm;
