from django.conf import settings
from django.db import models

# Create your models here.
from accounts.models import User, Department


class Documents(models.Model):
    """
    Model for Documents Upload by User
    """
    user=models.ForeignKey(User,on_delete=models.SET_DEFAULT, default=1)
    file_name = models.CharField(max_length=255, default=True, blank=True)
    file_path = models.FileField(upload_to='documents/%Y/%m/%d/', blank=True, null=True)


class Team(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    created_by=models.CharField(max_length=50,null=True,blank=True)
    name = models.CharField(max_length=40,null=True)
    agenda = models.CharField(max_length=40,null=True)
    description = models.TextField(max_length=200,null=True)
    department=models.ManyToManyField(Department, blank=True)
    members = models.ManyToManyField(settings.AUTH_USER_MODEL,related_name='team', blank=True)

    class Meta:
        verbose_name_plural = 'Teams'

    def __str__(self):
        return self.name




class Zoom(models.Model):
    MEETING_TYPE = (
        ('Instant', 'Instant'),
        ('Scheduled', 'Scheduled'),
        ('Recurring Instant', 'Recurring Instant'),
        ('Recurring Scheduled', 'Recurring Scheduled')
    )
    created_on=models.DateTimeField(auto_now_add=True)
    created_by=models.CharField(max_length=50)
    meeting_id=models.CharField(max_length=255,null=True,blank=True)
    topic=models.CharField(max_length=55)
    agenda=models.CharField(max_length=100,default="Normal meeting")
    start_time=models.DateTimeField(null=True,blank=True)
    meeting_type=models.CharField(max_length=30,choices=MEETING_TYPE)
    timezone=models.CharField(max_length=50,default="Asia/Kathmandu")
    start_url=models.TextField(max_length=1000,null=True,blank=True)
    join_url=models.CharField(max_length=200,null=True,blank=True)
    duration=models.IntegerField(default=60,null=True,blank=True)
    schedule_for=models.CharField(max_length=50,null=True,blank=True)
    occurance_id=models.CharField(max_length=50,null=True,blank=True)
    team=models.ForeignKey(Team,on_delete=models.RESTRICT,null=True,blank=True,related_name='team')


    class Meta:
        ordering= ('-created_on',)



    class Meta:
        ordering = ('-created_on', )


