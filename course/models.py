import urllib

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http import request
from moviepy.editor import *
from PIL import Image

from django.contrib.postgres.fields import ArrayField
from django.db import models


from accounts.models import Department



class Instructor(models.Model):
    name=models.CharField(max_length=55,null=True)
    positon=ArrayField(models.CharField(max_length=50))
    email=models.CharField(max_length=55, null=True,blank=True)


class Category(models.Model):
    created_by=models.CharField(max_length=55)
    created_data=models.DateTimeField(auto_now_add=True)
    updated_by=models.CharField(max_length=55,null=True,blank=True)
    updated_date=models.DateTimeField(null=True,blank=True)
    title=models.CharField(max_length=25)
    short_description=models.CharField(max_length=255, null=True, blank=True)
    thumbnail=models.ImageField(upload_to='video_category/%Y/%m/%d/',null=True,blank=True)
    department=models.ForeignKey(Department,on_delete= models.RESTRICT)


class SubCategory(models.Model):
    created_by = models.CharField(max_length=55)
    created_data = models.DateTimeField(auto_now_add=True)
    updated_by = models.CharField(max_length=55, null=True, blank=True)
    updated_date = models.DateTimeField(null=True, blank=True)
    title = models.CharField(max_length=25)
    short_description = models.CharField(max_length=255, null=True, blank=True)
    thumbnail = models.ImageField(upload_to='video_sub_category/%Y/%m/%d/', null=True, blank=True)
    category=models.ForeignKey(Category,on_delete=models.PROTECT)


class Course(models.Model):
    created_by = models.CharField(max_length=55)
    created_data = models.DateTimeField(auto_now_add=True)
    updated_by = models.CharField(max_length=55, null=True, blank=True)
    updated_date = models.DateTimeField(null=True, blank=True)
    title = models.CharField(max_length=25)
    agenda=models.CharField(max_length=55,default="Learning")
    short_description = models.CharField(max_length=255, null=True, blank=True)
    thumbnail = models.ImageField(upload_to='video_course/%Y/%m/%d/', null=True, blank=True)
    is_published=models.BooleanField(default=True)
    short_video=models.FileField(upload_to='video_course_video/%Y/%m/%d/',null=True, blank=True)
    sub_category=models.ForeignKey(SubCategory, on_delete=models.PROTECT)
    skill_gain=ArrayField(models.CharField(max_length=50,null=True,blank=True),null=True,blank=True)
    instructor=models.ForeignKey(Instructor, on_delete=models.RESTRICT,null=True,blank=True)




class Lesson(models.Model):
    created_by = models.CharField(max_length=55)
    created_data = models.DateTimeField(auto_now_add=True)
    updated_by = models.CharField(max_length=55, null=True, blank=True)
    updated_date = models.DateTimeField(null=True, blank=True)
    title = models.CharField(max_length=25)
    short_description = models.CharField(max_length=255, null=True, blank=True)
    duration=models.IntegerField()
    thumbnail = models.ImageField(upload_to='video_lesson/%Y/%m/%d/', null=True, blank=True)
    video = models.FileField(upload_to='video_lesson_video/%Y/%m/%d/')

    def save(self,*args,**kwargs):
        clip=VideoFileClip(self.video)
        self.duration=(clip.duration/60)
        frame_data=clip.get_frame(1)
        img=Image.fromarray(frame_data,'RGB')
        self.thumbnail=img

        super().save(*args, **kwargs)



class LessonVideoDocument(models.Model):
    created_by = models.CharField(max_length=55)
    created_data = models.DateTimeField(auto_now_add=True)
    updated_by = models.CharField(max_length=55, null=True, blank=True)
    updated_date = models.DateTimeField(null=True, blank=True)
    name = models.CharField(max_length=25)
    file_path = models.FileField(upload_to='video_lesson_document/%Y/%m/%d/')

class Syllabus(models.Model):
    title=models.CharField(max_length=55)
    description=models.TextField(max_length=955, null=True,blank=True)
    lesson=models.ManyToManyField(Lesson)


@receiver(post_save,sender=Course)
def save_video(sender,instance,created,**kwargs):
    import pdb;pdb.set_trace()
    clip=VideoFileClip(instance.short_video.path)
    frame_data=clip.get_frame(1)
    instance.thumbnail=Image.fromarray(frame_data,'RGB')
    instance.thumbnail.save('thumbnail_video.jpeg')

    instance.save()

