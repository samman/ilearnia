import React, { useContext } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

// Pages
import Login from "./pages/login/Login";
import Signup from "./pages/signup/Signup";
import UsersList from "./pages/users/UserList";
import DepartmentList from "./pages/departments/DepartmentList";
import Team from "./pages/teams/Team";
import MeetingList from "./pages/meetings/MeetingList";
import Exam from "./pages/exam/Exam";
import ExamParticipants from "./pages/exam/examParticipants/ExamParticipants";
import Dashboard from "./pages/dashboard/Dashboard";
import Profile from "./pages/profile/Profile";
import ChangePasswordForm from "./pages/changePassword/ChangePasswordForm";
import Verify from "./pages/Verify/Verify";

// Auth Context
import { AuthContext } from "./contexts/Auth";

// Guarded Route
import GuardedRoute from "./GuardedRoute";

// Page Not Found page
import PageNotFound from "./pages/PageNotFound/PageNotFound";

export default function Routes() {
  const { isLoggedIn } = useContext(AuthContext);

  return (
    <Switch>
      <Route
        path="/"
        exact
        render={() =>
          isLoggedIn ? <Redirect to="/dashboard" /> : <Redirect to="/login" />
        }
      />
      <Route path="/signup" exact component={Signup} />
      <GuardedRoute
        path="/dashboard"
        exact
        component={Dashboard}
        auth={isLoggedIn}
      />
      <GuardedRoute
        path="/login"
        exact
        component={Login}
        redirectTo="/dashboard"
        auth={!isLoggedIn}
      />
      <GuardedRoute
        path="/departments"
        exact
        component={DepartmentList}
        auth={isLoggedIn}
      />
      <GuardedRoute path="/teams" component={Team} auth={isLoggedIn} />
      <GuardedRoute
        path="/users"
        exact
        component={UsersList}
        auth={isLoggedIn}
      />
      <GuardedRoute
        path="/users/:id"
        exact
        component={Profile}
        auth={isLoggedIn}
      />
      <GuardedRoute
        path="/profile"
        exact
        component={Profile}
        auth={isLoggedIn}
      />
      <GuardedRoute
        path="/meetings"
        exact
        component={MeetingList}
        auth={isLoggedIn}
      />
      <GuardedRoute
        path="/exams/:id/participants"
        component={ExamParticipants}
        auth={isLoggedIn}
      />
      <GuardedRoute path="/exams" component={Exam} auth={isLoggedIn} />
      <GuardedRoute
        path="/change_password"
        exact
        component={ChangePasswordForm}
        auth={isLoggedIn}
      />
      <Route path="/verify" exact component={Verify} />
      <Route component={PageNotFound} />,
    </Switch>
  );
}
