import React, { createContext, useState, useCallback } from "react";

export const APIErrorContext = createContext({
    error: null,
    addError: () => {},
    removeError: () => {}
  });

export const APIErrorContextProvider = (props) => {

  const [error, setError] = useState(null);

  const removeError = () => setError(null);

  const addError = (err) => setError(err);

  const contextValue = {
    error,
    addError: useCallback(err => addError(err), []),
    removeError: useCallback(() => removeError(), [])
  };
  return (
    <APIErrorContext.Provider value={contextValue}>
      {props.children}
    </APIErrorContext.Provider>
  );
};
