import axios from "axios";

function publishResult(examId, userId) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/result/`, {
      exam: examId,
      user: userId
  },{
    headers: {
      authorization: `Token ${token}`,
    }
  });
}

// Makes a GET request for the result of the authenticated user
function getResult(examId){
    const token = localStorage.getItem("jwt_token");
    return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/result/${examId}/exam_user_result/`,{
      headers: {
        authorization: `Token ${token}`,
      }
    });
}

// Makes a GEt request for the result of a specified user
function getUserResult(userId, examId){
  const token = localStorage.getItem("jwt_token");
    return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/result/particularuser/${userId}/${examId}/`,{
      headers: {
        authorization: `Token ${token}`,
      }
    });
}

export { publishResult, getResult, getUserResult };
