import { http } from "../http";

export const getAllMeetings = (filterBy = '', searchTerm = '') => {
  let params = {};
  if (filterBy && searchTerm) {
    params.filterBy = filterBy;
    params.searchTerm = searchTerm;
  }
  return http.get("/meetings", { params });
};

export const getMeeting = (id) => {
  return http.get(`/meetings/${id}`);
};

export const createMeeting = (data) => {
  return http.post("/meetings", data);
};

export const updateMeeting = (id, data) => {
  return http.put(`/meetings/${id}`, data);
};

export const removeMeeting = (id) => {
  return http.delete(`/meetings/${id}`);
};