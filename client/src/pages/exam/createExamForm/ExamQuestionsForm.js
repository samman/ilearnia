import React, { useEffect } from "react";

const ExamQuestionsForm = ({
  questions,
  register,
  errors,
  remove,
  append,
  initQuestion,
  moveToFirstPage,
  questionLevels,
  watch,
  visible,
  isSaveLoading,
  onOpenUploadCSV
}) => {
  return (
    <div hidden={!visible}>
      {questions.map((question, index) => (
        <div key={`question-${index}`}>
          <div className="form-group qtns-opts">
            <div className="qtn">
              <label
                htmlFor={`questions[${index}]-questions`}
                className="m-b-5 f-w-600"
              >
                Question
              </label>
              <div className="d-flex align-items-center">
                <input
                  type="text"
                  className="form-control form-control-lg"
                  id={`questions[${index}]-question`}
                  placeholder="Enter question"
                  name={`questions[${index}].question`}
                  ref={register({ required: true })}
                />
              </div>
              {errors.questions?.[index].question?.type === "required" && (
                <span className="text-danger">Question is required</span>
              )}
            </div>

            <div className="opts mt-2">
              <label htmlFor={`questions[${index}]`} className="m-b-5 f-w-600">
                Options
              </label>
              <small
                id={`questions[${index}]-option-help`}
                className="form-text text-muted mt-0 mb-1"
              >
                Also, select the corrent answer.
              </small>
              <div className="col-sm-12">
                <div className="row">
                  <div className="col-6 d-flex align-items-center">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="correct_answer"
                      id={`questions[${index}]-correct-answer-1`}
                      name={`questions[${index}].correct_answer`}
                      ref={register({
                        required: true,
                      })}
                      value={watch(`questions[${index}].choice1`)}
                    />
                    <input
                      type="text"
                      className="form-control"
                      placeholder="First Option"
                      id={`questions[${index}]-option-1`}
                      name={`questions[${index}].choice1`}
                      ref={register({
                        required: true,
                      })}
                    />{" "}
                  </div>

                  <div className="col-6 d-flex align-items-center">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="correct_answer"
                      name="correct_answer"
                      id={`questions[${index}]-correct-answer-2`}
                      name={`questions[${index}].correct_answer`}
                      ref={register({
                        required: true,
                      })}
                      value={watch(`questions[${index}].choice2`)}
                    />
                    <input
                      type="text"
                      className="form-control"
                      id={`questions[${index}]-option-2`}
                      placeholder="Second Option"
                      id={`questions[${index}]-option-2`}
                      name={`questions[${index}].choice2`}
                      ref={register({
                        required: true,
                      })}
                    />{" "}
                  </div>
                </div>

                <div className="row">
                  <div className="col-6 d-flex align-items-center">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="correct_answer"
                      name="correct_answer"
                      id={`questions[${index}]-correct-answer-3`}
                      name={`questions[${index}].correct_answer`}
                      ref={register({
                        required: true,
                      })}
                      value={watch(`questions[${index}].choice3`)}
                    />
                    <input
                      type="text"
                      className="form-control"
                      id={`questions[${index}]-option-3`}
                      placeholder="Third Option"
                      id={`questions[${index}]-option-3`}
                      name={`questions[${index}].choice3`}
                      ref={register({
                        required: true,
                      })}
                    />{" "}
                  </div>
                  <div className="col-6 d-flex align-items-center">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="correct_answer"
                      name="correct_answer"
                      id={`questions[${index}]-correct-answer-4`}
                      name={`questions[${index}].correct_answer`}
                      ref={register({
                        required: true,
                      })}
                      value={watch(`questions[${index}].choice4`)}
                    />
                    <input
                      type="text"
                      className="form-control"
                      id={`questions[${index}]-option-4`}
                      placeholder="Fourth Option"
                      id={`questions[${index}]-option-4`}
                      name={`questions[${index}].choice4`}
                      ref={register({
                        required: true,
                      })}
                    />
                  </div>
                </div>

                <div>
                  {(errors.questions?.[index]?.choice1?.type === "required" ||
                    errors.questions?.[index]?.choice2?.type === "required" ||
                    errors.questions?.[index]?.choice3?.type === "required" ||
                    errors.questions?.[index]?.choice4?.type ===
                      "required") && (
                    <span className="text-danger row">All options is required</span>
                  )}

                  {errors.questions?.[index]?.correct_answer?.type ===
                    "required" && (
                    <span className="text-danger row">
                      You must select a correct answer
                    </span>
                  )}
                </div>
              </div>
            </div>
            <div className="mt-2">
              <label
                htmlFor={`questions[${index}]-concise-description`}
                className="m-b-5 f-w-600"
              >
                Level
              </label>
              {questionLevels?.length ? (
                questionLevels.map((level) => (
                  <div className="form-check" key={level.id}>
                    <input
                      type="radio"
                      className="form-check-input"
                      id={`${level.id}-radio`}
                      name={`questions[${index}].level`}
                      ref={register({ required: true })}
                      value={level.id}
                    />
                    <label
                      className="form-check-label"
                      htmlFor={`${level.id}-radio`}
                    >
                      {level.name}
                    </label>
                  </div>
                ))
              ) : (
                <div>
                  <span className="font-italic">
                    No level exists. Please first create levels.
                  </span>
                </div>
              )}
              {errors.questions?.[index]?.level?.type === "required" && (
                <span className="text-danger">You must select a level</span>
              )}
            </div>
            <div className="mt-2">
              <label
                htmlFor={`questions[${index}]-concise-description`}
                className="m-b-5 f-w-600"
              >
                Concise Description
              </label>
              <textarea
                className="form-control col-sm-12"
                id={`questions[${index}]-concise-description`}
                placeholder="You can give hints to the question"
                id={`questions[${index}]-concise-description`}
                name={`questions[${index}].concise_description`}
                ref={register}
              />
            </div>
            {index > 0 && (
              <div className="row d-flex align-items-center justify-content-end">
                <button
                  type="button"
                  className="btn btn-danger mt-2"
                  onClick={() => remove(index)}
                >
                  <i className="fas fa-trash"></i>
                </button>
              </div>
            )}
            <hr />
          </div>
        </div>
      ))}
      <button
        type="button"
        className="btn btn-success col-12"
        onClick={() => append(initQuestion)}
      >
        <i className="fas fa-plus"></i>
      </button>
      <div className="d-flex flex-row justify-content-between align-items-center p-3 bg-white">
        <button
          className="btn btn-primary d-flex align-items-center btn-danger"
          type="button"
          onClick={moveToFirstPage}
        >
          <i className="fa fa-angle-left mt-1 mr-1"></i>&nbsp;Go Back
        </button>
        <button
          className="btn btn-primary border-success align-items-center btn-success"
          type="submit"
          disabled={isSaveLoading}
        >
          Save
          {isSaveLoading && (
            <div
              className="spinner-border spinner-border-sm ml-2"
              role="status"
            >
              <span className="sr-only">Loading...</span>
            </div>
          )}
          <i className="fa fa-angle-right ml-2"></i>
        </button>
      </div>
      <div className="d-flex justify-content-end">
        <p className="btn d-inline text-info mt-4" onClick={onOpenUploadCSV}>
          Upload csv instead
        </p>
      </div>
    </div>
  );
};

export default ExamQuestionsForm;
