//React imports
import React from "react";
import DataTable from "react-data-table-component";

const DepartmentTable = (props) => {
    const handleDelete = (row) => props.onDelete(row);

    const columns = [
        {
            name: "Department Name",
            selector: "dept_name",
            sortable: true
        },
        {
            name: 'Action',
            button: true,
            cell: (row) =>
                props.loggedInUser?.is_admin ?
                    <div>
                        <button className="btn btn-primary btn-sm ml-1" onClick={() => handleDelete(row)}>
                            <i className="fas fa-trash"></i>
                        </button>
                    </div> : '',
            ignoreRowClick: true,
            allowOverflow: true,
        }
    ]

    return (
        <DataTable
            noHeader={true}
            pagination={true}
            paginationServer={true}
            columns={columns}
            data={props.rows}
            highlightOnHover={true}
            pointerOnHover={true}
            onRowClicked={props.onRowClicked}
            paginationPerPage={props.perPage}
            onChangeRowsPerPage={props.handlePerRowsChange}
            onChangePage={props.handlePageChange}
            progressPending={props.loading}
            paginationTotalRows={props.totalRows}
        />
    )
}

export default DepartmentTable;
