import React, { useState, useEffect, useContext } from "react";
import UserForm from "../users/UserForm";
import Dialog from "../../components/Dialog";

import { updateUser, getUser } from "../../api/user";
import { CatchAsync } from "../../util";

// reactstrap components
import {
  Button,
  Card,
  FormGroup,
  Form,
  FormControl,
  Row,
  Col,
  ListGroup,
} from "react-bootstrap";

import Logo from "../../assets/img/logo/logo.svg";
import { DepartmentsContext } from "../../contexts";

const Profile = (props) => {
  //Hooks
  const [user, setUser] = useState(null);
  const [isUserLoading, setIsUserLoading] = useState(true);
  const [isUpdateLoading, setIsUpdateLoading] = useState(false);
  const [isDialogOpen, setDialogOpen] = useState(false);
  const [isProfileUpdated, setIsProfileUpdated] = useState(true);

  // Contexts
  const departments = useContext(DepartmentsContext)?.departments

  const openEditDialog = (row) => {
    openDialog(row);
  };

  const openDialog = () => {
    setDialogOpen(true);
  };

  const closeDialog = () => {
    setDialogOpen(false);
  };

  useEffect(
    CatchAsync(async () => {
      if (isProfileUpdated) {
        setIsUserLoading(true);
        const res = await getUser(props.match.params.id);
        setUser(res.data);
        setIsUserLoading(false);
      }
      setIsProfileUpdated(false);
    }),
    [isProfileUpdated]
  );

  const update = CatchAsync(
    async (row) => {
      // Copying the file at first index of profilepic to image
      row.image = row.profilepic[0];

      const formData = new FormData();
      for (let key in row) {
        if (row[key]!==undefined) formData.append(key, row[key]);
      }

      setIsUpdateLoading(true);
      await updateUser(user.id, formData);
      setIsUpdateLoading(false);
      setIsProfileUpdated(true);
      setDialogOpen(false);
    },
    () => setIsUpdateLoading(false)
  );

  return (
    <>
      <div className="content h-100-p">
        {isUserLoading ? (
          <div className="w-100-p h-100-p d-flex align-items-center justify-content-center">
            <div
              className="spinner-border text-dark"
              style={{ width: "3rem", height: "3rem" }}
              role="status"
            >
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        ) : (
            <>
              <Row>
                {user.image && (
                  <Col md="4">
                    <Card className="card-user">
                      <div className="image">
                        <img alt="..." src={Logo} />
                      </div>
                      <Card.Body>
                        <div className="author">
                          <a href="#pablo" onClick={(e) => e.preventDefault()}>
                            <img
                              alt="..."
                              className="avatar border-gray"
                              src={user.image}
                            />
                            <h5 className="profile-title">{user.fullname}</h5>
                          </a>
                          <p className="description">{user.email}</p>
                        </div>
                      </Card.Body>
                    </Card>
                  </Col>
                )}
                <Col md="8" className="mx-auto">
                  <Card className="card-user">
                    <Card.Header className="text-primary font-weight-bold">User Profile</Card.Header>
                    <Card.Body>
                      <Form>
                        <Row>
                          <Col className="pl-1" md="6">
                            <FormGroup>
                              <label>ID</label>
                              <FormControl
                                value={user.employee_id || ""}
                                type="text"
                                disabled
                              />
                            </FormGroup>
                          </Col>
                          <Col className="pl-1" md="6">
                            <FormGroup>
                              <label>Full Name</label>
                              <FormControl
                                value={user.fullname || ""}
                                type="text"
                                disabled
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Col className="pl-1" md="6">
                            <FormGroup>
                              <label htmlFor="exampleFormControlEmail1">
                                Email address
                            </label>
                              <FormControl
                                value={user.email}
                                type="email"
                                disabled
                              />
                            </FormGroup>
                          </Col>
                          <Col className="pl-1" md="6">
                            <FormGroup>
                              <label>Contact No</label>
                              <FormControl
                                value={user.contact_number || ""}
                                disabled
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row className="mb-4">
                          {(user.is_admin ||
                            user.is_internalInstructor ||
                            user.is_externalInstructor ||
                            user.is_staff) &&
                            (
                              <Col className="pl-1" md="6">
                                <label>Roles</label>
                                <ListGroup>
                                  {user.is_admin && (
                                    <ListGroup.Item>Admin</ListGroup.Item>
                                  )}
                                  {user.is_internalInstructor && (
                                    <ListGroup.Item>
                                      Internal Instructor
                                    </ListGroup.Item>
                                  )}
                                  {user.is_externalInstructor && (
                                    <ListGroup.Item>
                                      External Instructor
                                    </ListGroup.Item>
                                  )}
                                  {user.is_staff && (
                                    <ListGroup.Item>HOD</ListGroup.Item>
                                  )}
                                </ListGroup>
                              </Col>
                            )
                          }
                          {user.department && user.is_admin &&
                            <Col className="pl-1" md="6">
                              <label>Department</label>
                              <ListGroup>
                                <ListGroup.Item>{departments.find(dept => dept.id = user.department)?.dept_name}</ListGroup.Item>
                              </ListGroup>
                            </Col>
                          }
                        </Row>
                        <Row>
                          <div className="update">
                            <Button
                              className="btn-round"
                              color="primary"
                              type="button"
                              onClick={openEditDialog}
                            >
                              Update Profile
                          </Button>
                          </div>
                        </Row>
                      </Form>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
              <Dialog
                open={isDialogOpen}
                handleClose={closeDialog}
                additionalClass="form-modal"
              >
                <UserForm
                  user={user}
                  onSubmit={update}
                  onClose={closeDialog}
                  isSaveLoading={isUpdateLoading}
                  departments={departments}
                />
              </Dialog>
            </>
          )}
      </div>
    </>
  );
};

export default Profile;
