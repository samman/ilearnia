from rest_framework import serializers, viewsets, status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from .models import EmailOTP
from django.db import models
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.conf import settings
from rest_framework.throttling import AnonRateThrottle

from django.contrib.auth import get_user_model
User = get_user_model()

class EmailOTPSerializer(serializers.Serializer):
	otpcode = serializers.CharField(max_length=255)


class EmailOTPView(APIView):
	"""
	View For EmailOTP Post Method and Verification
	"""
	permission_classes = (AllowAny, )
	throttle_classes = [AnonRateThrottle]

	def post(self, request, *args, **kwargs):
		serializer = EmailOTPSerializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		otpcode=serializer.validated_data['otpcode']

		if otpcode:
			queryset=EmailOTP.objects.filter(otp_code__iexact=otpcode).order_by('-created_date').last()

			if queryset is not None:
				#import pdb; pdb.set_trace()

				#if queryset.session_id == request.session['session_id']:
				email=queryset.email
				user=User.objects.get(email=email)
				token, created = Token.objects.get_or_create(user=user)
				#del request.session['session_id']
				EmailOTP.objects.filter(email=email).delete()
				return Response({
						'message': 'OTP Validation Success',
						'token': token.key,
						'email': user.email,
						'is_active': user.is_active,
						'is_staff': user.is_staff,
						'is_admin': user.is_admin,
						'is_internalInstructor': user.is_internalInstructor,
						'is_externalInstructor': user.is_externalInstructor
					},status=status.HTTP_200_OK)
				# else:
				# 	return Response({'detail': 'OTP Validation failed'})
					
			return Response({'detail':'OTP Validation failed'},status=status.HTTP_400_BAD_REQUEST)

		else:
			return Response({'detail': 'Please input your otp code from your email'},status=status.HTTP_400_BAD_REQUEST)