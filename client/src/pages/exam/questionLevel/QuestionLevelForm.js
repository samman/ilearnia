import React from "react";
import { useForm } from "react-hook-form";

const QuestionLevelForm = ({ onSubmit, isSaveLoading }) => {
  const { register, handleSubmit, errors } = useForm();

  return (
    <div>
      <div className="row m-l-0 m-r-0">
        <div className="col-sm-12" style={{ minHeight: "20vh" }}>
          <div className="card-block px-0">
            <form className="row" noValidate onSubmit={handleSubmit(onSubmit)}>
              <input name="id" readOnly hidden ref={register({})} />

              <div className="row mx-0 w-100-p">
                <div className="form-group col-sm-12">
                  <label htmlFor="input-level-name" className="m-b-5 f-w-600">
                    Level Name
                  </label>
                  <input
                    type="text"
                    className="form-control text-muted f-w-400"
                    id="input-level-name"
                    name="name"
                    placeholder="Enter name"
                    ref={register({
                      required: true,
                    })}
                  />
                  {errors.name && errors.name.type === "required" && (
                    <span className="text-danger">Name is required</span>
                  )}
                </div>
                <div className="form-group col-sm-12">
                  <label htmlFor="input-level-reward" className="m-b-5 f-w-600">
                    Reward
                  </label>
                  <input
                    type="number"
                    className="form-control text-muted f-w-400"
                    id="input-level-reward"
                    name="reward"
                    placeholder="Enter reward"
                    ref={register({
                      required: true,
                    })}
                  />
                  {errors.reward && errors.reward.type === "required" && (
                    <span className="text-danger">Reward is required</span>
                  )}
                </div>
                <div className="form-group col-sm-12 d-flex justify-content-between mb-0">
                  <button
                    type="submit"
                    className="btn btn-primary mx-auto"
                    disabled={isSaveLoading}
                  >
                    Save
                    {isSaveLoading && (
                      <div
                        className="spinner-border spinner-border-sm ml-2"
                        role="status"
                      >
                        <span className="sr-only">Loading...</span>
                      </div>
                    )}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default QuestionLevelForm;
