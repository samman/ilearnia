from django.db import models
from django.contrib.postgres.fields import ArrayField


from django.contrib.auth import get_user_model

from accounts.models import Department
# from documents.models import Team

User=get_user_model()


class Csv(models.Model):
    name=models.CharField(max_length=50,null=True,blank=True)
    upload=models.FileField(upload_to='exam/%Y/%m/%d/')


    def __str__(self):
        return str(self.name)

class Level(models.Model):
    name=models.CharField(max_length=50,default='Level 1')
    reward=models.IntegerField(default=1)


class Exam(models.Model):
    EXAM_TYPE = (
        ('strict', 'strict'),
        ('flexible', 'flexible'),
    )
    RESULT=(
        ('Automatic','Automatic'),
        ('Manual','Manual'),
    )

    created_by=models.CharField(max_length=50,null=True,blank=True)
    created_in=models.DateTimeField(auto_now_add=True)
    name=models.CharField(max_length=20)
    description=models.TextField(max_length=500,null=True,blank=True)
    exam_type=models.CharField(max_length=10,choices=EXAM_TYPE,default='flexible')
    duration=models.IntegerField(blank=True,null=True)
    start_time=models.DateTimeField(null=True,blank=True)
    result_publish=models.CharField(max_length=10,choices=RESULT,default='Automatic')
    department=models.ManyToManyField(Department,related_name='exam_team')
    csv_file=models.ForeignKey(Csv, on_delete=models.PROTECT, null=True,blank=True)

    def __str__(self):
        return str(self.name)




class QuestionAnswer(models.Model):
    CHOICE_TYPE = (
        ('Single Selection', 'Single Selection'),
        ('Multiple Selection', 'Multiple Selection'),
        ('Description Selection', 'Description Selection')
    )
    question=models.CharField(max_length=255)
    choice1=models.CharField(max_length=255)
    choice2=models.CharField(max_length=255)
    choice3=models.CharField(max_length=255)
    choice4=models.CharField(max_length=255)
    correct_answer =ArrayField(models.CharField(max_length=255))
    conside=models.CharField(max_length=255,null=True,blank=True)
    choice_type = models.CharField(max_length=255, choices=CHOICE_TYPE, default='Single Selection')
    level = models.ForeignKey(Level, on_delete=models.RESTRICT,null=True)



    def __str__(self):
        return self.question

class ExamQuestion(models.Model):
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE, related_name='exam')
    question=models.ForeignKey(QuestionAnswer,on_delete=models.PROTECT,null=True)

    def __str__(self):
        return self.question.question


class ExamUser(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    question=models.ForeignKey(ExamQuestion,on_delete=models.PROTECT,related_name='que')
    question_answer=ArrayField(models.CharField(max_length=255, blank=True, null=True))
    short_description=models.CharField(max_length=155, null=True,blank=True)


    def __str__(self):
        return str(self.user)

class Result(models.Model):
    exam=models.ForeignKey(Exam,on_delete=models.PROTECT)
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    score=models.IntegerField(null=True,blank=True)
    result_status=models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)
