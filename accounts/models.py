from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from random import randint
import random
import string
from django.contrib.auth.signals import user_logged_in
from django.conf import settings
from django.core.validators import MinLengthValidator

import logging
log = logging.getLogger(__name__)

class Department(models.Model):
    """
    Models For Department
    """
    dept_name = models.CharField(max_length=255, unique=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)



class UserAccountManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):

        if not email:
            raise ValueError("The email must be set")
        if not password:
            raise ValueError("The password must be set")

        email = self.normalize_email(email)
        user = self.model(email=email, password=password, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_staffuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
            is_staff=True
        )
        return user

    def create_superuser(self, email, password, **extra_fields):

        if not email.endswith('@prabhubank.com'):                                                                                       
            raise ValueError("Only @prabhubank.com email addresses allowed")

        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_admin', True)
        extra_fields.setdefault('is_verified', True)


        return self.create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):

    """
    Models for users
    """

    GENDER_TYPE = (
        ('male', 'male'),
        ('female', 'female'),
        ('other', 'other')
    )

    created_by = models.IntegerField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True,blank=True)
    employee_id = models.CharField(max_length=255, blank=True)
    email = models.EmailField(max_length=255, unique=True)
    fullname = models.CharField(max_length=255, blank=True)
    contact_number = models.CharField(validators=[MinLengthValidator(7)], max_length=10, null=True, unique=True)
    image=models.ImageField(upload_to='profile_image',blank=True,null=True)
    department=models.ForeignKey(Department,on_delete=models.RESTRICT,related_name='department',null=True)
    branch=models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(default=False)
    is_verified = models.BooleanField(default=False)
    is_staff=models.BooleanField(default=False)
    is_admin=models.BooleanField(default=False)
    is_internalInstructor=models.BooleanField(default=False)
    is_externalInstructor=models.BooleanField(default=False)


    objects = UserAccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        return self.fullname

    def get_short_name(self):
        return self.fullname

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def __str__(self):
        return self.email


class UserModifyRecord(models.Model):
    """
    Models for user modification log
    """
    modified_user=models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
    modified_by=models.IntegerField()
    modified_date=models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.modified_by)


class EmailOTP(models.Model):
    """
    Models for Email OTP System for Login
    """
    email=models.CharField(max_length=255, null=True)
    otp_code=models.CharField(max_length=55, blank=True, null=True)
    #session_id=models.CharField(max_length=255, blank=True, null=True)
    created_date=models.DateTimeField(blank=True, null=True)


    def __str__(self):
        return self.email 

    class Meta:
        ordering = ('-created_date', )


@receiver(user_logged_in)
def create_otp(sender, request, user, **kwargs):
    email=request.user.email
    otp_code=''.join(random.choice(string.digits) for _ in range(6))
    #session_key = request.session.session_key
    created_date=timezone.now()
    #EmailOTP.objects.create(email=email, otp_code=otp_code, session_id=session_key, created_date=created_date)
    EmailOTP.objects.create(email=email, otp_code=otp_code, created_date=created_date)

