import axios from "axios";

function getAllUsers(params){
    const token = localStorage.getItem("jwt_token");
    return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/users/`, {
        headers: {
            authorization: `Token ${token}`
        },
        params: params
    });
}

function createUser(userData){
    const token = localStorage.getItem("jwt_token");
    return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/users/`, userData, {
        headers: {
            authorization: `Token ${token}`
        }
    })
}

function updateUser(id, userData){
    const token = localStorage.getItem("jwt_token");
    return axios.put(`${process.env.REACT_APP_API_BASE_URL}/api/users/${id}/`, userData, {
        headers: {
            authorization: `Token ${token}`,
        }
    })
}

// function removeUser(userId){
//     const token = localStorage.getItem("jwt_token");
//     return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/users/${userId}/soft/`, {
//         headers: {
//             authorization: `Token ${token}`
//         }
//     });
// }

function removeUser(userId){
    const token = localStorage.getItem("jwt_token");
    return axios.delete(`${process.env.REACT_APP_API_BASE_URL}/api/users/${userId}/`, {
        headers: {
            authorization: `Token ${token}`
        }
    });
}

function getUser(userId){
    const token = localStorage.getItem("jwt_token");
    return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/users/${userId}/`, {
        headers: {
            authorization: `Token ${token}`
        },
    });
}

export { getAllUsers, createUser, removeUser, updateUser, getUser }
