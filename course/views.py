from django.shortcuts import render
from rest_framework import viewsets

from .models import *
from .serializers import *

class InstructorView(viewsets.ModelViewSet):
    queryset = Instructor.objects.all()
    serializer_class = InstructorSerializer


class CategoryView(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class SubCategoryView(viewsets.ModelViewSet):
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class CourseView(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class LessonView(viewsets.ModelViewSet):
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

class LessonVideoDocumentView(viewsets.ModelViewSet):
    queryset = LessonVideoDocument.objects.all()
    serializer_class = LessionVideoDocumentSerializer

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class SyllabusView(viewsets.ModelViewSet):
    queryset = Syllabus.objects.all()
    serializer_class = SyllabusSerializer