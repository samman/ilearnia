from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from .views import UserRegisterView, VerifyEmail, UserLoginOTPView, UserLoginView, UserLogoutView, DepartmentView, UserLogView, ResendEmailVerification, UserModifyRecordView
from documents.views import TeamView
from .pwd import ChangePasswordView
from .utility import EmailOTPView


router=routers.DefaultRouter()
router.register('users', UserRegisterView, basename='users')
router.register('department', DepartmentView, basename='dept')
router.register('userlogs', UserLogView, basename='user-logs')
router.register('team', TeamView, basename='team')
router.register('modify',UserModifyRecordView,basename='modify')

urlpatterns = [
	path('token/create/', jwt_views.TokenObtainPairView.as_view(), name='token_create'),
	path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
	path('', include(router.urls)),
	path('verify-email/', VerifyEmail.as_view(), name='verify-email'),
	path('re-verifyemail/', ResendEmailVerification.as_view(), name='re-verifyemail'),
	path('v1/login/', UserLoginOTPView.as_view(), name='otp-login'),
	path('login/', UserLoginView.as_view(), name='login'),
	path('logout/', UserLogoutView.as_view(), name='logout'),
	path('change-password/<int:pk>/', ChangePasswordView.as_view(), name='change-password-view'),
	path('v1/verify-otp/', EmailOTPView.as_view(), name='verifyotp'),
	# path('api-auth/', include('rest_framework.urls',namespace='rest_framework)')),
	# path('modify/', UserModifyRecordView.as_view(), name='modify'),
]