const roles = {
    // {
    //     name: "admin",
    //     selector: "is_admin"
    // },
    // {
    //     name: "staff",
    //     selector: "is_staff"
    // },
    // {
    //     name: "internal instructor",
    //     selector: "is_internalInstructor"
    // },
    // {
    //     name: "external instructor",
    //     selector: "is_externalInstructor"
    // }
    admin: "is_admin",
    staff: "is_staff",
    internalInstructor: "is_intervalInstructor",
    externalInstructor: "is_externalInstructor"
}

export default roles;