import axios from "axios";

function getAllExams(params) {
  const token = localStorage.getItem("jwt_token");
  return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/exam/`, {
    headers: {
      authorization: `Token ${token}`,
    },
    params
  });
}

function getEnrolledExams(params) {
  const token = localStorage.getItem("jwt_token");
  return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/exam/user/`, {
    headers: {
      authorization: `Token ${token}`,
    },
    params
  });
}

function createExam(data) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/exam/`, data, {
    headers: {
      authorization: `Token ${token}`,
    },
  });
}

function updateExam(deptId, data) {
  const token = localStorage.getItem("jwt_token");
  return axios.put(
    `${process.env.REACT_APP_API_BASE_URL}/api/exam/${deptId}/`,
    data,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function removeExam(deptId) {
  const token = localStorage.getItem("jwt_token");
  return axios.delete(
    `${process.env.REACT_APP_API_BASE_URL}/api/exam/${deptId}/`,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function getAllExaminees(examId, params) {
  const token = localStorage.getItem("jwt_token");
  return axios.get(
    `${process.env.REACT_APP_API_BASE_URL}/api/exam/${examId}/exam_user/`,
    {
      headers: {
        authorization: `Token ${token}`,
      },
      params
    }
  );
}

export { getAllExams, createExam, updateExam, removeExam, getAllExaminees, getEnrolledExams };