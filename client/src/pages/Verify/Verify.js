import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { resendVerification, verifyEmail } from "../../api/auth";
import VerificationFailed from "./VerificationFailed";
import VerificationSuccessful from "./VerificationSuccessful";
import qs from "qs";

function Verify(props) {
  const [isLoading, setIsLoading] = useState(false);
  const [isResendLoading, setIsResendLoading] = useState(false);
  const [serverResponse, setServerResponse] = useState(null);
  const history = useHistory();

  const queryParams = qs.parse(props.location.search, {
    ignoreQueryPrefix: true,
  });

  const verification_token = queryParams.token;
  const email = queryParams.email;

  useEffect(() => {
    const verification = async () => {
      try {
        setIsLoading(true);
        const res = await verifyEmail(verification_token);
        setServerResponse(res);
        setIsLoading(false);
      } catch (err) {
        setServerResponse(err.response);
        setIsLoading(false);
      }
    };
    verification();
  }, []);

  const resend = async () => {
    try {
      setIsResendLoading(true);
      await resendVerification(email);
      setIsResendLoading(false);
      history.push("/");
    } catch (err) {
      setIsResendLoading(false);
    }
  };

  const goToLogin = () => {
    return history.push("/login");
  };

  return (
    <div className="w-100-p h-100-p">
      <div className="Signup container">
        <div className="row justify-content-center">
          <div className="col-xl-10 col-lg-12 col-md-9">
            <div className="card o-hidden border-0 shadow-lg">
              <div className="card-body p-4">
                {isLoading ? (
                  <div className="w-100-p h-100 d-flex align-items-center justify-content-center">
                    <div
                      className="spinner-border text-dark"
                      style={{ width: "3rem", height: "3rem" }}
                      role="status"
                    ></div>
                    <span className="p-4 font-weight-bold h4">
                      Verifying email. Please wait.
                    </span>
                  </div>
                ) : (
                  <>
                    {`${serverResponse?.status}`.startsWith("4") && (
                      <VerificationFailed
                        message={serverResponse.data.detail}
                        handleResend={resend}
                        isResendLoading={isResendLoading}
                      />
                    )}
                    {`${serverResponse?.status}`.startsWith("5") && (
                      <h1>Sorry! Something went wrong.</h1>
                    )}
                    {`${serverResponse?.status}`.startsWith("2") && (
                      <VerificationSuccessful handleBackToLogin={goToLogin} />
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Verify;
