import React from "react";
import "./PageNotFound.css";

const PageNotFound = () => {

    return(
        <div id="main">
    	    <div class="fof">
        		<h1>404 Page Not Found</h1>
    	    </div>
        </div>
    )

}

export default PageNotFound;