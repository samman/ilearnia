import React, { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import {
  getAllUsers,
  createUser,
  removeUser,
} from "../../api/user";
import UserTable from "./UserTable";
import Dialog from "../../components/Dialog";
import UserForm from "./UserForm";
import Searchbar from "../../components/Searchbar";
import { CatchAsync } from "../../util";
import { DepartmentsContext } from "../../contexts";

const initUser = {
  fullname: "",
  email: "",
  phone: "",
  dob: "",
  gender: "",
  addresses: [],
  is_admin: false,
  is_externalInstructor: false,
  is_internalInstructor: false,
  is_staff: false,
  password: "",
  confirm_password: "",
  image: null,
  gender2: "",
};

const searchOptions = [
  {
    name: "Email",
    selector: "email",
  },
];

const probableRoles = {
  is_admin: "active",
  is_externalInstructor: "external instructor",
  is_internalInstructor: "internal instructor",
  is_hod: "hod",
};

const UsersList = () => {
  // Hooks
  const [rows, setRows] = useState([]);
  const [isDialogOpen, setDialogOpen] = useState(false);
  const [isUsersListManpulated, setUsersListManipulated] = useState(true);
  const [selected, setSelected] = useState(initUser);
  const [filterBy, setFilterBy] = useState(searchOptions[0]);
  const [searchTerm, setSearchTerm] = useState("");
  const history = useHistory();
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [totalRows, setTotalRows] = useState(0);
  const [isUserDataLoading, setIsUserDataLoading] = useState(false);
  const [isCreateLoading, setIsCreateLoading] = useState(false);

  // Contexts
  const { departments } = useContext(DepartmentsContext)

  const prepareData = (datas) => {
    const rows = datas.map((data) => {
      // Role
      let userRole = [];
      Object.keys(probableRoles).forEach((prole) => {
        data[prole] && userRole.push(probableRoles[prole]);
      });
      !userRole.length && userRole.push("learner");
      const role = userRole.join(", ");

      // Phone Numbers
      return { ...data, role };
    });
    return rows;
  };
  
  useEffect(
      CatchAsync(
        async () => {
        console.log("rendered")
        if (isUsersListManpulated) {
          const params = {
            search: searchTerm,
            filter_by: filterBy.selector,
            limit: perPage,
            offset: (page - 1) * perPage,
          };
          setIsUserDataLoading(true);
          const res = await getAllUsers(
            params
          );
          setRows(prepareData(res.data.results));
          setTotalRows(res.data.count);
          setIsUserDataLoading(false);
        }
        setUsersListManipulated(false);
      },
      () => setIsUserDataLoading(false)
    ),
    [isUsersListManpulated]
  );

  const openCreateDialog = () => {
    openDialog(initUser);
  };

  const redirectToUserDetails = (row) => {
    history.push({
      pathname: `/users/${row.id}`,
      state: {
        user: row,
      },
    });
  };

  const openDialog = (row) => {
    setDialogOpen(true);
    setSelected(row);
  };

  const closeDialog = () => {
    setDialogOpen(false);
    setSelected(initUser);
  };

  const create = CatchAsync(
    async (row) => {
      row.image = row.profilepic[0];

      const formData = new FormData();
      for (let key in row) {
        if (row[key]) formData.append(key, row[key]);
      }
      setIsCreateLoading(true);
      await createUser(formData);
      setIsCreateLoading(false);
      setDialogOpen(false);
      setUsersListManipulated(true);
    },
    () => {
      setIsCreateLoading(false);
      setDialogOpen(false);
    }
  );

  const remove = CatchAsync(async (row) => {
    await removeUser(row.id);
    setUsersListManipulated(true);
  });

  const handleFilter = (term) => {
    setSearchTerm(term);
    setUsersListManipulated(true);
  };

  const handleFilterBy = (option) => {
    setFilterBy(option);
    setUsersListManipulated(true);
  };

  const handlePageChange = (page) => {
    setPage(page);
    setUsersListManipulated(true);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setIsUserDataLoading(true);

    const params = {
      search: searchTerm,
      filter_by: filterBy.selector,
      limit: newPerPage,
      offset: (page - 1) * newPerPage,
    };

    getAllUsers(params).then((res) => {
      setRows(prepareData(res.data.results));
      setTotalRows(res.data.count);
    });

    setPerPage(newPerPage);

    setIsUserDataLoading(false);
  };

  return (
    <div className="user-list">
      <Dialog
        open={isDialogOpen}
        handleClose={closeDialog}
        additionalClass="form-modal"
      >
        <UserForm
          user={selected}
          onSubmit={create}
          onClose={closeDialog}
          isSaveLoading={isCreateLoading}
          departments={departments}
        />
      </Dialog>
      <div className="card shadow mb-4">
        <div className="card-header py-3">
          <Searchbar
            options={searchOptions}
            selected={filterBy.name}
            onChange={handleFilter}
            onFilterChange={handleFilterBy}
          />
          <button
            className="btn btn-primary btn-circle btn tr-sticky-add-btn"
            onClick={openCreateDialog}
          >
            <i className="fas fa-plus"></i>
          </button>
        </div>
        <div className="card-body">
          <UserTable
            isUserDataLoading={isUserDataLoading}
            handlePerRowsChange={handlePerRowsChange}
            handlePageChange={handlePageChange}
            rows={rows}
            perPage={perPage}
            onDelete={remove}
            onRowClicked={redirectToUserDetails}
            onClose={closeDialog}
            totalRows={totalRows}
            loading={isUserDataLoading}
            departments={departments}
          />
        </div>
      </div>
    </div>
  );
};

export default UsersList;
