from rest_framework import serializers


from .models import *

class AuthorSerializer(serializers.ModelSerializer):

    class Meta:
        model=Author
        fields='__all__'

class GenreSerializer(serializers.ModelSerializer):

    class Meta:
        model=Genre
        fields='__all__'

class LanguageSerializer(serializers.ModelSerializer):

    class Meta:
        model=Language
        fields='__all__'

class BookSerializer(serializers.ModelSerializer):
    page=serializers.IntegerField(source='pages',read_only=True)

    class Meta:
        model=Book
        fields=['id','title','summary','description','thumbnail','level','author','genre','Language','upload_files','published_date','updated_date','page']
    
    # def validate_file(self, file):
    #     import pdb;pdb.set_trace()
    #     try:
    #         if not upload_files.name.endswith(('.pdf')):
    #             raise exceptions.ValidationError("Only  pdf files are accepted")

    #         return upload_files
    #     except:
    #         pass
class LevelDifficultySerializer(serializers.ModelSerializer):

    class Meta:
        model=LevelDifficulty
        fields='__all__'    
    
class BookmarkSerializer(serializers.ModelSerializer):
    user=serializers.CharField(source='user.email',read_only=True)
    class Meta:
        model=Bookmark
        fields=['user','book']