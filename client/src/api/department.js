import axios from "axios";

function getAllDepartments(params) {
  const token = localStorage.getItem("jwt_token");
  return axios.get(`${process.env.REACT_APP_API_BASE_URL}/api/department/`, {
    headers: {
      authorization: `Token ${token}`,
    },
    params
  });
}

function createDepartment(data) {
  const token = localStorage.getItem("jwt_token");
  return axios.post(`${process.env.REACT_APP_API_BASE_URL}/api/department/`, data, {
    headers: {
      authorization: `Token ${token}`,
    },
  });
}

function updateDepartment(deptId, data) {
  const token = localStorage.getItem("jwt_token");
  return axios.put(
    `${process.env.REACT_APP_API_BASE_URL}/api/department/${deptId}/`,
    data,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

function removeDepartment(deptId) {
  const token = localStorage.getItem("jwt_token");
  return axios.delete(
    `${process.env.REACT_APP_API_BASE_URL}/api/department/${deptId}/`,
    {
      headers: {
        authorization: `Token ${token}`,
      },
    }
  );
}

export { getAllDepartments, createDepartment, updateDepartment, removeDepartment };
