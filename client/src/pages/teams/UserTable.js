//React imports
import React, { useEffect } from "react";
import DataTable from "react-data-table-component";

const UserTable = ({
  rows,
  onRowClicked,
  perPage,
  handlePageChange,
  handlePerRowsChange,
  isUserDataLoading,
  totalRows,
  selectedUsers,
}) => {
  const columns = [
    {
      name: "Full Name",
      selector: "fullname",
      sortable: true,
      format: (row) => (row.fullname ? row.fullname : "-"),
    },
    {
      name: "Role",
      selector: "role",
      sortable: true,
      format: (row) => (row.role ? row.role : "-"),
    },
    {
      name: "Email Address",
      selector: "email",
      sortable: true,
    },
    {
      name: "Select",
      button: true,
      cell: (row) =>
        !row.is_admin ? (
          <div className="w-100-p h-100-p">
            <div className="form-check w-100-p h-100-p d-flex align-items-center justify-content-center">
              <input
                className="form-check-input"
                type="checkbox"
                id="flexCheckChecked"
                checked={selectedUsers.includes(row)}
                onChange={() => onRowClicked(row)}
              />
            </div>
          </div>
        ) : (
          ""
        ),
      ignoreRowClick: true,
      allowOverflow: true,
    },
  ];

  const selectAllToogle = <h1>Hi</h1>;

  return (
    <DataTable
      noHeader={true}
      actions={selectAllToogle}
      pagination={true}
      paginationServer={true}
      columns={columns}
      data={rows}
      highlightOnHover={true}
      pointerOnHover={true}
      onRowClicked={onRowClicked}
      paginationPerPage={perPage}
      onChangeRowsPerPage={handlePerRowsChange}
      onChangePage={handlePageChange}
      progressPending={isUserDataLoading}
      paginationTotalRows={totalRows}
    />
  );
};

export default UserTable;
