// import React from "react";
import { Switch, Route, useHistory } from "react-router-dom";
import React, { useContext, useEffect, useState } from "react";
import Swal from "sweetalert2";
import { apiRequestFunc, getAllExams, getEnrolledExams, removeExam } from "../../api/exam";
import { CatchAsync } from "../../util";
import { AuthContext } from "../../contexts";

import ExamList from "./ExamList";
import CreateExamForm from "./createExamForm/CreateExamForm";
import FillExamForm from "./fillExamForm/FillExamForm";
import Dialog from "../../components/Dialog";
import QuestionLevel from "./questionLevel/QuestionLevel";

const initExam = {
  name: "",
  description: "",
  exam_type: "",
  duration: "",
  start_time: "",
  department: [],
  questions: [
    {
      question: "",
      correct_answer: "",
      choise1: "",
      choise2: "",
      choise3: "",
      choise4: "",
      choice_type: "",
      level: "",
      concise_description: "",
    },
  ],
};

const searchOptions = [
  {
    name: "Exam Title",
    selector: "name",
  },
];

const Exam = ({ match }) => {
  const { loggedInUser } = useContext(AuthContext);
  const [rows, setRows] = useState([]);
  const [selectedExam, setSelectedExam] = useState(initExam);
  const [isExamManipulated, setIsExamManipulated] = useState(true);
  const [filterBy, setFilterBy] = useState(searchOptions[0]);
  const [searchTerm, setSearchTerm] = useState("");
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [totalRows, setTotalRows] = useState(0);
  const [isDeptDetailLoading, setisDeptDetailLoading] = useState(false);
  const [isQuestionLevelDialogOpen, setQuestionLevelDialogOpen] = useState(
    false
  );
  const history = useHistory();
  const apiRequestFunc = loggedInUser?.is_admin ? getAllExams : getEnrolledExams

  useEffect(
    CatchAsync(
      async () => {
        if (isExamManipulated) {
          const params = {
            search: searchTerm,
            filter_by: filterBy.selector,
            limit: perPage,
            offset: (page - 1) * perPage,
          };
          setisDeptDetailLoading(true);
          const res = await apiRequestFunc(params);
          if (loggedInUser?.is_admin){
            setRows(res.data.results);
            setTotalRows(res.data.count);
          }else{
            setRows(res.data);
            setTotalRows(res.count);
          }
          
          setisDeptDetailLoading(false);
        }
        setIsExamManipulated(false);
      },
      () => setisDeptDetailLoading(false)
    ),
    [isExamManipulated]
  );

  const showRemoveWarning = (row) => {
    Swal.fire({
      title: "Are you sure?",
      text: "Some may have already taken the exam.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.isConfirmed) {
        remove(row);
      }
    });
  };

  const remove = CatchAsync(async (row) => {
    await removeExam(row.id);
    setIsExamManipulated(true);
  });

  const handleFilter = (term) => {
    setSearchTerm(term);
    setIsExamManipulated(true);
  };

  const handleFilterBy = (option) => {
    setFilterBy(option);
    setIsExamManipulated(true);
  };

  const handlePageChange = (page) => {
    setPage(page);
    setIsExamManipulated(true);
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setisDeptDetailLoading(true);

    const params = {
      search: searchTerm,
      filter_by: filterBy.selector,
      limit: newPerPage,
      offset: (page - 1) * newPerPage,
    };

    apiRequestFunc(params).then((res) => {
      setRows(res.data.results);
      setTotalRows(res.data.count);
    });

    setPerPage(newPerPage);

    setisDeptDetailLoading(false);
  };

  const closeQuestionLevelDialog = () => {
    setQuestionLevelDialogOpen(false);
  };

  const openDialog = () => {
    setQuestionLevelDialogOpen(true);
  };

  const moveToFillExaminationPage = (exam) => {
    setSelectedExam(exam);
    history.push(`${match.path}/joinexam/${exam.id}`);
  };

  const handleAddClick = () => {
    history.push(`${match.path}/create`);
  };

  const viewParticipants = (exam) => {
    history.push(`${match.path}/${exam.id}/participants`)
  }

  return (
    <div>
      <Dialog
        open={isQuestionLevelDialogOpen}
        handleClose={closeQuestionLevelDialog}
        additionalClass="form-modal"
      >
        <QuestionLevel onClose={closeQuestionLevelDialog} />
      </Dialog>
      <Switch>
        <Route
          path={`${match.path}/create`}
          exact
          component={(props) => <CreateExamForm {...props} exam={initExam} />}
        />
        <Route
          path={`${match.path}/joinexam/:id`}
          exact
          component={(props) => <FillExamForm {...props} exam={selectedExam} />}
        />
        <Route
          path=""
          exact
          component={(props) => (
            <ExamList
              isDeptDetailLoading={isDeptDetailLoading}
              handlePerRowsChange={handlePerRowsChange}
              handlePageChange={handlePageChange}
              rows={rows}
              perPage={perPage}
              onDelete={showRemoveWarning}
              onRowClicked={(row) => {}}
              onViewParticipants={viewParticipants}
              totalRows={totalRows}
              loading={isDeptDetailLoading}
              loggedInUser={loggedInUser}
              searchOptions={searchOptions}
              filterBy={filterBy}
              handleFilter={handleFilter}
              handleFilterBy={handleFilterBy}
              initExam={initExam}
              onLevelClick={openDialog}
              onAddClick={handleAddClick}
              onJoin={moveToFillExaminationPage}
              {...props}
            />
          )}
        />
      </Switch>
    </div>
  );
};

export default Exam;
