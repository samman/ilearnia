//React imports
import React from "react";
import DataTable from "react-data-table-component";

const DepartmentTable = (props) => {
  const handleDelete = (row) => props.onDelete(row);

  const columns = [
    {
      name: "Exam Title",
      selector: "name",
      sortable: true,
    },
    {
      name: "Type",
      selector: "exam_type",
      sortable: true,
    },
    {
      name: "Action",
      button: true,
      minWidth: "40%",
      cell: (row) => (
        <>
          <button
            className="btn btn-success btn-sm btn-icon-split"
            onClick={() => props.onJoin(row)}
            target="_blank"
            rel="noopener noreferrer"
          >
            <span className="icon text-white-50">
              <i className="fas fa-sign-in-alt"></i>
            </span>
            <span className="text">Join Now</span>
          </button>
          {props.loggedInUser?.is_admin ? (
            <div className="ml-2">
              <button
                className="btn btn-info btn-sm btn-icon-split"
                onClick={() => props.onViewParticipants(row)}
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon text-white-50">
                  <i className="fas fa-eye"></i>
                </span>
                <span className="text">View Examinees</span>
              </button>
              <button
                className="btn btn-primary btn-sm ml-1"
                onClick={() => handleDelete(row)}
              >
                <i className="fas fa-trash"></i>
              </button>
            </div>
          ) : (
            ""
          )}
        </>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
    },
  ];

  return (
    <DataTable
      noHeader={true}
      pagination={true}
      paginationServer={true}
      columns={columns}
      data={props.rows}
      highlightOnHover={true}
      pointerOnHover={true}
      onRowClicked={props.onRowClicked}
      paginationPerPage={props.perPage}
      onChangeRowsPerPage={props.handlePerRowsChange}
      onChangePage={props.handlePageChange}
      progressPending={props.loading}
      paginationTotalRows={props.totalRows}
    />
  );
};

export default DepartmentTable;
