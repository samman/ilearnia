from django.urls import path, include
from .views import DocumentsView, ZoomView, TeamView
from rest_framework import routers

router=routers.DefaultRouter()
router.register('documents', DocumentsView, basename='documents')
router.register('zoom', ZoomView, basename='zoom')
router.register('team', TeamView, basename='team')


urlpatterns = [
	path('', include(router.urls)),
]