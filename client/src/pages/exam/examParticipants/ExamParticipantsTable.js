//React imports
import React, { useEffect } from "react";
import DataTable from "react-data-table-component";

const ExamParticipantsTable = ({
  rows,
  onRowClicked,
  perPage,
  handlePageChange,
  handlePerRowsChange,
  isUserDataLoading,
  totalRows,
  onPublishResult,
  onViewResult
}) => {
  const columns = [
    {
      name: "Full Name",
      selector: "fullname",
      sortable: true,
      format: (row) => (row.fullname ? row.fullname : "-"),
    },
    {
      name: "Role",
      selector: "role",
      sortable: true,
      format: (row) => (row.role ? row.role : "-"),
    },
    {
      name: "Email Address",
      selector: "email",
      sortable: true,
    },
    {
      name: "Action",
      button: true,
      minWidth: "30%",
      cell: (row) => (
        <div className="w-100-p h-100-p">
          <div className="form-check w-100-p h-100-p d-flex align-items-center justify-content-center">
            <button
              className="btn btn-success btn-sm btn-icon-split mx-2"
              onClick={() => onPublishResult(row)}
              target="_blank"
              rel="noopener noreferrer"
            >
              <span className="icon text-white-50">
                <i className="fas fa-sign-in-alt"></i>
              </span>
              <span className="text">Publish Result</span>
            </button>
            <button
              className="btn btn-info btn-sm btn-icon-split mx-2"
              onClick={() => onViewResult(row)}
              target="_blank"
              rel="noopener noreferrer"
            >
              <span className="icon text-white-50">
                <i className="fas fa-eye"></i>
              </span>
              <span className="text">Vew Result</span>
            </button>
          </div>
        </div>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
    },
  ];

  const selectAllToogle = <h1>Hi</h1>;

  return (
    <DataTable
      noHeader={true}
      actions={selectAllToogle}
      pagination={true}
      paginationServer={true}
      columns={columns}
      data={rows}
      highlightOnHover={true}
      pointerOnHover={true}
      onRowClicked={onRowClicked}
      paginationPerPage={perPage}
      onChangeRowsPerPage={handlePerRowsChange}
      onChangePage={handlePageChange}
      progressPending={isUserDataLoading}
      paginationTotalRows={totalRows}
    />
  );
};

export default ExamParticipantsTable;
