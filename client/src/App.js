import { useContext } from "react";

import "./App.scss";

// Contexts
import { AuthContext } from "./contexts/Auth";

// Layout Components
import Sidebar from "./layouts/Sidebar";
import Navbar from "./layouts/Navbar";

// Error display component
import APIErrorAlert from "./components/APIErrorAlert";

// Routes
import Routes from "./Routes";

function App() {
  const { isLoggedIn } = useContext(AuthContext);

  return (
    <div className="App" id="wrapper">
      {isLoggedIn && <Sidebar />}
      <div id="content-wrapper" className="d-flex flex-column">
        <div id="content" style={{ height: "100vh", overflow: "hidden" }}>
          {isLoggedIn && <Navbar />}
          <div
            className="container-fluid pt-4"
            style={{ height: "calc(100vh - 70px)", overflowY: "scroll" }}
          >
            {isLoggedIn && <APIErrorAlert></APIErrorAlert>}
            <Routes />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
