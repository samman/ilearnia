import React from "react";
import { useForm } from "react-hook-form";

const DepartmentForm = ({ department, onSubmit, onClose, isSaveLoading }) => {
  const { register, handleSubmit, errors } = useForm(
    !!department && {
      defaultValues: department,
    }
  );

  return (
    <div>
      <div className="row m-l-0 m-r-0">
        <div className="col-sm-12" style={{ minHeight: "20vh" }}>
          <div className="card-block">
            <form className="row" noValidate onSubmit={handleSubmit(onSubmit)}>
              <input name="id" readOnly hidden ref={register({})} />

              <div className="row mx-0 w-100-p">
                <div className="form-group col-sm-12">
                  <label htmlFor="input-dept-name" className="m-b-5 f-w-600">
                    Department Name
                  </label>
                  <input
                    type="text"
                    className="form-control text-muted f-w-400"
                    id="input-dept-name"
                    name="dept_name"
                    placeholder="Enter department name"
                    ref={register({
                      required: true,
                    })}
                  />
                  {errors.dept_name && errors.depat_name.type === "required" && (
                    <span className="text-danger">Department name is required</span>
                  )}
                </div>
                <div className="form-group col-sm-12 d-flex justify-content-between">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={isSaveLoading}
                  >
                    Save
                    {isSaveLoading && (
                      <div
                        className="spinner-border spinner-border-sm ml-2"
                        role="status"
                      >
                        <span className="sr-only">Loading...</span>
                      </div>
                    )}
                  </button>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    onClick={onClose}
                  >
                    Close
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DepartmentForm;
